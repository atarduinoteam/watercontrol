#ifndef BaseEepromStruct_h
#define BaseEepromStruct_h

class BaseEepromStruct {
  public:
    BaseEepromStruct(int offset);
    virtual ~BaseEepromStruct();
    int getStructOffset();
  protected:
    int* _structureOffset;
    virtual void initStruct() = 0;
    virtual void readStruct() = 0;
    virtual void initStructureOffset() = 0;
    virtual int getStructSize() = 0;
    int getOffset();
  private:
    int _offset;
};

#endif
