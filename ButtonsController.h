#ifndef ButtonsController_h
#define ButtonsController_h

#include "AnalogButtons.h"
#include "GenericDefinitions.h"

AnalogButtons* getAnalogButtons();

void initButtons();

unsigned long getLastButtonClickTime();

#ifdef TEST_SKETCH
  void okButtonClick();
  void escButtonClick();
  void downButtonClick();
  void upButtonClick();
  void resetFiltersButtonClick();
#endif

#endif
