#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <printf.h>

const byte NRF_CE_PIN = 9;
const byte NRF_CS_PIN = 10;

const byte RADIO_CHANNEL = 0x66;

// SERVR - Server pipe
// 1CLNT - Water control client
const byte PIPE[2][6] = {"SERVR", "1CLNT"};

typedef enum {
  REQUEST_WC_STAT = 1
} SmartWitchHouseRequest;

typedef struct {
  int deltaPressure;
  unsigned long deltaTime;
} __attribute__((packed)) DeltaEntry;

// ================ WaterControl response entry ================ //
typedef struct {
  DeltaEntry deltaEntry;
  byte size;
  byte index;
} __attribute__((packed)) RadioDeltaEntry;

RF24 radio(NRF_CE_PIN, NRF_CS_PIN);

bool receiveInProgress = false;
SmartWitchHouseRequest request = REQUEST_WC_STAT;
unsigned long lastCheckTime;
const unsigned long WAIT_TIME = 500;
const unsigned long SEND_PERIOD = 1000;
const byte MAX_REQUEST_ATTEMPT = 5;
byte currentAttempt = 0;
unsigned long lastSendTime;
bool wasReceived = false;
int expectedEntriesCount = -1;
int receivedEntriesCRC = 0;

void setup() {
  Serial.begin(9600);
  printf_begin();
  radio.begin();
  radio.setAutoAck(true);
  radio.disableDynamicPayloads();
  radio.setPayloadSize(32);
  radio.setChannel(RADIO_CHANNEL);
  radio.setPALevel(RF24_PA_MIN);
  radio.setDataRate(RF24_250KBPS);
  radio.setAddressWidth(5);
  radio.openWritingPipe(PIPE[0]);
  radio.openReadingPipe(1, PIPE[1]);
  radio.setRetries(3,5);
  radio.printDetails();
  radio.powerUp();
  radio.stopListening();
  lastSendTime = millis();
}

void loop() {
  if (!receiveInProgress && millis() - lastSendTime > SEND_PERIOD) {
    Serial.println("Send compleately new request");
    sendRequest();
    currentAttempt = 0;
  }
  if (receiveInProgress) {
    getData();
    waitBeforeFinishDataReceiving(WAIT_TIME);
  }
}

void sendRequest() {
  radio.writeBlocking(&request, sizeof(SmartWitchHouseRequest), 300);
  receiveInProgress = radio.txStandBy(300);
  if (receiveInProgress) {
    radio.startListening();
    lastCheckTime = millis();
    lastSendTime = millis();
    expectedEntriesCount = -1;
    receivedEntriesCRC = 0;
  } else {
    Serial.println("Error of sending");
  }
}

void getData() {
  RadioDeltaEntry deltaEntry;
  while (radio.available()) {
    radio.read(&deltaEntry, sizeof(RadioDeltaEntry));
    wasReceived = true;
    lastCheckTime = millis();
    if (expectedEntriesCount == -1) {
      expectedEntriesCount = deltaEntry.size;
    } else if (expectedEntriesCount != deltaEntry.size) {
      Serial.println("Wrong expected size");
    }
    receivedEntriesCRC += deltaEntry.index;
    Serial.print(deltaEntry.size); Serial.print(" - ");Serial.print(deltaEntry.index); Serial.print(": "); Serial.print(deltaEntry.deltaEntry.deltaTime); Serial.print(" - "); Serial.println(deltaEntry.deltaEntry.deltaPressure);
  }
}

void waitBeforeFinishDataReceiving(unsigned long waitTime) {
  if ((millis() - lastCheckTime) > waitTime) {
    radio.stopListening();
    receiveInProgress = false;
    if (wasReceived) {
      wasReceived = false;
      if (!checkReceivedDataCorrectness()) {
        Serial.println("CRC failed");
        performAttempt();
      }
    } else {
      Serial.print("Reply was not received.");
      performAttempt();
    }
  }
}

void onWcRequestButtonClick(const int state) {
  if (receiveInProgress) {
    Serial.println("Cannot request data, receive cycle is in progress");
  } else if (state == HIGH) {
    currentAttempt = 0;
    sendRequest();
  }
}

bool checkReceivedDataCorrectness() {
  int expectedCRC;
  expectedCRC = (expectedEntriesCount / 2) * (expectedEntriesCount + 1);
  if (expectedEntriesCount % 2 == 1) {
    expectedCRC = expectedCRC + (expectedEntriesCount / 2) + 1;
  }
  return expectedCRC == receivedEntriesCRC;
}

bool attemptIsAvailable() {
  if (currentAttempt < MAX_REQUEST_ATTEMPT) {
    currentAttempt++;
  }
  return currentAttempt < MAX_REQUEST_ATTEMPT;
}

void performAttempt() {
  Serial.print("Attempt count = "); Serial.println(currentAttempt);
  if (attemptIsAvailable()) {
    sendRequest();
  } else {
    Serial.println("Maximum count of attempts was done");
  }
}
