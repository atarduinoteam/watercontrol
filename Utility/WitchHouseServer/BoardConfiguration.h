#ifndef BoardConfiguration_h
#define BoardConfiguration_h

const byte NRF_CE_PIN = 9;
const byte NRF_CS_PIN = 10;

const int WC_REQUEST_BUTTON = 2;

#endif
