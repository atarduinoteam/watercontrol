#include <FloatingMedian.h>
#include <unity.h>
#include <stdio.h>

FloatingMedian* floatingMedian;

void testAfterInitializationMedianIsZero() {
    floatingMedian = new FloatingMedian(9, 2);
    TEST_ASSERT_EQUAL(0, floatingMedian -> getMedian());
    delete floatingMedian;
}

void testInsertingNextValueMedianWillBeReturnedCorrectly() {
    floatingMedian = new FloatingMedian(13, 2);
    int sensorValues[20] = {200, 400, 600, 800, 802, 804, 806, 808, 810, 812, 814, 816, 1000, 802, 604, 406, 1008, 1010, 1012, 1014};
    int expectedPressure[20] = {0, 0, 22, 66, 133, 222, 311, 400, 490, 580, 670, 738, 784, 806, 806, 806, 808, 830, 852, 875};
    for (int i = 0; i < 20; i++) {
        TEST_ASSERT_EQUAL(expectedPressure[i], floatingMedian -> processNextValue(sensorValues[i]));
    }
    delete floatingMedian;
}

void testMedianWillReturnTheSameValueIfNoNewSensorValuesWereProcessed() {
    floatingMedian = new FloatingMedian(13, 2);
    int sensorValues[20] = {200, 400, 600, 800, 802, 804, 806, 808, 810, 812, 814, 816, 1000, 802, 604, 406, 1008, 1010, 1012, 1014};
    for (int i = 0; i < 20; i++) {
        floatingMedian -> processNextValue(sensorValues[i]);
    }
    for (int i = 0; i < 5; i++) {
        TEST_ASSERT_EQUAL(875, floatingMedian -> getMedian());
    }
    delete floatingMedian;
}

void testSimplestFloatingMedianWillAlwaysReturnLatestSensorValue() {
    floatingMedian = new FloatingMedian(1, 0);
    for (int i = 0; i < 20; i++) {
        TEST_ASSERT_EQUAL(i, floatingMedian -> processNextValue(i));
    }
    delete floatingMedian;
}

void testTheSameValuesWillBeProcessedCorrectly() {
    floatingMedian = new FloatingMedian(13, 3);
    int sensorValues[20] = {90, 100, 110, 90, 100, 110, 90, 100, 110, 90, 100, 110, 1000, 90, 100, 110, 90, 100, 110, 1000};
    int expectedPressure[20] = {0, 0, 0, 12, 25, 40, 52, 67, 81, 94, 95, 98, 101, 101, 101, 101, 101, 101, 101, 104};
    for (int i = 0; i < 20; i++) {
        TEST_ASSERT_EQUAL(expectedPressure[i], floatingMedian -> processNextValue(sensorValues[i]));
    }
    delete floatingMedian;
}

void testMedianForInitialValueWillBeCorrect() {
    int arraySize = 13;
    floatingMedian = new FloatingMedian(arraySize, 2);
    int initialSensorValue = 500;
    for (int i = 0; i < arraySize; i++) {
        floatingMedian -> processNextValue(initialSensorValue);
    }
    TEST_ASSERT_EQUAL(initialSensorValue, floatingMedian -> processNextValue(initialSensorValue));
    delete floatingMedian;
}

int main(int argc, char **argv) {
    UNITY_BEGIN();
    RUN_TEST(testAfterInitializationMedianIsZero);
    RUN_TEST(testInsertingNextValueMedianWillBeReturnedCorrectly);
    RUN_TEST(testMedianWillReturnTheSameValueIfNoNewSensorValuesWereProcessed);
    RUN_TEST(testSimplestFloatingMedianWillAlwaysReturnLatestSensorValue);
    RUN_TEST(testTheSameValuesWillBeProcessedCorrectly);
    RUN_TEST(testMedianForInitialValueWillBeCorrect);
    UNITY_END();
    return 0;
}
