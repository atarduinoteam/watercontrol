#include <FloatMedian.h>
#include <unity.h>
#include <stdio.h>

FloatMedian* floatMedian;

FloatMedian* fillFloatMedianWithValues(_byte arraySize, _byte peakSize, int values[], _byte valuesSize) {
    floatMedian = new FloatMedian(arraySize, peakSize);
    for (_byte i = 0; i < valuesSize; i++) {
        floatMedian -> processNextValue(values[i]);
    }
    return floatMedian;
}

void checkFloatMedianArray(FloatMedian* floatMedian, int expectedValues[], _byte expectedIndexes[], _byte expectedSize) {
    ValueItem* valueItem = floatMedian -> getFirstValueItem();
    int i = 0;
    while (valueItem) {
        TEST_ASSERT_EQUAL_MESSAGE(expectedValues[i], valueItem -> getValue(), "Wrong values");
        TEST_ASSERT_EQUAL_MESSAGE(expectedIndexes[i], valueItem -> getIndex(), "Wrong indexes");
        i++;
        valueItem = valueItem -> getNextValueItem();
    }
    TEST_ASSERT_EQUAL(expectedSize, i);
}

void testArraySizeCalculation() {
    floatMedian = new FloatMedian(1, 1);
    TEST_ASSERT_EQUAL(3, floatMedian -> getArraySize());
    delete floatMedian;
    floatMedian = new FloatMedian(5, 2);
    TEST_ASSERT_EQUAL(5, floatMedian -> getArraySize());
    delete floatMedian;
    floatMedian = new FloatMedian(5, 3);
    TEST_ASSERT_EQUAL(7, floatMedian -> getArraySize());
    delete floatMedian;
    floatMedian = new FloatMedian(6, 2);
    TEST_ASSERT_EQUAL(5, floatMedian -> getArraySize());
    delete floatMedian;
    floatMedian = new FloatMedian(6, 3);
    TEST_ASSERT_EQUAL(7, floatMedian -> getArraySize());
    delete floatMedian;
}

void testArrayInitialization() {
    floatMedian = new FloatMedian(9, 2);
    ValueItem* valueItem = floatMedian -> getFirstValueItem();
    TEST_ASSERT_NOT_NULL(valueItem);
    int i = 0;
    while (valueItem) {
        TEST_ASSERT_EQUAL(i, valueItem -> getIndex());
        TEST_ASSERT_EQUAL(0, valueItem -> getValue());
        valueItem = valueItem -> getNextValueItem();
        i++;
    }
    TEST_ASSERT_EQUAL(9, i);
    delete floatMedian;
}

void testElementInsertionInArray() {
    _byte arraySize = 9;

    printf("Test 1...");
    _byte valuesCount = 8;
    int values[valuesCount] = {1, 2, 3, 4, 5, 6, 7, 8};
    int expectedValues[arraySize] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    _byte expectedIndexes[arraySize] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    floatMedian = fillFloatMedianWithValues(arraySize, 2, values, valuesCount);
    checkFloatMedianArray(floatMedian, expectedValues, expectedIndexes, arraySize);
    delete floatMedian;
    printf("ok\n");

    printf("Test 2...");
    valuesCount = 1;
    int values_1[valuesCount] = {10};
    int expectedValues_1[arraySize] = {0, 0, 0, 0, 0, 0, 0, 0, 10};
    _byte expectedIndexes_1[arraySize] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    floatMedian = fillFloatMedianWithValues(arraySize, 2, values_1, valuesCount);
    checkFloatMedianArray(floatMedian, expectedValues_1, expectedIndexes_1, arraySize);
    delete floatMedian;
    printf("ok\n");

    printf("Test 3...");
    valuesCount = 9;
    int values_2[valuesCount] = {10, 12, 11, 14, 13, 28, 7, 22, 9};
    int expectedValues_2[arraySize] = {7, 9, 10, 11, 12, 13, 14, 22, 28};
    _byte expectedIndexes_2[arraySize] = {6, 8, 0, 2, 1, 4, 3, 7, 5};
    floatMedian = fillFloatMedianWithValues(arraySize, 2, values_2, valuesCount);
    checkFloatMedianArray(floatMedian, expectedValues_2, expectedIndexes_2, arraySize);
    delete floatMedian;
    printf("ok\n");

    printf("Test 4...");
    valuesCount = 5;
    int values_3[valuesCount] = {4, 4, 4, 3, 3};
    int expectedValues_3[arraySize] =    {0, 0, 0, 0, 3, 3, 4, 4, 4};
    _byte expectedIndexes_3[arraySize] = {0, 1, 2, 3, 8, 7, 6, 5, 4};
    floatMedian = fillFloatMedianWithValues(arraySize, 2, values_3, valuesCount);
    checkFloatMedianArray(floatMedian, expectedValues_3, expectedIndexes_3, arraySize);
    delete floatMedian;
    printf("ok\n");
}

void testMedian() {
    _byte arraySize = 9;

    printf("Test 1 (common) ... ");
    _byte valuesCount = 8;
    int values[valuesCount] = {1, 2, 3, 4, 5, 6, 7, 8};
    floatMedian = fillFloatMedianWithValues(arraySize, 2, values, valuesCount);
    TEST_ASSERT_EQUAL(4, floatMedian -> getMedian());
    delete floatMedian;
    printf("ok\n");

    printf("Test 2 (with peaks) ... ");
    valuesCount = 8;
    int values_1[valuesCount] = {100, 25, 30, 35, 500, 6, 70, 40};
    floatMedian = fillFloatMedianWithValues(arraySize, 2, values_1, valuesCount);
    TEST_ASSERT_EQUAL(40, floatMedian -> getMedian());
    delete floatMedian;
    printf("ok\n");

    printf("Test 3 (overflow) ... ");
    valuesCount = 8;
    int values_3[valuesCount] = {10000, 20000, 30000, 24000, 25000, 6, 7000, 8000};//{10000, 20000, , 24000, , , , 8000};
    floatMedian = fillFloatMedianWithValues(arraySize, 2, values_3, valuesCount);
    TEST_ASSERT_EQUAL(13800, floatMedian -> getMedian());
    delete floatMedian;
    printf("ok\n");
}

void testPeakValueCalculationForMedianCalculation() {
    _byte arraySize = 9;

    printf("Test 1 (common) ... ");
    _byte valuesCount = 8;
    int values[valuesCount] = {10000, 20000, 23000, 4, 5, 60, 70, 80}; // , , , , 60, 70, 80, 
    floatMedian = fillFloatMedianWithValues(arraySize, 2, values, valuesCount);
    TEST_ASSERT_EQUAL(2043, floatMedian -> getMedian(2));
    TEST_ASSERT_EQUAL(70, floatMedian -> getMedian(3));
    TEST_ASSERT_EQUAL(70, floatMedian -> getMedian(5));
    delete floatMedian;
    printf("ok\n");
}

int main(int argc, char **argv) {
    UNITY_BEGIN();
    // RUN_TEST(testArraySizeCalculation);
    // RUN_TEST(testArrayInitialization);
    // RUN_TEST(testElementInsertionInArray);
    // RUN_TEST(testMedian);
    // RUN_TEST(testPeakValueCalculationForMedianCalculation);
    UNITY_END();
    return 0;
}
