#ifndef FloatMedian_h
#define FloatMedian_h

typedef unsigned int _byte __attribute__((__mode__(__QI__)));

class ValueItem {
    public:
        ValueItem(int value, _byte index, ValueItem* nextValueItem);
        ~ValueItem();
        int getValue();
        _byte getIndex();
        ValueItem* getNextValueItem();
        void setNextValueItem(ValueItem* nextValueItem);
        void decreaseIndex();
    private:
        int _value;
        _byte _index;
        ValueItem* _nextValueItem;
};

class FloatMedian {
    public:
        FloatMedian(_byte arraySize, _byte peakArraySize);
        ~FloatMedian();
        _byte getArraySize();
        ValueItem* getFirstValueItem();
        int processNextValue(int value);
        int getMedian();
        int getMedian(_byte peakArraySize);
    private:
        _byte _calculateArraySize(_byte arraySize, _byte peakArraySize);
        void _initArray();
        _byte _arraySize;
        _byte _peakArraySize;
        ValueItem* _firstValueItem;
};

#endif