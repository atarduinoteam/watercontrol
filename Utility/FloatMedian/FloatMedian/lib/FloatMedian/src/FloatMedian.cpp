#include "FloatMedian.h"

// ======================= FloatMedian ======================= //

FloatMedian::FloatMedian(_byte arraySize, _byte peakArraySize) {
    _arraySize = _calculateArraySize(arraySize, peakArraySize);
    _peakArraySize = peakArraySize;
    _initArray();
}

FloatMedian::~FloatMedian() {
    ValueItem* valueItem = _firstValueItem;
    while (valueItem) {
        ValueItem* deletedValueItem = valueItem;
        valueItem = valueItem -> getNextValueItem();
        delete deletedValueItem;
    }
}

int FloatMedian::processNextValue(int value) {
    // Remove oldest value
    ValueItem* prevValueItem = 0;
    ValueItem* valueItem = getFirstValueItem();
    while (valueItem -> getIndex() != 0) {
        prevValueItem = valueItem;
        valueItem = valueItem -> getNextValueItem();
    }
    if (prevValueItem) {
        prevValueItem -> setNextValueItem(valueItem -> getNextValueItem());
    } else {
        _firstValueItem = valueItem -> getNextValueItem();
    }
    delete valueItem;
    // Decrease indexes
    valueItem = getFirstValueItem();
    while (valueItem) {
        valueItem -> decreaseIndex();
        valueItem = valueItem -> getNextValueItem();
    }
    // insert new ValueItem into the correct position
    prevValueItem = 0;
    valueItem = getFirstValueItem();
    while (valueItem && (valueItem -> getValue() < value)) {
        prevValueItem = valueItem;
        valueItem = valueItem -> getNextValueItem();
    }
    ValueItem* newValueItem = new ValueItem(value, getArraySize() - 1, valueItem);
    if (prevValueItem) {
        prevValueItem ->setNextValueItem(newValueItem);
    } else {
        _firstValueItem = newValueItem;
    }
    // Calculate average
    return getMedian();
}

_byte FloatMedian::getArraySize() {
    return _arraySize;
}

ValueItem* FloatMedian::getFirstValueItem() {
    return _firstValueItem;
}

int FloatMedian::getMedian() {
    return getMedian(_peakArraySize);
}

int FloatMedian::getMedian(_byte peakArraySize) {
    _byte actualPeakArraySize = peakArraySize;
    _byte maxPeakArraySize = (_arraySize - 1) / 2;
    if (peakArraySize > maxPeakArraySize) {
        actualPeakArraySize = maxPeakArraySize;
    }
    ValueItem* valueItem = getFirstValueItem();
    for (int i = 0; i < actualPeakArraySize; i++) {
        valueItem = valueItem -> getNextValueItem();
    }
    long result = 0;
    _byte meaningfulArrayLength = _arraySize - 2 * actualPeakArraySize;
    for (int i = 0; i < meaningfulArrayLength; i++) {
        result += valueItem -> getValue();
        valueItem = valueItem -> getNextValueItem();
    }
    return result / meaningfulArrayLength;
}

_byte FloatMedian::_calculateArraySize(_byte arraySize, _byte peakArraySize) {
    if (arraySize % 2 == 0) {
        arraySize--;
    }
    if (arraySize < (2*peakArraySize + 1)) {
        return 2*peakArraySize + 1;
    } else {
        return arraySize;
    }
}

void FloatMedian::_initArray() {
    ValueItem* nextValueItem = 0;
    for (int i = _arraySize; i > 0; i--) {
        nextValueItem = new ValueItem(0, i - 1, nextValueItem);
    }
    _firstValueItem = nextValueItem;
}

// ======================= ValueItem ======================= //

ValueItem::ValueItem(int value, _byte index, ValueItem* nextValueItem) {
    _value = value;
    _index = index;
    _nextValueItem = nextValueItem;
}

ValueItem::~ValueItem() {

}

int ValueItem::getValue() {
    return _value;
}

_byte ValueItem::getIndex() {
    return _index;
}

ValueItem* ValueItem::getNextValueItem() {
    return _nextValueItem;
}

void ValueItem::setNextValueItem(ValueItem* nextValueItem) {
    _nextValueItem = nextValueItem;
}

void ValueItem::decreaseIndex() {
    _index--;
}
