#include "FloatingMedian.h"
#include <Arduino.h>

FloatingMedian::FloatingMedian(byte arraySize, byte peakArraySize) {
    _arraySize = arraySize;
    _peakArraySize = peakArraySize;
    _initArray();
}

FloatingMedian::~FloatingMedian() {
    delete[] _sensorValues;
}

int FloatingMedian::processNextValue(int value) {
    for (byte i = 0; i < (_arraySize - 1); i++) {
        _sensorValues[i] = _sensorValues[i + 1];
    }
    _sensorValues[_arraySize - 1] = value;
    return getMedian();
}

int FloatingMedian::getMedian() {
    int lastMin = -1;
    int currentMin = 1024;
    int minElementCounter = 1;
    long result = 0;
    for (byte j = 0; j < (_arraySize - _peakArraySize); ) {
        for (byte i = 0; i < _arraySize; i++) {
            if (_sensorValues[i] > lastMin && _sensorValues[i] < currentMin) {
                currentMin = _sensorValues[i];
                minElementCounter = 1;
            } else if (_sensorValues[i] == currentMin) {
                minElementCounter++;
            }
        }
        if (j < _peakArraySize) {
            if (minElementCounter > (_peakArraySize - j)) {
                result += currentMin * min(minElementCounter - _peakArraySize + j, _arraySize - 2 * _peakArraySize);
            }
        } else {
            result += currentMin * min(minElementCounter, _arraySize - _peakArraySize - j);
        }
        j += minElementCounter;
        lastMin = currentMin;
        currentMin = 1024;
    }
    return (result / (_arraySize - 2 * _peakArraySize));
}

void FloatingMedian::_initArray() {
    _sensorValues = new int[_arraySize];
    for (byte i = 0; i < _arraySize; i++) {
        _sensorValues[i] = 0;
    }
}
