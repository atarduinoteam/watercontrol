#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include <SmartWitchHouse.h>
#include <printf.h>

RF24 radio(9, 10);

void setup() {
  Serial.begin(9600);
  printf_begin();
  radio.begin();
  radio.setPayloadSize(sizeof(int));
//  radio.setPayloadSize(32);
//  radio.enableDynamicPayloads();
  radio.setChannel(RADIO_CHANNEL);
  radio.setPALevel(RF24_PA_MIN);
  radio.setDataRate(RF24_250KBPS);
  radio.setAddressWidth(5);
  radio.openWritingPipe(PIPE[1]);
  radio.openReadingPipe(1, PIPE[0]);
  radio.printDetails();
  radio.powerUp();
  radio.startListening(); 
}

SmartWitchHouseRequest request;

unsigned int i;
unsigned int counter;

void loop() {
  delay(500);
  counter = 0;
  while(radio.available()) {
    radio.read(&i, sizeof(SmartWitchHouseRequest));
    counter++;
  }
  Serial.println(counter);
//  delay(200);
//  while (radio.available()) {
//    Serial.println("Available");
//    radio.read(&request, sizeof(SmartWitchHouseRequest));
//    if (request == REQUEST_WC_STAT) {
//      radio.stopListening();
//      DeltaEntry d;
//      d.deltaPressure = 100;
//      d.deltaTime = 1;
//      radio.write(&d, sizeof(DeltaEntry));
//      d.deltaTime = 2;
//      radio.write(&d, sizeof(DeltaEntry));
//      d.deltaTime = 3;
//      radio.write(&d, sizeof(DeltaEntry));
//      radio.startListening();
//    }
//    Serial.println(request);
//  }
}
