#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <printf.h>
//#include <SmartWitchHouse.h>

RF24* _radio;

const byte NRF_CE_PIN = 9;
const byte NRF_CS_PIN = 10;

const byte RADIO_CHANNEL = 0x66;

// SERVR - Server pipe
// 1CLNT - Water control client
const byte PIPE[2][6] = {"SERVR", "1CLNT"};

typedef enum {
  REQUEST_WC_STAT = 1
} SmartWitchHouseRequest;

typedef struct {
  int deltaPressure;
  unsigned long deltaTime;
} __attribute__((packed)) DeltaEntry;

// ================ WaterControl response entry ================ //
typedef struct {
  DeltaEntry deltaEntry;
  byte size;
  byte index;
} __attribute__((packed)) RadioDeltaEntry;

void setup() {
  Serial.begin(9600);
  printf_begin();
  _radio = new RF24(NRF_CE_PIN, NRF_CS_PIN);
  _radio -> begin();
  _radio -> setAutoAck(true);
  _radio -> setPayloadSize(32);
  _radio -> setChannel(RADIO_CHANNEL);
  _radio -> setPALevel(RF24_PA_MIN);
  _radio -> setDataRate(RF24_250KBPS);
  _radio -> setAddressWidth(5);
  _radio -> openWritingPipe(PIPE[1]);
  _radio -> openReadingPipe(1, PIPE[0]);
  _radio -> printDetails();
  _radio -> powerUp();
  _radio -> startListening(); 
}

DeltaEntry deltaEntry;

unsigned int i = 0;
unsigned int counter = 0;
unsigned long startTime = 0;
const unsigned long OTHER_ACTIVITY_DELAY = 500;
SmartWitchHouseRequest request;

void loop() {
  otherActivitiesSimulation(OTHER_ACTIVITY_DELAY);
  // Check request availability and pass data back if needed
  check();
}

void otherActivitiesSimulation(unsigned long duration) {
  unsigned long otherActivityStart = millis();
  // Imitate other activities stuff
  unsigned long i = 0;
  while(millis() - otherActivityStart < duration) {
    i++;
    if (i % 100 == 0) {
      Serial.print(".");
    }
  }
  Serial.println();
}

void check() {
  while (_radio -> available()) {
    startTime = millis();
    Serial.println("Data available");
    _radio -> read(&request, sizeof(SmartWitchHouseRequest));
    if (request == REQUEST_WC_STAT) {
       _radio -> stopListening();
       for (int i = 0; i < 10; i++) {
         DeltaEntry deltaEntry;
         deltaEntry.deltaPressure = i*10;
         deltaEntry.deltaTime = millis();
         RadioDeltaEntry radioDeltaEntry;
         radioDeltaEntry.deltaEntry = deltaEntry;
         radioDeltaEntry.size = 10;
         radioDeltaEntry.index = i + 1;
         _radio -> writeBlocking(&radioDeltaEntry, sizeof(RadioDeltaEntry), 300);
         bool res = _radio -> txStandBy(300);
         if (!res) {
           Serial.println("Cannot pass data");
           break;
         }
       }
       _radio -> startListening();
       Serial.print("Finished. Duration: "); Serial.println(millis() - startTime);
    } else {
      Serial.println("Wrong request");
    }
  }
}
