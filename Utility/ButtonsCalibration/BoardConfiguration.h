#ifndef BoardConfiguration_h
#define BoardConfiguration_h

const int WTPS_PIN=A6;
const int PPS_PIN=A7;
const int MENU_BUTTONS_PIN = A2;
/**
 * Common status diode address:
 * |h|l|status|
 * |0|0|green |
 * |0|1|yellow|
 * |1|0|red   |
 */
const byte DIODE_ADDR_LOW = 1;
const byte DIODE_ADDR_HIGH = 2;

const byte WATER_PUMP_PIN = 11;

const byte NRF_CE_PIN = 12;
const byte NRF_CS_PIN = 13;

const byte EEPROM_DATA_VERSION = 13;

const int MAX_SENSOR_VALUE = 1023;
const int EEPROM_SIZE = 512;
const int EXPECTED_PRESSURE = 2000;

const unsigned long CLOSE_MENU_DELAY = 10000;

#endif
