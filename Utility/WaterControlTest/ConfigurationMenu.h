#ifndef configuration_menu_h
#define configuration_menu_h

#include <Arduino.h>
#include <PCD8544.h>
#include "Configuration.h"
#include "BoardConfiguration.h"
#include "SensorValueNormalizer.h"

class BaseMenuItem {
  public:
    BaseMenuItem(char __name[], BaseMenuItem* parent, PCD8544* lcd);
    virtual ~BaseMenuItem();
    virtual BaseMenuItem* onOkAction() = 0; // Returns menu container which should be displayed after "ok" action finished
    virtual void onEscAction();
    virtual void onUpAction() = 0;
    virtual void onDownAction() = 0;
    virtual void drawScreen() = 0;
    virtual void init() = 0;                // Executed before enter to the current menu;
    virtual void redrawScreen();
    char* getName();
    BaseMenuItem* getParent();
  protected:
    PCD8544* getLcd();
  private:
    char* _name;
    BaseMenuItem* _parent;
    PCD8544* _lcd;
};

class ContainerMenuItem : public BaseMenuItem {
  public:
    ContainerMenuItem(char _name[], byte size, PCD8544* lcd, BaseMenuItem* parent);
    ~ContainerMenuItem();
    BaseMenuItem* onOkAction() override;
    void onUpAction() override;
    void onDownAction() override;
    void drawScreen() override;
    void init() override;
    byte getSize();
    void addMenuItem(BaseMenuItem* menuItem);
  private:
    BaseMenuItem** _childItems;
    byte _containerTop; // Used for addMenuItem operation;
    byte _size;
    byte _currentSelectedMenuItem;
    byte _startPosition;
    byte _endPosition;
    void _updateFrame();
    BaseMenuItem* _getCurrentMenuItem();
};

class LeafMenuItem : public BaseMenuItem {
  public:
    LeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* _lcd);
    ~LeafMenuItem();
};

class NumericLeafMenuItem : public LeafMenuItem {
  public:
    NumericLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd, ConfigurationProperty configurationProperty, int minValue, int maxValue, int delta = 10, bool convertPressure = true);
    ~NumericLeafMenuItem();
    BaseMenuItem* onOkAction() override;
    void onUpAction() override;
    void onDownAction() override;
    void drawScreen() override;
    void init() override;
    void onEscAction() override;
  protected:
    int getCurrentValue();
    void setCurrentValue(int currentValue);
    void setConfigurationProperty(ConfigurationProperty configurationProperty);
  private:
    int _minValue;
    int _maxValue;
    int _currentValue;
    int _delta;
    bool _convertPressure;
    ConfigurationProperty _configurationProperty;
    void _applyNumericValue();
};

class BooleanLeafMenuItem : public LeafMenuItem {
  public:
    BooleanLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd, ConfigurationProperty configurationProperty);
    BaseMenuItem* onOkAction() override;
    void onUpAction() override;
    void onDownAction() override;
    void drawScreen() override;
    void init() override;
  private:
    bool _currentValue;
    ConfigurationProperty _configurationProperty;
};

class BaseActionLeafMenuItem : public LeafMenuItem {
  public:
    BaseActionLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd);
    void onUpAction() override;
    void onDownAction() override;
    BaseMenuItem* onOkAction() override;
    void init() override;
  protected:
    byte _saved;
};

class AutoZeroLevelSensorLeafMenuItem : public BaseActionLeafMenuItem {
  public:
    AutoZeroLevelSensorLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd);
    BaseMenuItem* onOkAction() override;
    void drawScreen() override;
};

class ManualZeroLevelSensorLeafMenuItem : public NumericLeafMenuItem {
  public:
    ManualZeroLevelSensorLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd, int minValue, int maxValue, int sensorPin, int delta = 1);
    void drawScreen() override;
    void redrawScreen() override;
  private:
    int _sensorPin;
};

class ManualNonZeroLevelSensorLeafMenuItem : public NumericLeafMenuItem {
  public:
    ManualNonZeroLevelSensorLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd, int minValue, int maxValue, int sensorPin, int delta = 1);
    void drawScreen() override;
    void redrawScreen() override;
  private:
    int _sensorPin;
};

class AutoNonZeroLevelSensorLeafMenuItem : public NumericLeafMenuItem {
  public:
    AutoNonZeroLevelSensorLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd, int minValue, int maxValue, int delta = 10);
    void init() override;
    BaseMenuItem* onOkAction() override;
    void drawScreen() override;
  private:
    byte _saved;
};

class ConfigurationMenu {
  public:
    ConfigurationMenu(PCD8544* lcd);
    ~ConfigurationMenu();
    void onOkAction();
    void onUpAction();
    void onDownAction();
    bool onEscAction(); // If true that means that requested escape from menu screen
    void turnOn();
    void redrawScreen();
  private:
    PCD8544* _lcd;
    void _init(); // Set initial menu state
    BaseMenuItem* _currentMenuItem;
    ContainerMenuItem* _root;
};

#endif
