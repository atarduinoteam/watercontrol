#include "ButtonsController.h"
#include "InitBoard.h"

AnalogButtons* _analogButtons;

AnalogButtons* getAnalogButtons() {
  return _analogButtons;
}

const int OK_BUTTON_LEVEL = 208;
const int UP_BUTTON_LEVEL = 403;
const int DOWN_BUTTON_LEVEL = 609;
const int ESC_BUTTON_LEVEL = 833;
const int RESET_FILTERS_BUTTON_LEVEL = 1023;

unsigned long _lastButtonClickTime = 0;

unsigned long getLastButtonClickTime() {
  return _lastButtonClickTime;
}

void okButtonClick() {
  _lastButtonClickTime = millis();
  if (getDisplayController() != NULL) {
    getDisplayController() -> turnOnHighlight();
    if (getDisplayController() -> isDisplayRegularMenu()) {
      getDisplayController() -> setDisplayRegularMenu(false);
      getDisplayController() -> getConfigurationMenu() -> turnOn();
    } else {
      getDisplayController() -> getConfigurationMenu() -> onOkAction();
    }
  }
}

void upButtonClick() {
  _lastButtonClickTime = millis();
  if (getDisplayController() != NULL) {
    getDisplayController() -> turnOnHighlight();
    if (!getDisplayController() -> isDisplayRegularMenu()) {
      getDisplayController() -> getConfigurationMenu() -> onUpAction();
    }
  }
}

void downButtonClick() {
  _lastButtonClickTime = millis();
  if (getDisplayController() != NULL) {
    getDisplayController() -> turnOnHighlight();
    if (!getDisplayController() -> isDisplayRegularMenu()) {
      getDisplayController() -> getConfigurationMenu() -> onDownAction();
    }
  }
}

void escButtonClick() {
  _lastButtonClickTime = millis();
  if (getDisplayController() != NULL) {
    getDisplayController() -> turnOnHighlight();
    if (!getDisplayController() -> isDisplayRegularMenu()) {
      if (getDisplayController() -> getConfigurationMenu() -> onEscAction()) {
        getDisplayController() -> setDisplayRegularMenu(true);
      }
    }
  }
}

void resetFiltersButtonClick() {
  _lastButtonClickTime = millis();
  if (getDisplayController() != NULL) {
    getDisplayController() -> turnOnHighlight();
  }
  getStatistic() -> resetErrorCode();
  getStatistic() -> setPpsCycleMax(0);
  getDisplayController() -> setDisplayRegularMenu(true);
}

void initButtons() {
  Button* okButton = new Button(OK_BUTTON_LEVEL, &okButtonClick);
  Button* upButton = new Button(UP_BUTTON_LEVEL, &upButtonClick);
  Button* downButton = new Button(DOWN_BUTTON_LEVEL, &downButtonClick, &downButtonClick);
  Button* escButton = new Button(ESC_BUTTON_LEVEL, &escButtonClick);
  Button* resetFiltersButton = new Button(RESET_FILTERS_BUTTON_LEVEL, &resetFiltersButtonClick);
  _analogButtons = new AnalogButtons(MENU_BUTTONS_PIN, 5);
  _analogButtons -> add(okButton);
  _analogButtons -> add(upButton);
  _analogButtons -> add(downButton);
  _analogButtons -> add(escButton);
  _analogButtons -> add(resetFiltersButton);
}
