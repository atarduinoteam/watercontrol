#ifndef RadioController_h
#define RadioController_h

#include "Statistic.h"
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include "DeltaLog.h"

class RadioController {
  public:
    RadioController(Statistic* statistic, DeltaLog* deltaLog);
    void check();
  private:
    RF24* _radio;
    Statistic* _statistic;
    DeltaLog* _deltaLog;
    void _passLogs();
    bool _sendDeltaEntry(DeltaEntry deltaEntry, int size, int index);
};

#endif
