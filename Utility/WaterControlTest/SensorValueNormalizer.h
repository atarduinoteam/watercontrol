#ifndef SensorValueNormalizer_h
#define SensorValueNormalizer_h

#include "BoardConfiguration.h"
#include "Configuration.h"

class SensorValueNormalizer {
    public:
      SensorValueNormalizer(int arraySize, int peakArraySize, int initialWtpsSensorValue, int initialPpsSensorValue);
      ~SensorValueNormalizer();
      int getWtpsMedian(int sensorValue);
      int getPpsMedian(int sensorValue);
      int normalizeWtpsSensorValue(int sensorValue, int maxWtpsPressure = 0);
      int normalizePpsSensorValue(int sensorValue, int maxPpsPressure = 0);
    private:
      FloatingMedian* _wtpsFloatingMedian;
      FloatingMedian* _ppsFloatingMedian;
};

#endif
