#ifndef log_h
#define log_h

#include "Statistic.h"
#include <SmartWitchHouse.h>

class DeltaLogIterator {
  public:
    DeltaLogIterator();
    bool hasNext();
    DeltaEntry getNext();
    void init();
    int getSize();
  private:
    int _stepCount;
    int _currentPosition;
    int _size;
    bool _isFirstCycle();
};

class DeltaLog {
  public:
    DeltaLog();
    ~DeltaLog();
    bool writeDeltaIfBigger(int delta);
    DeltaLogIterator* getIterator();
  private:
    void _writeDelta(DeltaEntry delta);
    int _lastDeltaPressure = 0;
    unsigned long _lastSavingTime;
    DeltaLogIterator* _iterator;
};

#endif
