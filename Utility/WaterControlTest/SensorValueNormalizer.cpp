#include "SensorValueNormalizer.h"
#include "InitBoard.h"
#include "BoardConfiguration.h"

// Initial values have to be initialized to prevent pump turn on right after power is on. Otherwise first ${arraySize} iterations calculated normilized pressure will be less then actual.
SensorValueNormalizer::SensorValueNormalizer(int arraySize, int peakArraySize, int initialWtpsSensorValue, int initialPpsSensorValue) {
  _wtpsFloatingMedian = new FloatingMedian(arraySize, peakArraySize);
  _ppsFloatingMedian = new FloatingMedian(arraySize, peakArraySize);
  for (int i = 0; i < arraySize; i++) {
    _wtpsFloatingMedian -> processNextValue(initialWtpsSensorValue);
    _ppsFloatingMedian -> processNextValue(initialPpsSensorValue);
  }
}

SensorValueNormalizer::~SensorValueNormalizer() {
  delete _wtpsFloatingMedian;
  delete _ppsFloatingMedian;
}

int SensorValueNormalizer::getWtpsMedian(int sensorValue) {
  return _wtpsFloatingMedian -> processNextValue(sensorValue);
}

int SensorValueNormalizer::getPpsMedian(int sensorValue) {
  return _ppsFloatingMedian -> processNextValue(sensorValue);
}

int SensorValueNormalizer::normalizeWtpsSensorValue(int sensorValue, int maxWtpsPressure) {
  int medianSensorValue = _wtpsFloatingMedian -> processNextValue(sensorValue);
  return map(medianSensorValue, getConfiguration() -> getWtpsOffset(), MAX_SENSOR_VALUE, 0, maxWtpsPressure == 0 ? getConfiguration() -> getMaxWtpsPressure() : maxWtpsPressure);
}

int SensorValueNormalizer::normalizePpsSensorValue(int sensorValue, int maxPpsPressure) {
  int medianSensorValue = _ppsFloatingMedian -> processNextValue(sensorValue);
  return map(medianSensorValue, getConfiguration() -> getPpsOffset(), MAX_SENSOR_VALUE, 0, maxPpsPressure == 0 ? getConfiguration() -> getMaxPpsPressure() : maxPpsPressure);
}
