#ifndef GenericDefinitions_h
#define GenericDefinitions_h

#define TEST_SKETCH
//#define MANUAL_E2E_TEST
//#define CONFIGURATION_MENU_ROLL_TESTS

// *** Test definitions ***

#define EEPROM_SAVING_TEST

//#define DELTA_LOG_TEST
//#define MEMORY_LEAK_TEST

//#define REGULAR_SCREEN_TESTS

//#define CONFIGURATION_MENU_ITEMS_TESTS

//#define CONFIGURATION_MENU_NAVIGATION_TESTS

//#define CONFIGURATION_VALUES_SAVING

//#define SCREEN_HIGHLIGHT
//#define RESET_BUTTON_FUNCTIONALITY
//#define PUMP_CONTROLLER_1

//#define PUMP_CONTROLLER_2

// To avoid unexpected issues this test set should be executed alone
//#define UNIT_TEST

//#define PROTECTION_TEST
//#define ERROR_CODE_TEST
//#define WATCHDOG_TEST

//#define WATCHDOG_REBOOT_TEST

//#define RADIO_TEST

#endif
