#include "SensorValidator.h"
#include <Arduino.h>

SensorValidator::SensorValidator(int allowableDelta, unsigned long checkDuration) {
    _allowableDelta = allowableDelta;
    _checkDuration = checkDuration;
    _startTime = 0;
}

SensorValidator::~SensorValidator() {

}

bool SensorValidator::isValid(int wtpsValue, int ppsValue) {
    if (wtpsValue > (ppsValue + _allowableDelta)) {
        if (_startTime == 0) {
            _startTime = millis();
        }
        return ((millis() - _startTime) < _checkDuration);
    } else {
        _startTime = 0;
        return true;
    }
}
