#ifndef SensorValidator_h
#define SensorValidator_h

class SensorValidator {
    public:
        SensorValidator(int allowableDelta, unsigned long checkDuration);
        ~SensorValidator();
        bool isValid(int wtpsValue, int ppsValue);
    private:
        int _allowableDelta;
        unsigned long _checkDuration;
        unsigned long _startTime;
};

#endif
