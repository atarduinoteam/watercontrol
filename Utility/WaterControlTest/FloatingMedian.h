#ifndef FloatingMedian_h
#define FloatingMedian_h

#include <Arduino.h>

class FloatingMedian {
    public:
        FloatingMedian(byte arraySize, byte peakArraySize);
        ~FloatingMedian();
        int processNextValue(int value);
        int getMedian();
    private:
        void _initArray();
        byte _arraySize;
        byte _peakArraySize;
        int* _sensorValues;
};

#endif
