#ifndef InitBoard_h
#define InitBoard_h

#include "DisplayController.h"

DisplayController* getDisplayController();
Configuration* getConfiguration();
Statistic* getStatistic();
SensorValueNormalizer* getSensorValueNormalizer();

void initDisplayController(PCD8544* lcd);
void initEeprom(byte version);
void releaseCommonResources();
float convertAnalogPressureToFloat(int pressure);

#endif
