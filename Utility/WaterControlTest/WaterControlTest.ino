#include "InitBoard.h"
#include <EEPROM.h>
#include "AUTest.h"
#include "DeltaLog.h"
#include "ButtonsController.h"
#include "ConfigurationMenu.h"
#include "PumpController.h"
#include "RadioController.h"
#include "FloatingMedian.h"
#include <avr/wdt.h>
#include "WatchdogProcessor.h"

#define PUMP_TEST_PIN 19
#define RELE_DELAY 50

PCD8544 lcd;
PumpController* pumpController;
DeltaLog* deltaLog;
RadioController* radioController;

void setup() {
  // Watchdog initial configuration
  MCUSR = 0;
  wdt_disable();
  
  Serial.begin(9600);
  Serial.println(F("Tests are started..."));
  
  pinMode(DIODE_ADDR_LOW, OUTPUT);
  pinMode(DIODE_ADDR_HIGH, OUTPUT);
  pinMode(WATER_PUMP_PIN, OUTPUT);
  pinMode(SCREEN_HIGHLIGHT_PIN, OUTPUT);
  pinMode(WTPS_PIN, INPUT);
  pinMode(PPS_PIN, INPUT);

  #ifndef MANUAL_E2E_TEST
    #ifdef EEPROM_SAVING_TEST
      processTestResult(testNextVersionSaving(), F("Test next version saving"));
      processTestResult(testDefaultVersionSaving(), F("Test default version saving"));
      processTestResult(testConfigurationInitializing(), F("Test configuration was initialized"));
      processTestResult(testConfigurationPropertiesReadWrite(), F("Test configuration values updates"));
      processTestResult(testConfigurationPropertiesPreserving(), F("Test configuraion values preserving"));
      processTestResult(testStatisticWasInitialized(), F("Test statistic initialization"));
      processTestResult(testStatisticReadWrite(), F("Test statistic values updates"));
      processTestResult(testStatisticPropertiesPreserving(), F("Test statistic values preserving"));
      processTestResult(testDeltaLogWriteForBigger(), F("Test DeltaLog write of bigger value"));
      processTestResult(testDeltaLogWillNotWriteForLess(), F("Test DeltaLog write of less value"));
      processTestResult(testDeltaLogBiggerValueWillBePreserved(), F("Test biggest value will be preserved between restarts"));
      processTestResult(testFirstDeltaLogEntryWillNotCorruptStatistic(), F("Test first DeltaLog entry will not corrupt statistic"));
      processTestResult(testFirstDeltaLogEntryWillBeReadCorrectly(), F("Test first DeltaLog entry will be read correctly"));
    #else
      initEeprom(EEPROM_DATA_VERSION + 1);
      _initEeprom(EEPROM_DATA_VERSION);
    #endif

    deltaLog = new DeltaLog();
    pumpController = new PumpController(deltaLog);
    #ifndef UNIT_TEST
      initDisplayController(&lcd);
      radioController = new RadioController(getStatistic(), deltaLog);
    #endif
    initButtons();
    
    #ifdef DELTA_LOG_TEST
      processTestResult(testInitialValueAfterFirstTurnOnWillBeZero(), F("Test DeltaLog will work correctly after first turn on"));
      processTestResult(testIterator(), F("Test DeltaLog iterator reading"));
      processTestResult(testLogRoll(), F("Test DeltaLog roll"));
      processTestResult(testIteratorAfterRoll(), F("Test Iterator after DeltaLog roll"));
      processTestResult(testDeltaLogIterator(5, 5), F("Test Iterator size without roll"));
      processTestResult(testDeltaLogIterator(200, 6), F("Test Iterator size with roll"));
      int logEntriesCount = (EEPROM_SIZE - getStatistic() -> getStructOffset()) / sizeof(DeltaEntry);
      processTestResult(testDeltaLogIterator(logEntriesCount - 1, 7), F("Test Iterator when next item is last"));
      processTestResult(testDeltaLogIterator(logEntriesCount, 8), F("Test Iterator when all eeprom filled and next item is first"));
      processTestResult(testDeltaLogIterator(0, 9), F("Test Iterator when no records were created"));
    #endif
  
    #ifdef MEMORY_LEAK_TEST
      processTestResult(testConfigurationDestructor(), F("Test memory leaks for Configuration"));
      processTestResult(testStatisticDestructor(), F("Test memory leaks for Statistic"));
      processTestResult(testPumpControllerDestructor(), F("Test memory leaks for Pump Controller"));
      processTestResult(testMemoryLeakForWatchdogProcessor(), F("Test memory leaks for WatchdogProcessor"));
      processTestResult(testAnalogButtonsDestructor(), F("Test memory leaks for Analog Buttons"));
      processTestResult(testDeltaLogDestructor(), F("Test memory leaks for display controller for DeltaLog"));
      processTestResult(testDisplayControllerDestructor(), F("Test memory leaks for display controller"));
      processTestResult(testFloatingMedianDestructor(), F("Test memory leaks for FloatingMedian"));
      processTestResult(testSensorValueNormalizerDestructor(), F("Test memory leaks for SensorValueNormalizer"));
      processTestResult(testSensorValueNormalizerProcessingDestructor(), F("Test memory leaks during processing values in FloatingMedian"));
    #endif

    #ifdef REGULAR_SCREEN_TESTS
      refreshDisplayController();
      processTestResult(testDisplayControllerWithoutErrorsWithZeroValues(), F("Test regular screen with zero values, without errors"));
      processTestResult(testGreenLedLightIfNoErrors(), F("Test green diode light"));
      processTestResult(testDisplayControllerWithoutErrorsWithNonZeroValues(), F("Test regular screen with non zero values, without errors"));
      processTestResult(testDisplayControllerError(PpsWarning, F("PPS warning (PPS WARNING) displayed?")), F("Test regular screen with PPS warning"));
      processTestResult(assertManualAccept(F("Yellow diode highlighted?")), F("Test diode PPS warning"));
      processTestResult(testDisplayControllerError(ErrorTooBigPpsValue, F("Too big PPS value error (CLOGGED) displayed?")), F("Test regular screen with too big PPS value error"));
      processTestResult(assertManualAccept(F("Red diode highlighted?")), F("Test diode too big PPS value error"));
      processTestResult(testDisplayControllerError(SensorsRestrictionViolation, F("Corrupted sensors error (BROKEN SENSOR) displayed?")), F("Test regular screen with corrupted sensors error"));
      processTestResult(assertManualAccept(F("Red diode highlighted?")), F("Test diode corrupted sensors error"));
      processTestResult(assertManualAccept(F("Red diode highlighted?")), F("Test diode for not enough memory error"));
      processTestResult(testDisplayControllerWatchdogCounter(), F("Test regular screen with displayed watchdog counter"));
    #endif
    
    #ifdef CONFIGURATION_MENU_ROLL_TESTS
      processTestResult(testRollMenuDown(), F("Test roll menu down"));
      processTestResult(testRollMenuUp(), F("Test roll menu up"));
    #endif

    #ifdef CONFIGURATION_MENU_ITEMS_TESTS
      refreshDisplayController();
      processTestResult(testWtpsMinCorrectDisplaying(), F("Test WTPS min displaying"));
      processTestResult(testWtpsMinCorrectIncreasing(), F("Test increased WTPS min displaying"));
      processTestResult(testWtpsMinCorrectDecreasing(), F("Test decreased WTPS min displaying"));
      processTestResult(testWtpsMaxCorrectDisplaying(), F("Test WTPS max displaying"));
      processTestResult(testWtpsMaxCorrectIncreasing(), F("Test increased WTPS max displaying"));
      processTestResult(testWtpsMaxCorrectDecreasing(), F("Test decreased WTPS max displaying"));
      processTestResult(testPpsYellowCorrectDisplaying(), F("Test PPS yellow displaying"));
      processTestResult(testPpsYellowCorrectIncreasing(), F("Test increased PPS yellow displaying"));
      processTestResult(testPpsYellowCorrectDecreasing(), F("Test decreased PPS yellow displaying"));
      processTestResult(testPpsRedCorrectDisplaying(), F("Test PPS red displaying"));
      processTestResult(testPpsRedCorrectIncreasing(), F("Test increased PPS red displaying"));
      processTestResult(testPpsRedCorrectDecreasing(), F("Test decreased PPS red displaying"));
      processTestResult(testContrastDisplaying(), F("Test contrast displaying"));
      processTestResult(testContrastCorrectIncreasing(), F("Test increased contrast displaying"));
      processTestResult(testContrastCorrectDecreasing(), F("Test decreased contrast displaying"));
      processTestResult(testLogDisplaying(), F("Test log displaying"));
      processTestResult(testLogDecreasing(), F("Test changed down log correct displaying"));
      processTestResult(testLogIncreasing(), F("Test changed up log correct displaying"));
      processTestResult(testCalibrationMenuDisplaying(), F("Test Calibration menu displaying"));
      processTestResult(testAutoZeroCalibrationMenuDisplaying(), F("Test Zero Auto Calibration menu displaying"));
      processTestResult(testWtpsZeroManualCalibrationMenuDisplaying(), F("Test WTPS 0 man menu displaying"));
      processTestResult(testWtpsZeroManualCalibrationIncreasing(), F("Test WTPS 0 man increasing"));
      processTestResult(testWtpsZeroManualCalibrationDecreasing(), F("Test WTPS 0 man decreasing"));
      processTestResult(testWtpsZeroManualCalibrationSensorRefreshing(), F("Test WTPS 0 man sensor refresh"));
      processTestResult(testPpsZeroManualCalibrationMenuDisplaying(), F("Test PPS 0 man menu displaying"));
      processTestResult(testPpsZeroManualCalibrationIncreasing(), F("Test PPS 0 man increasing"));
      processTestResult(testPpsZeroManualCalibrationDecreasing(), F("Test PPS 0 man decreasing"));
      processTestResult(testPpsZeroManualCalibrationSensorRefreshing(), F("Test PPS 0 man sensor refresh"));
      processTestResult(testNonZeroAutoCalibrationDisplaying(), F("Test X Auto Calibration displaying"));
      processTestResult(testNonZeroAutoCalibrationIncreasing(), F("Test Manual Calibration increasing"));
      processTestResult(testNonZeroAutoCalibrationDecreasing(), F("Test Manual Calibration decreasing"));
      processTestResult(testWtpsNonZeroManualCalibrationMenuDisplaying(), F("Test WTPS X man menu displaying"));
      processTestResult(testWtpsNonZeroManualCalibrationIncreasing(), F("Test WTPS X man increasing"));
      processTestResult(testWtpsNonZeroManualCalibrationDecreasing(), F("Test WTPS X man decreasing"));
      processTestResult(testWtpsNonZeroManualCalibrationSensorRefreshing(), F("Test WTPS X man sensor refresh"));
      processTestResult(testManualWtpsAdjustmentWillBeAppliedToPressureCalculationImmediatelly(), F("Test WTPS X man will influence on pressure calculation immediatelly"));
      processTestResult(testPpsNonZeroManualCalibrationMenuDisplaying(), F("Test PPS X man menu displaying"));
      processTestResult(testPpsNonZeroManualCalibrationIncreasing(), F("Test PPS X man increasing"));
      processTestResult(testPpsNonZeroManualCalibrationDecreasing(), F("Test PPS X man decreasing"));
      processTestResult(testPpsNonZeroManualCalibrationSensorRefreshing(), F("Test PPS X man sensor refresh"));
      processTestResult(testManualPpsAdjustmentWillBeAppliedToPressureCalculationImmediatelly(), F("Test PPS X man will influence on pressure calculation immediatelly"));
    #endif

    #ifdef CONFIGURATION_MENU_NAVIGATION_TESTS
      processTestResult(testMenuButtonClick(), F("Test root configuration menu entering"));
      processTestResult(testOkButtonClick(), F("Test menu navigation"));
      processTestResult(testEscButtonClick(), F("Test returning to root menu"));
      processTestResult(testConfigurationMenuExit(), F("Test exit from configuration menu"));
      processTestResult(testExitFromMenuByDelay(), F("Test exit from configuration menu by delay"));
      processTestResult(testExitFromDeepMenuByDelay(), F("Test exit from deep configuration menu by delay"));
      processTestResult(testDownActionWillMoveCursorDown(), F("Test move one position down"));
      processTestResult(testDownActionWillRollTheList(), F("Test move out of the list end"));
      processTestResult(testUpActionWillMoveCursorUp(), F("Test move one position up"));
      processTestResult(testUpActionWillRollTheList(), F("Test move out of the list start"));
      processTestResult(testRollForNumericValueUp(), F("Test numeric value rolling up"));
      processTestResult(testRollForNumericValueDown(), F("Test numeric value rolling down"));
      processTestResult(testDisplayContrastApplying(), F("Test display contrast applying"));
      // Restore initial display contrast
      getConfiguration() -> setConfigurationValue(DisplayContrast, 60);
      refreshDisplayController();
    #endif

    #ifdef CONFIGURATION_VALUES_SAVING
      processTestResult(testWtpsMinValueSaving(), F("Test WTPS min value saving"));
      processTestResult(testWtpsMinValueRollback(), F("Test WTPS min value rollback"));
      processTestResult(testWtpsMaxValueSaving(), F("Test WTPS max value saving"));
      processTestResult(testWtpsMaxValueRollback(), F("Test WTPS max value rollback"));
      processTestResult(testPpsYellowValueSaving(), F("Test PPS yellow value saving"));
      processTestResult(testPpsYellowValueRollback(), F("Test PPS yellow value rollback"));
      processTestResult(testPpsRedValueSaving(), F("Test PPS red value saving"));
      processTestResult(testPpsRedValueRollback(), F("Test PPS red value rollback"));
      processTestResult(testDisplayContrastValueSaving(), F("Test display contrast value saving"));
      processTestResult(testDisplayContrastValueRollback(), F("Test display contrast value rollback"));
      processTestResult(testLogEnabledValueSaving(), F("Test log enabling value saving"));
      processTestResult(testLogEnabledValueRollback(), F("Test log enabling value rollback"));
      processTestResult(testAutoZeroCalibrationValueSaving(), F("Test auto zero values saving"));
      processTestResult(testAutoZeroCalibrationValueRollback(), F("Test auto zero values rollback"));
      processTestResult(testWtpsZeroManualCalibrationValueSaving(), F("Test WTPS 0 manual calibration value saving"));
      processTestResult(testWtpsZeroManualCalibrationValueRollback(), F("Test WTPS 0 manual calibration value rollback"));
      processTestResult(testPpsZeroManualCalibrationValueSaving(), F("Test PPS 0 manual calibration value saving"));
      processTestResult(testPpsZeroManualCalibrationValueRollback(), F("Test PPS 0 manual calibration value rollback"));
      processTestResult(testNonZeroAutoCalibrationPredefinedPressureValueSaving(), F("Test auto non 0 predefined pressure value saving"));
      processTestResult(testNonZeroAutoCalibrationPredefinedPressureValueRollback(), F("Test auto non 0 predefined pressure value rollback"));
      processTestResult(testManualCalibrationValueSaving(), F("Test Manual Calibration value saving"));
      processTestResult(testManualCalibrationValueRollback(), F("Test Manual Calibration value rollback"));
      processTestResult(testWtpsNonZeroManualCalibrationValueSaving(), F("Test WTPS X manual calibration value saving"));
      processTestResult(testWtpsNonZeroManualCalibrationValueRollback(), F("Test WTPS X manual calibration value saving"));
      processTestResult(testPpsNonZeroManualCalibrationValueSaving(), F("Test PPS X manual calibration value saving"));
      processTestResult(testPpsNonZeroManualCalibrationValueRollback(), F("Test PPS X manual calibration value saving"));
      processTestResult(testRollForBooleanValueUp(), F("Test boolean value rolling up"));
      processTestResult(testRollForBooleanValueDown(), F("Test boolean value rolling down"));
    #endif

    #ifdef SCREEN_HIGHLIGHT
      processTestResult(testScreenHighlightOff(), F("Test screen is off"));
      processTestResult(testScreenHighlightOn(), F("Test screen is on"));
      processTestResult(testButtonClickWillTurnOnHighlight(), F("Test buttons will turn on highlight"));
      processTestResult(testAutoOffScreenHiglight(), F("Test auto turn off highlight"));
    #endif

    #ifdef RESET_BUTTON_FUNCTIONALITY
      processTestResult(testResetFiltersButton(), F("Test reset will clear errors and PPS Cycle Max"));
      refreshDisplayController();
      processTestResult(testManuallyResetFiltersButton(), F("Test screen and diodes will be updated"));
    #endif

    #ifdef PUMP_CONTROLLER_1
      pinMode(PUMP_TEST_PIN, INPUT_PULLUP);
      processTestResult(testAfterPowerOnInitiallyPumpIsOff(), F("Test right after device power on - pump is off"));
      processTestResult(testPumpWillOnIfPressureBecomeLowerThenConfigured(), F("Test correct processing of low wtps boundary"));
      processTestResult(testPumpWillBeOnIfWtpsBetweenMinAndMax(), F("Test pump will be on if WTPS between min and max"));
      processTestResult(testPumpWillOffIfPressureBecomeBiggerThenConfigured(), F("Test correct processing of up wtps boundary"));
      processTestResult(testPumpWillBeStillOnForPpsYellow(), F("Test Yellow PPS level crossing"));
      processTestResult(testPumpWillBeBlockedIfRedLevelCrossed(), F("Test Red PPS level crossing"));
      processTestResult(testPumpWillBeBlockedWithAnyError(), F("Test pump will be blocked with any error"));
      processTestResult(testWtpsBiggerPpsWillTurnOffPumpAfterConfiguredDuration(), F("Test pump will be turned off if PPS lower WTPS"));
      processTestResult(testWtpsBiggerPpsWillNotTurnOffPumpBeforeConfiguredDuration(), F("Test pump will not be turned off if PPS lower WTPS less then configured duration"));
    #endif

    #ifdef PUMP_CONTROLLER_2
      pinMode(PUMP_TEST_PIN, INPUT_PULLUP);
      processTestResult(testWtpsBiggerPpsWillSetSensorsRestrictionViolationError(), F("Test SensorsRestrictionViolation error will be set if PPS lower WTPS"));
      processTestResult(testSensorDiffWillBeSavedIfItBiggerThenPrevious(), F("Test bigger delta pressure will be logged"));
      processTestResult(testSensorDiffWillNotBeSavedIfItEqualsOrLessPrevious(), F("Test equal or lower delta pressure will not be logged"));
      processTestResult(testPpsCycleMaxWillBeUpdatedIfBigger(), F("Test PPS cycle max will be updated if new PPS value bigger"));
      processTestResult(testPpsCycleMaxWillBeTheSameIfEqualsOrLess(), F("Test PPS cycle max will be the same if new PPS value lower or the same"));
    #endif

    #ifdef UNIT_TEST
      processTestResult(testAnalogPressureConverterWillReturnZeroForNegativeInput(), F("Test analog pressure converter for negative values"));
      processTestResult(testAnalogPressureConverterWillReturnValidPressure(), F("Test analog pressure converter for positive values"));
      processTestResult(testWtpsSensorValueNormalizerWillBeInitializedWithDefaultValues(), F("Test initialization of WTPS sensor value normalizer"));
      processTestResult(testPpsSensorValueNormalizerWillBeInitializedWithDefaultValues(), F("Test initialization of PPS sensor value normalizer"));
      processTestResult(testWtpsSensorValueNormalizerWillProduceCorrectResults(), F("Test WTPS sensor value normalization"));
      processTestResult(testPpsSensorValueNormalizerWillProduceCorrectResults(), F("Test PPS sensor value normalization"));
    #endif

    #ifdef ERROR_CODE_TEST
      processTestResult(testStatisticResetErrorWillBeAbleToSetNoErrorCodeRegardlessCurrentErrorCode(), F("Test error code incorrect override (statistic)"));
      processTestResult(testErrorsWithHighPriorityCanOverrideLowerPriorityErrors(), F("Test error code correct override (statistic)"));
      processTestResult(testErrorCodeCannotBeChangedToTheCodeWithTheSamePriorityOrDowngraded(), F("Test error code reset (statistic)"));
    #endif

    #ifdef WATCHDOG_TEST
      processTestResult(testWatchdogProcessorWillIncreaseWatchdogCounterForWdtReset(), F("Test WatchdogProcessor will increase WatchdogCounter for WdtReset"));
      processTestResult(testWatchdogProcessorWillNotIncreaseWatchdogCounterForNonWdtReset(), F("Test WatchdogProcessor will not increase WatchdogCounter for non WdtReset"));
    #endif

    #ifdef WATCHDOG_REBOOT_TEST
      processTestResult(testResetWillNotLeadToInfiniteLoop(), F("Test reset will not lead to i nfinite loop. Normally this message should not be displayed"));
    #endif
    
  #endif
  
  #ifdef MANUAL_E2E_TEST
    initEeprom(EEPROM_DATA_VERSION + 1);
    _initEeprom(EEPROM_DATA_VERSION);
    getStatistic() -> resetErrorCode();
    getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
    getStatistic() -> setPpsCycleMax(6000);
    deltaLog = new DeltaLog();
    pumpController = new PumpController(deltaLog);
    initDisplayController(&lcd);
    initButtons();
    radioController = new RadioController(getStatistic(), deltaLog);
    #ifdef RADIO_TEST
      processTestResult(prepareTestDataForRadio(deltaLog), F("Test radio data preparation"));
    #endif
  #endif
    
  printResults();
  
  wdt_enable(WDTO_4S);
}

void loop() {
// Calibrate sensor resistors
//  Serial.println(analogRead(WTPS_PIN));
//  Serial.println(analogRead(PPS_PIN));
  wdt_reset();
  #ifndef UNIT_TEST
    radioController -> check();
    getDisplayController() -> check(2600, 3700);
  #endif
  getAnalogButtons() -> check();
  pumpController -> check(2600, 3700);
}

bool testDefaultVersionSaving() {
  _initEeprom(EEPROM_DATA_VERSION);
  return testVersionSaving(EEPROM_DATA_VERSION);
}

bool testNextVersionSaving() {
  initEeprom(EEPROM_DATA_VERSION + 1);
  return testVersionSaving(EEPROM_DATA_VERSION + 1);
}

bool testVersionSaving(byte expectedVersion) {
  byte savedVersion;
  EEPROM.get(0, savedVersion);
  return expectedVersion == savedVersion;
}

bool testConfigurationInitializing() {
  Configuration* config = getConfiguration();
  bool result = true;
  result = result && assertEquals(config -> getWtpsMin(), 2500, F("Wrong WtpsMin"));
  result = result && assertEquals(config -> getWtpsMax(), 3500, F("Wrong WtpsMax"));
  result = result && assertEquals(config -> getPpsRed(), 4500, F("Wrong PpsRed"));
  result = result && assertEquals(config -> getPpsYellow(), 4000, F("Wrong PpsYellow"));
  result = result && assertEquals(config -> isLogEnabled(), true, F("Wrong LogEnabled"));
  result = result && assertEquals(config -> getDisplayContrast(), 60, F("Wrong DisplayContrast"));
  result = result && assertEquals(config -> getWtpsOffset(), 100, F("Wrong WtpsOffset"));
  result = result && assertEquals(config -> getPpsOffset(), 100, F("Wrong PpsOffset"));
  result = result && assertEquals(config -> getMaxWtpsPressure(), 11843, F("Wrong MaxWtpsPressure"));
  result = result && assertEquals(config -> getMaxPpsPressure(), 11843, F("Wrong MaxPpsPressure"));
  result = result && assertEquals(config -> getConfigurationPressure(), 3300, F("Wrong ConfigurationPressure"));
  return result;
}

bool testConfigurationPropertiesReadWrite() {
  bool result = true;
  result = result && _checkIntConfigurationPropertyReadWrite(WtpsMin, F("Wrong WtpsMin"));
  result = result && _checkIntConfigurationPropertyReadWrite(WtpsMax, F("Wrong WtpsMax"));
  result = result && _checkIntConfigurationPropertyReadWrite(PpsYellow, F("Wrong PpsYellow"));
  result = result && _checkIntConfigurationPropertyReadWrite(PpsRed, F("Wrong PpsRed"));
  result = result && _checkIntConfigurationPropertyReadWrite(DisplayContrast, F("Wrong DisplayContrast"));
  result = result && _checkIntConfigurationPropertyReadWrite(WtpsOffset, F("Wrong WtpsOffset"));
  result = result && _checkIntConfigurationPropertyReadWrite(PpsOffset, F("Wrong PpsOffset"));
  result = result && _checkIntConfigurationPropertyReadWrite(MaxWtpsPressure, F("Wrong MaxWtpsPressure"));
  result = result && _checkIntConfigurationPropertyReadWrite(MaxPpsPressure, F("Wrong MaxPpsPressure"));
  result = result && _checkBoolConfigurationPropertyReadWrite(LogEnabled, F("Wrong LogEnabled"));
  result = result && _checkIntConfigurationPropertyReadWrite(ConfigurationPressure, F("Wrong ConfigurationPressure"));
  return result;
}

bool _checkIntConfigurationPropertyReadWrite(ConfigurationProperty configurationProperty, const __FlashStringHelper* errorMessage) {
  int expectedPropValue = _increaseIntConfigurationValue(configurationProperty);
  int actualPropValue = getConfiguration() -> getIntConfigurationValue(configurationProperty);
  return assertEquals(actualPropValue, expectedPropValue, errorMessage);
}

int _increaseIntConfigurationValue(ConfigurationProperty configurationProperty) {
  int propValue = getConfiguration() -> getIntConfigurationValue(configurationProperty);
  propValue = propValue + 1;
  getConfiguration() -> setConfigurationValue(configurationProperty, propValue);
  return propValue;
}

bool _checkBoolConfigurationPropertyReadWrite(ConfigurationProperty configurationProperty, const __FlashStringHelper* errorMessage) {
  bool expectedPropValue = _changeBoolConfigurationValue(configurationProperty);
  bool actualPropValue = getConfiguration() -> getBoolConfigurationValue(configurationProperty);
  return assertEquals(actualPropValue, expectedPropValue, errorMessage);
}

bool _changeBoolConfigurationValue(ConfigurationProperty configurationProperty) {
  bool propValue = getConfiguration() -> getBoolConfigurationValue(configurationProperty);
  propValue = !propValue;
  getConfiguration() -> setConfigurationValue(configurationProperty, propValue);
  return propValue;
}

bool testConfigurationPropertiesPreserving() {
  bool result = _checkPreservedIntConfigurationValue(WtpsMin, F("Wrong WtpsMin"));
  result = result && _checkPreservedIntConfigurationValue(WtpsMax, F("Wrong WtpsMax"));
  result = result && _checkPreservedIntConfigurationValue(PpsYellow, F("Wrong PpsYellow"));
  result = result && _checkPreservedIntConfigurationValue(PpsRed, F("Wrong PpsRed"));
  result = result && _checkPreservedIntConfigurationValue(DisplayContrast, F("Wrong DisplayContrast"));
  result = result && _checkPreservedIntConfigurationValue(WtpsOffset, F("Wrong WtpsOffset"));
  result = result && _checkPreservedIntConfigurationValue(PpsOffset, F("Wrong PpsOffset"));
  result = result && _checkPreservedIntConfigurationValue(MaxWtpsPressure, F("Wrong MaxWtpsPressure"));
  result = result && _checkPreservedIntConfigurationValue(MaxPpsPressure, F("Wrong MaxPpsPressure"));
  result = result && _checkPreservedBoolConfigurationValue(LogEnabled, F("Wrong LogEnabled"));
  result = result && _checkPreservedIntConfigurationValue(ConfigurationPressure, F("Wrong ConfigurationPressure"));
  return result;
}

bool _checkPreservedIntConfigurationValue(ConfigurationProperty configurationProperty, const __FlashStringHelper* errorMessage) {
  int expectedPropValue = _increaseIntConfigurationValue(configurationProperty);
  _initEeprom(EEPROM_DATA_VERSION);
  int actualPropValue = getConfiguration() -> getIntConfigurationValue(configurationProperty);
  return assertEquals(actualPropValue, expectedPropValue, errorMessage);
}

bool _checkPreservedBoolConfigurationValue(ConfigurationProperty configurationProperty, const __FlashStringHelper* errorMessage) {
  bool expectedPropValue = _changeBoolConfigurationValue(configurationProperty);
  _initEeprom(EEPROM_DATA_VERSION);
  bool actualPropValue = getConfiguration() -> getBoolConfigurationValue(configurationProperty);
  return assertEquals(actualPropValue, expectedPropValue, errorMessage);
}

bool testStatisticWasInitialized() {
  bool result = true;
  result = result && assertEquals(getStatistic() -> getPpsCycleMax(), 0, F("Wrong PpsCycleMax"));
  result = result && assertEquals(getStatistic() -> getIndex(), 0, F("Wrong Index"));
  result = result && assertEquals(getStatistic() -> getErrorCode(), 0, F("Wrong ErrorCode"));
  result = result && assertEquals(getStatistic() -> getPumpCounter(), (unsigned long) 0, F("Wrong PumpCounter"));
  result = result && assertEquals(getStatistic() -> getWatchdogCounter(), (unsigned long) 0, F("Wrong WatchdogCounter"));
  return result;
}

void _initEeprom(byte version) {
  releaseCommonResources();
  initEeprom(version);
}

bool testStatisticReadWrite() {
  bool result = true;
  
  Statistic* statistic  = getStatistic();
  
  int ppsCycleMax = statistic -> getPpsCycleMax();
  statistic -> setPpsCycleMax(ppsCycleMax + 1);
  result = result && assertEquals(statistic -> getPpsCycleMax(), ppsCycleMax + 1, F("Wrong PpsCycleMax"));

  int index = statistic -> getIndex();
  statistic -> setIndex(index + 1);
  result = result && assertEquals(statistic -> getIndex(), index + 1, F("Wrong Index"));

  EErrorCode errorCode = statistic -> getErrorCode();
  result = result && assertEquals(errorCode, NoError, F("Wrong initial value of ErrorCode"));
  statistic -> resetErrorCode();
  statistic -> setErrorCode(ErrorTooBigPpsValue);
  result = result && assertEquals(statistic -> getErrorCode(), ErrorTooBigPpsValue, F("Wrong ErrorCode"));

  unsigned long pumpCounter = statistic -> getPumpCounter();
  statistic -> increasePumpCounter();
  result = result && assertEquals(statistic -> getPumpCounter(), pumpCounter + 1, F("Wrong PumpCounter"));

  unsigned long watchdogCounter = statistic -> getWatchdogCounter();
  statistic -> increaseWatchdogCounter();
  result = result && assertEquals(statistic -> getWatchdogCounter(), watchdogCounter + 1, F("Wrong WatchdogCounter"));
  
  return result;
}

bool testStatisticPropertiesPreserving() {
   bool result = true;
  
  Statistic* statistic  = getStatistic();
  
  int ppsCycleMax = statistic -> getPpsCycleMax();
  statistic -> setPpsCycleMax(ppsCycleMax + 1);
  _initEeprom(EEPROM_DATA_VERSION);
  result = result && assertEquals(statistic -> getPpsCycleMax(), ppsCycleMax + 1, F("Wrong preserved PpsCycleMax"));

  int index = statistic -> getIndex();
  statistic -> setIndex(index + 1);
  _initEeprom(EEPROM_DATA_VERSION);
  result = result && assertEquals(statistic -> getIndex(), index + 1, F("Wrong preserved Index"));

  EErrorCode errorCode = statistic -> getErrorCode();
  result = result && assertEquals(errorCode, ErrorTooBigPpsValue, F("Wrong initial value of ErrorCode"));
  statistic -> resetErrorCode();
  _initEeprom(EEPROM_DATA_VERSION);
  result = result && assertEquals(statistic -> getErrorCode(), NoError, F("Wrong preserved ErrorCode"));

  unsigned long pumpCounter = statistic -> getPumpCounter();
  result = result && assertEquals(pumpCounter, (unsigned long) 0, F("Wrong initial value of PumpCounter"));
  statistic -> increasePumpCounter();
  statistic -> increasePumpCounter();
  statistic -> increasePumpCounter();
  statistic -> increasePumpCounter();
  statistic -> increasePumpCounter();
  statistic -> increasePumpCounter();
  statistic -> increasePumpCounter();
  statistic -> increasePumpCounter();
  statistic -> increasePumpCounter();
  statistic -> increasePumpCounter();
  statistic -> increasePumpCounter();
  _initEeprom(EEPROM_DATA_VERSION);
  result = result && assertEquals(statistic -> getPumpCounter(), (unsigned long) 10, F("Wrong preserved PumpCounter"));

  unsigned long watchdogCounter = statistic -> getWatchdogCounter();
  statistic -> increaseWatchdogCounter();
   _initEeprom(EEPROM_DATA_VERSION);
  result = result && assertEquals(statistic -> getWatchdogCounter(), watchdogCounter + 1, F("Wrong preserved WatchdogCounter"));

  return result;
}

bool testDeltaLogDestructor() {
  for (int i = 0; i < 2049; i++) {
    DeltaLog* deltaLog = new DeltaLog();
    delete deltaLog;
  }
  return true;
}

bool testDisplayControllerDestructor() {
  for( int i = 0; i < 2049; i++) {
    refreshDisplayController();
    Serial.print(F("."));
    if ((i + 1) % 100 == 0) {
      Serial.println();
    }
  }
  Serial.println();
  return true;
}

bool testFloatingMedianDestructor() {
  for( int i = 0; i < 2049; i++) {
    FloatingMedian* floatingMedian = new FloatingMedian(11, 2);
    delete floatingMedian;
  }
  return true;
}

bool testConfigurationDestructor() {
  for( int i = 0; i < 2049; i++) {
    Configuration* configuration = new Configuration(sizeof(byte), false);
    delete configuration;
  }
  return true;
}

bool testMemoryLeakForWatchdogProcessor() {
  for( int i = 0; i < 2049; i++) {
    WatchdogProcessor* watchdogProcessor = new WatchdogProcessor;
    delete watchdogProcessor;
  }
  return true;
}

bool testStatisticDestructor() {
  for( int i = 0; i < 2049; i++) {
    Statistic* statistic = new Statistic(getConfiguration() -> getStructOffset(), false);
    delete statistic;
  }
  return true;
}

bool testPumpControllerDestructor() {
  DeltaLog* deltaLog = new DeltaLog();
  for( int i = 0; i < 2049; i++) {
    PumpController* pumpController = new PumpController(deltaLog);
    delete pumpController;
  }
  delete deltaLog;
  return true;
}

bool testAnalogButtonsDestructor() {
  for( int i = 0; i < 2049; i++) {
    Button* b1 = new Button(100);
    Button* b2 = new Button(200);
    Button* b3 = new Button(300);
    Button* b4 = new Button(400);
    AnalogButtons* analogButtons = new AnalogButtons(MENU_BUTTONS_PIN, 4);
    analogButtons -> add(b1);
    analogButtons -> add(b2);
    analogButtons -> add(b3);
    analogButtons -> add(b4);
    delete analogButtons; 
  }
  return true;
}

bool testSensorValueNormalizerDestructor() {
  for( int i = 0; i < 2049; i++) {
    SensorValueNormalizer* sensorValueNormalizer = new SensorValueNormalizer(1, 0, 0, 0);
    delete sensorValueNormalizer;
  }
  return true;
}

bool testSensorValueNormalizerProcessingDestructor() {
  FloatingMedian* floatingMedian = new FloatingMedian(13, 1);
  for( int i = 0; i < 2049; i++) {
    floatingMedian -> processNextValue(i);
  }
  delete floatingMedian;
  return true;
}

bool testDeltaLogWriteForBigger() {
  DeltaLog* deltaLog = new DeltaLog();
  int initialIndex = getStatistic() -> getIndex();
  deltaLog -> writeDeltaIfBigger(1);
  deltaLog -> writeDeltaIfBigger(2);
  deltaLog -> writeDeltaIfBigger(3);
  int index = getStatistic() -> getIndex();
  bool result = assertEquals(index - initialIndex, 3, F("Wrong index value, not all deltaLog was saved"));
  delete deltaLog;
  return result;
}

bool testDeltaLogWillNotWriteForLess() {
  DeltaLog* deltaLog = new DeltaLog();
  int initialIndex = getStatistic() -> getIndex();
  deltaLog -> writeDeltaIfBigger(2);
  deltaLog -> writeDeltaIfBigger(1);
  deltaLog -> writeDeltaIfBigger(0);
  int index = getStatistic() -> getIndex();
  bool result = assertEquals(index - initialIndex, 0, F("Wrong index value, deltaLog was saved by mistake"));
  delete deltaLog;
  return result;
}

bool testDeltaLogBiggerValueWillBePreserved() {
  DeltaLog* deltaLog = new DeltaLog();
  int initialIndex = getStatistic() -> getIndex();
  deltaLog -> writeDeltaIfBigger(100);
  int index = getStatistic() -> getIndex();
  bool result = assertEquals(index - initialIndex, 1, F("Wrong initial value for test"));
  delete deltaLog;
  deltaLog = new DeltaLog();
  deltaLog -> writeDeltaIfBigger(99);
  index = getStatistic() -> getIndex();
  result = result && assertEquals(index - initialIndex, 1, F("Less value should not be preserved"));
  delete deltaLog;
  deltaLog = new DeltaLog();
  deltaLog -> writeDeltaIfBigger(101);
  index = getStatistic() -> getIndex();
  result = result && assertEquals(index - initialIndex, 2, F("Bigger value should be preserved"));
  delete deltaLog;
  return result;
}

bool testFirstDeltaLogEntryWillNotCorruptStatistic() {
  _initEeprom(EEPROM_DATA_VERSION + 5);
  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(SensorsRestrictionViolation);
  unsigned long pumpCounter = getStatistic() -> getPumpCounter();
  int ppsCycleMax = getStatistic() -> getPpsCycleMax();
  int index = getStatistic() -> getIndex();
  EErrorCode errorCode = getStatistic() -> getErrorCode();
  DeltaLog* deltaLog = new DeltaLog();
  deltaLog -> writeDeltaIfBigger(5);
  bool result = assertEquals(getStatistic() -> getPumpCounter(), pumpCounter, F("PumpCounter corrupted"));
  result = result && assertEquals(getStatistic() -> getPpsCycleMax(), ppsCycleMax, F("PpsCycleMax corrupted"));
  result = result && assertEquals(getStatistic() -> getIndex(), index + 1, F("Index corrupted"));
  result = result && assertTrue(getStatistic() -> getErrorCode() == errorCode, F("ErrorCode corrupted"));
  delete deltaLog;
  return result;
}

bool testFirstDeltaLogEntryWillBeReadCorrectly() {
  _initEeprom(EEPROM_DATA_VERSION + 6);
  DeltaLog* deltaLog = new DeltaLog();
  DeltaLogIterator* iterator = deltaLog -> getIterator();
  byte count = 0;
  bool result = true;
  while (iterator -> hasNext()) {
    count++;
    DeltaEntry deltaEntry = iterator -> getNext();
    result = result && assertEquals(deltaEntry.deltaPressure, 0, F("Wrong initial delta entry"));
    result = result && assertEquals(deltaEntry.deltaTime, 0L, F("Wrong initial delta time"));
  }
  result = result && assertEquals(count, 1, F("Wrong count of initial DeltaEntries"));
  delete deltaLog;
  return result;
}

bool testInitialValueAfterFirstTurnOnWillBeZero() {
  _initEeprom(EEPROM_DATA_VERSION + 2);
  DeltaLog* deltaLog = new DeltaLog();
  int index = getStatistic() -> getIndex();
  bool result = assertEquals(index, 0, F("Initial index should be zero after EEPROM initialization"));
  deltaLog -> writeDeltaIfBigger(0);
  index = getStatistic() -> getIndex();
  result = result && assertEquals(index, 0, F("Wrong index value, 0 should not be saved"));
  deltaLog -> writeDeltaIfBigger(1);
  index = getStatistic() -> getIndex();
  result = result && assertEquals(index, 1, F("Wrong index value, 1 should be saved"));
  delete deltaLog;
  return result; 
}

bool testIterator() {
  _initEeprom(EEPROM_DATA_VERSION + 3);
  DeltaLog* deltaLog = new DeltaLog();
  byte i;
  for (i = 1; i < 6; i++) {
    delay(200);
    deltaLog -> writeDeltaIfBigger(i);
  }
  DeltaLogIterator* iterator = deltaLog -> getIterator();
  bool result = true;
  i = 0;
  DeltaEntry deltaEntry;
  while (iterator -> hasNext()) {
    deltaEntry = iterator -> getNext();
    result = result && assertEquals(deltaEntry.deltaPressure, i, F("Delta pressure should be consequtive"));
    if (i == 0) {
      result = result && assertEquals(deltaEntry.deltaTime, 0L, F("First Delta time should be 0"));
    } else {
      result = result && assertTrue(deltaEntry.deltaTime > 180 && deltaEntry.deltaTime < 220, F("Delta time should be approximately 200"));
    }
    i++;
  }
  delete deltaLog;
  return result;
}

bool testLogRoll() {
  _initEeprom(EEPROM_DATA_VERSION + 4);
  DeltaLog* deltaLog = new DeltaLog();
  int i;
  const int logEntriesCount = (EEPROM_SIZE - getStatistic() -> getStructOffset()) / sizeof(DeltaEntry);
  const int rollOffset = 20;
  for (i = 1; i < logEntriesCount + rollOffset; i++) {
    delay(20);
    deltaLog -> writeDeltaIfBigger(i);
  }
  DeltaLogIterator* iterator = deltaLog -> getIterator();
  bool result = true;
  i = rollOffset;
  DeltaEntry deltaEntry;
  while (iterator -> hasNext()) {
    deltaEntry = iterator -> getNext();
    result = result && assertEquals(deltaEntry.deltaPressure, i, F("Delta pressure should be consequtive"));
    result = result && assertTrue(deltaEntry.deltaTime > 5 && deltaEntry.deltaTime < 35, F("Delta time should be approximately 20"));
    i++;
  }
  result = result && assertEquals(i - rollOffset, logEntriesCount , F("Wrong number of delta entries"));
  delete deltaLog;
  return result;
}

// Should be executed after testLogRoll
bool testIteratorAfterRoll() {
  DeltaLog* deltaLog = new DeltaLog();
  const int logEntriesCount = (EEPROM_SIZE - getStatistic() -> getStructOffset()) / sizeof(DeltaEntry);
  const int rollOffset = 20;
  bool result = true;
  DeltaLogIterator* iterator = deltaLog -> getIterator();
  int i = rollOffset;
  DeltaEntry deltaEntry;
  while (iterator -> hasNext()) {
    deltaEntry = iterator -> getNext();
    result = result && assertEquals(deltaEntry.deltaPressure, i, F("Delta pressure should be consequtive"));
    result = result && assertTrue(deltaEntry.deltaTime > 5 && deltaEntry.deltaTime < 35, F("Delta time should be approximately 20"));
    i++;
  }
  result = result && assertEquals(logEntriesCount, i - rollOffset, F("Wrong number of delta entries"));
  delete deltaLog;
  return result;
}

bool testDeltaLogIterator(int size, byte eepromVersion) {
  _initEeprom(EEPROM_DATA_VERSION + eepromVersion);
  DeltaLog* deltaLog = new DeltaLog();
  for (int i = 1; i <= size; i++) {
    delay(20);
    deltaLog -> writeDeltaIfBigger(i);
  }
  DeltaLogIterator* iterator = deltaLog -> getIterator();
  const int MAX_LOG_ENTRIES_COUNT = ((EEPROM_SIZE - getStatistic() -> getStructOffset()) / sizeof(DeltaEntry));
  bool result = assertEquals(iterator -> getSize(), size < MAX_LOG_ENTRIES_COUNT ? size + 1 : MAX_LOG_ENTRIES_COUNT, F("Wrong size"));
  byte actualSize = 0;
  DeltaEntry deltaEntry;
  int expectedPressure = size < MAX_LOG_ENTRIES_COUNT ? 0 : ((size % MAX_LOG_ENTRIES_COUNT) + 1);
  while (iterator -> hasNext()) {
    deltaEntry = iterator -> getNext();
    actualSize++;
    result = result && assertEquals(deltaEntry.deltaPressure, expectedPressure, F("Delta pressure should be consequtive"));
    if (expectedPressure == 0) {
      result = result && assertEquals(deltaEntry.deltaTime, 0L, F("First Delta time should be 0"));
    } else {
      result = result && assertTrue(deltaEntry.deltaTime > 5 && deltaEntry.deltaTime < 35, F("Delta time should be approximately 20"));
    }
    expectedPressure++;
  }
  result = result && assertEquals(iterator -> getSize(), actualSize, F("Wrong returned size"));
  delete deltaLog;
  return result;
}

void refreshDisplayController() {
  delete getDisplayController();
  initDisplayController(&lcd);
}

bool testDisplayControllerWithoutErrorsWithZeroValues() {  
  getDisplayController() -> setDisplayRegularMenu(true);
  getDisplayController() -> check(0, 0);
  return assertManualAccept(F("Regular screen without errors with zero values displayed?"));
}

bool testGreenLedLightIfNoErrors() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  getDisplayController() -> check(0, 0);
  return assertManualAccept(F("Green diode is lighted?"));
}

bool testDisplayControllerWithoutErrorsWithNonZeroValues() {
  getStatistic() -> setPpsCycleMax(3610);
  getDisplayController() -> setDisplayRegularMenu(true);
  getDisplayController() -> check(2600, 2610);
  return assertManualAccept(F("Regular screen without errors with 2600/2610/3610 values displayed?"));
}

bool testDisplayControllerError(EErrorCode errorCode, const __FlashStringHelper* promptMessage) {
  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(errorCode);
  getDisplayController() -> setDisplayRegularMenu(true);
  getDisplayController() -> check(2600, 2610);
  return assertManualAccept(promptMessage);
}

bool testDisplayControllerWatchdogCounter() {
  getStatistic() -> increaseWatchdogCounter();
  getStatistic() -> increaseWatchdogCounter();
  getStatistic() -> increaseWatchdogCounter();
  getDisplayController() -> setDisplayRegularMenu(true);
  getDisplayController() -> check(2600, 2610);
  return assertManualAccept(F("Regular screen displayed Watchdog counter (3)?"));
}

bool testMenuButtonClick() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  getDisplayController() -> check(2600, 2610);
  return assertManualAccept(F("Root configuration menu displayed?"));
}

bool testOkButtonClick() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); // first click is for configuration menu entering
  okButtonClick(); // second click is for navigation/applying actions in the configuration menu
  getDisplayController() -> check(2600, 2610);
  return assertManualAccept(F("WTPS menu displayed?"));
}

bool testEscButtonClick() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); // first click is for configuration menu entering
  okButtonClick(); // second click is for navigation/applying actions in the configuration menu
  escButtonClick();
  getDisplayController() -> check(2600, 2610);
  return assertManualAccept(F("Root menu displayed?"));
}

bool testConfigurationMenuExit() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); // first click is for configuration menu entering
  okButtonClick(); // second click is for navigation/applying actions in the configuration menu
  escButtonClick(); // return to root
  escButtonClick(); // exit from configuration menu
  getDisplayController() -> check(2600, 2610);
  return assertManualAccept(F("Regular menu displayed?"));
}

bool testExitFromMenuByDelay() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); // first click is for configuration menu entering
  unsigned long startEventTime = millis();
  do {
    getDisplayController() -> check(2600, 2610);
  } while ((millis() - startEventTime) < (CLOSE_MENU_DELAY + 100));
  return assertManualAccept(F("Was root menu displayed and was it finally changed to regular menu?"));
}

bool testExitFromDeepMenuByDelay() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); // first click is for configuration menu entering
  okButtonClick(); // second click is for navigation/applying actions in the configuration menu
  unsigned long startEventTime = millis();
  do {
    getDisplayController() -> check(2600, 2610);
  } while ((millis() - startEventTime) < (CLOSE_MENU_DELAY + 100));
  return assertManualAccept(F("Was WTPS menu displayed and was it finally changed to regular menu?"));
}

bool testDownActionWillMoveCursorDown() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); // first click is for configuration menu entering
  downButtonClick(); // move one position down
  getDisplayController() -> check(2600, 2610);
  return assertManualAccept(F("Is second item selected in root menu?"));
}

bool testDownActionWillRollTheList() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); // first click is for configuration menu entering
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick(); // move 7 position down
  getDisplayController() -> check(2600, 2610);
  return assertManualAccept(F("Is 4th item selected in root menu?"));
}

bool testUpActionWillMoveCursorUp() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); // first click is for configuration menu entering
  upButtonClick(); // move one position up
  getDisplayController() -> check(2600, 2610);
  return assertManualAccept(F("Is third item selected in root menu?"));
}

bool testUpActionWillRollTheList() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); // first click is for configuration menu entering
  upButtonClick();
  upButtonClick();
  upButtonClick(); // move 3 position up
  getDisplayController() -> check(2600, 2610);
  return assertManualAccept(F("Is last item selected in root menu?"));
}

bool testRollMenuDown() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); // first click is for configuration menu entering
  for(byte i = 0; i < 5; i++) {
    downButtonClick();
    getDisplayController() -> check(2600, 2610);
    delay(1000);
  }
  return assertManualAccept(F("Is menu roll down properly?"));
}

bool testRollMenuUp() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); // first click is for configuration menu entering
  for(byte i = 0; i < 5; i++) {
    upButtonClick();
    getDisplayController() -> check(2600, 2610);
    delay(1000);
  }
  return assertManualAccept(F("Is menu roll up properly?"));
}

bool testWtpsMinCorrectDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  okButtonClick(); 
  okButtonClick();
  return assertManualAccept(F("Is WTPS min displayed?"));
}

bool testWtpsMinCorrectIncreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  okButtonClick(); 
  okButtonClick();
  upButtonClick();
  return assertManualAccept(F("Is WTPS min was increased?"));
}

bool testWtpsMinCorrectDecreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  okButtonClick(); 
  okButtonClick();
  downButtonClick();
  return assertManualAccept(F("Is WTPS min was decreased?"));
}

bool testWtpsMaxCorrectDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  okButtonClick(); 
  downButtonClick();
  okButtonClick();
  return assertManualAccept(F("Is WTPS max displayed?"));
}

bool testWtpsMaxCorrectIncreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  okButtonClick(); 
  okButtonClick();
  upButtonClick();
  return assertManualAccept(F("Is WTPS max was increased?"));
}

bool testWtpsMaxCorrectDecreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  okButtonClick(); 
  okButtonClick();
  downButtonClick();
  return assertManualAccept(F("Is WTPS max was decreased?"));
}

bool testPpsYellowCorrectDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  downButtonClick();
  okButtonClick(); 
  okButtonClick();
  return assertManualAccept(F("Is Pps yellow displayed?"));
}

bool testPpsYellowCorrectIncreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick(); 
  okButtonClick();
  upButtonClick();
  return assertManualAccept(F("Is Pps yellow was increased?"));
}

bool testPpsYellowCorrectDecreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  okButtonClick(); 
  okButtonClick();
  downButtonClick();
  return assertManualAccept(F("Is Pps yellow was decreased?"));
}

bool testPpsRedCorrectDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  okButtonClick();
  downButtonClick();
  okButtonClick();
  return assertManualAccept(F("Is Pps red displayed?"));
}

bool testPpsRedCorrectIncreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  return assertManualAccept(F("Is Pps red was increased?"));
}

bool testPpsRedCorrectDecreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  okButtonClick(); 
  okButtonClick();
  downButtonClick();
  return assertManualAccept(F("Is Pps red was decreased?"));
}

bool testContrastDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  downButtonClick();
  okButtonClick();
  return assertManualAccept(F("Is contrast displayed?"));
}

bool testContrastCorrectIncreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  upButtonClick();
  return assertManualAccept(F("Is contrast was increased?"));
}

bool testContrastCorrectDecreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  okButtonClick(); 
  downButtonClick();
  return assertManualAccept(F("Is contrast was decreased?"));
}

bool testLogDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick(); 
  downButtonClick();
  okButtonClick();
  return assertManualAccept(F("Is log displayed?"));
}

bool testLogIncreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  upButtonClick();
  return assertManualAccept(F("Is log was changed?"));
}

bool testLogDecreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  downButtonClick();
  return assertManualAccept(F("Is log was changed?"));
}

bool testCalibrationMenuDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  downButtonClick();
  okButtonClick();
  return assertManualAccept(F("Is calibration menu displayed?"));
}

bool testAutoZeroCalibrationMenuDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  return assertManualAccept(F("Is Set Zero menu displayed?"));
}

bool testWtpsZeroManualCalibrationMenuDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  return assertManualAccept(F("Is WTPS 0 man menu displayed?"));
}

bool testWtpsZeroManualCalibrationIncreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  return assertManualAccept(F("Is WTPS 0 was increased?"));
}

bool testWtpsZeroManualCalibrationDecreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  return assertManualAccept(F("Is WTPS 0 was decreased?"));
}

void _checkDisplayController() {
  if (Serial.available()) {
    Serial.read();
  }
  while (!Serial.available()) {
    int wtpsCurrentValue = getSensorValueNormalizer() -> normalizeWtpsSensorValue(analogRead(WTPS_PIN));
    int ppsCurrentValue = getSensorValueNormalizer() -> normalizePpsSensorValue(analogRead(PPS_PIN));
    getDisplayController() -> check(wtpsCurrentValue, ppsCurrentValue);
  }
}

bool testWtpsZeroManualCalibrationSensorRefreshing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  Serial.println("Try to update WTPS sensor");
  _checkDisplayController();
  return assertManualAccept(F("Is WTPS sensor updated?"));
}

bool testPpsZeroManualCalibrationMenuDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  return assertManualAccept(F("Is PPS 0 man menu displayed?"));
}

bool testPpsZeroManualCalibrationIncreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  return assertManualAccept(F("Is PPS 0 was increased?"));
}

bool testPpsZeroManualCalibrationDecreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  return assertManualAccept(F("Is PPS 0 was decreased?"));
}

bool testPpsZeroManualCalibrationSensorRefreshing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  Serial.println("Try to update PPS sensor");
  _checkDisplayController();
  return assertManualAccept(F("Is PPS sensor updated?"));;
}

bool testNonZeroAutoCalibrationDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  return assertManualAccept(F("Is X auto calibration displayed?"));
}

bool testNonZeroAutoCalibrationIncreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  return assertManualAccept(F("Is X auto calibration was increased?"));
}

bool testNonZeroAutoCalibrationDecreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  return assertManualAccept(F("Is x auto calibration was decreased?"));
}

bool testWtpsNonZeroManualCalibrationMenuDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  return assertManualAccept(F("Is WTPS X manual calibration displayed?"));
}

bool testWtpsNonZeroManualCalibrationIncreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  return assertManualAccept(F("Is WTPS X manual calibration was increased?"));
}

bool testWtpsNonZeroManualCalibrationDecreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  return assertManualAccept(F("Is WTPS X manual calibration was decreased?"));
}

bool testWtpsNonZeroManualCalibrationSensorRefreshing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  Serial.println("Try to update WTPS/PPS sensor");
  _checkDisplayController();
  return assertManualAccept(F("Is WTPS/PPS pressure updated?"));
}

void _requestAdjustSensor(int requestedValue, int sensorPin) {
  Serial.print(F("Set "));Serial.print(requestedValue);Serial.print(F(" for "));Serial.println(sensorPin == WTPS_PIN ? F("WTPS sensor") : F("PPS sensor"));
  if (Serial.available()) {
    Serial.read();
  }
  while (!Serial.available()) {
    Serial.println(analogRead(sensorPin));
    delay(1000);
    int wtpsCurrentValue = getSensorValueNormalizer() -> normalizeWtpsSensorValue(analogRead(WTPS_PIN));
    int ppsCurrentValue = getSensorValueNormalizer() -> normalizePpsSensorValue(analogRead(PPS_PIN));
    getDisplayController() -> check(wtpsCurrentValue, ppsCurrentValue);
    Serial.read();
  }
  Serial.read();
}

bool testManualWtpsAdjustmentWillBeAppliedToPressureCalculationImmediatelly() {
  getStatistic() -> resetErrorCode();
  int prevWtpsOffset = getConfiguration() -> getWtpsOffset();
  int prevMaxWtpsPressure = getConfiguration() -> getMaxWtpsPressure();
  getConfiguration() -> setConfigurationValue(WtpsOffset, 100);
  getConfiguration() -> setConfigurationValue(MaxWtpsPressure, 5000);
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  bool result = assertManualAccept(F("Is WTPS 0?"));
  _requestAdjustSensor(300, WTPS_PIN);
  result = assertManualAccept(F("Is WTPS 1.08?"));
  for (int i = 0; i < 500; i++) {
    downButtonClick();
    if (i % 10 == 0) {
      Serial.print(F("."));
    }
  }
  Serial.println();
  result &= assertManualAccept(F("Is WTPS 0.98?"));
  _requestAdjustSensor(100, WTPS_PIN);
  getConfiguration() -> setConfigurationValue(WtpsOffset, prevWtpsOffset);
  getConfiguration() -> setConfigurationValue(MaxWtpsPressure, prevMaxWtpsPressure);
  return result;
}

bool testPpsNonZeroManualCalibrationMenuDisplaying() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  return assertManualAccept(F("Is PPS X manual calibration displayed?"));
}

bool testPpsNonZeroManualCalibrationIncreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  return assertManualAccept(F("Is PPS X manual calibration was increased?"));
}

bool testPpsNonZeroManualCalibrationDecreasing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  return assertManualAccept(F("Is PPS X manual calibration was decreased?"));
}

bool testPpsNonZeroManualCalibrationSensorRefreshing() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  Serial.println("Try to update WTPS/PPS sensor");
  _checkDisplayController();
  return assertManualAccept(F("Is WTPS/PPS pressure updated?"));
}


bool testManualPpsAdjustmentWillBeAppliedToPressureCalculationImmediatelly() {
  getStatistic() -> resetErrorCode();
  int prevPpsOffset = getConfiguration() -> getPpsOffset();
  int prevMaxPpsPressure = getConfiguration() -> getMaxPpsPressure();
  getConfiguration() -> setConfigurationValue(PpsOffset, 100);
  getConfiguration() -> setConfigurationValue(MaxPpsPressure, 5000);
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  bool result = assertManualAccept(F("Is PPS 0?"));
  _requestAdjustSensor(300, PPS_PIN);
  result = assertManualAccept(F("Is PPS 1.08?"));
  for (int i = 0; i < 500; i++) {
    downButtonClick();
    if (i % 10 == 0) {
      Serial.print(F("."));
    }
  }
  Serial.println();
  result &= assertManualAccept(F("Is PPS 0.98?"));
  _requestAdjustSensor(100, PPS_PIN);
  return result;
  getConfiguration() -> setConfigurationValue(PpsOffset, prevPpsOffset);
  getConfiguration() -> setConfigurationValue(MaxPpsPressure, prevMaxPpsPressure);
}

bool testWtpsMinValueSaving() {
  refreshDisplayController();
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialWtpsMin = getConfiguration() -> getWtpsMin();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> getWtpsMin(), initialWtpsMin + 50, F("WTPS min should be increased on 50 points"));
}

bool testWtpsMinValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialWtpsMin = getConfiguration() -> getWtpsMin();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  escButtonClick();
  return assertEquals(getConfiguration() -> getWtpsMin(), initialWtpsMin, F("WTPS min should stay as is"));
}

bool testWtpsMaxValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialWtpsMax = getConfiguration() -> getWtpsMax();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> getWtpsMax(), initialWtpsMax - 50, F("WTPS max should be decreased on 50 points"));
}

bool testWtpsMaxValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialWtpsMax = getConfiguration() -> getWtpsMax();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  escButtonClick();
  return assertEquals(getConfiguration() -> getWtpsMax(), initialWtpsMax, F("WTPS max should stay as is"));
}

bool testPpsYellowValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialPpsYellow = getConfiguration() -> getPpsYellow();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> getPpsYellow(), initialPpsYellow + 50, F("PPS yellow should be increased on 50 points"));
}

bool testPpsYellowValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialPpsYellow = getConfiguration() -> getPpsYellow();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  escButtonClick();
  return assertEquals(getConfiguration() -> getPpsYellow(), initialPpsYellow, F("PPS yellow should stay as is"));
}

bool testPpsRedValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialPpsRed = getConfiguration() -> getPpsRed();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> getPpsRed(), initialPpsRed - 50, F("PPS red should be decreased on 50 points"));
}

bool testPpsRedValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialPpsRed = getConfiguration() -> getPpsRed();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  escButtonClick();
  return assertEquals(getConfiguration() -> getPpsRed(), initialPpsRed, F("PPS red should stay as is"));
}

bool testDisplayContrastValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialDisplayContrast = getConfiguration() -> getDisplayContrast();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> getDisplayContrast(), initialDisplayContrast + 5, F("Display contrast should be increased on 5 points"));
}

bool testDisplayContrastValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialDisplayContrast = getConfiguration() -> getDisplayContrast();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  escButtonClick();
  return assertEquals(getConfiguration() -> getDisplayContrast(), initialDisplayContrast, F("Display contrast should stay as is"));
}

bool testLogEnabledValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  bool initialLogEnabled = getConfiguration() -> isLogEnabled();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> isLogEnabled(), !initialLogEnabled, F("Log enabling should be changed"));
}

bool testLogEnabledValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  bool initialLogEnabled = getConfiguration() -> isLogEnabled();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  escButtonClick();
  return assertEquals(getConfiguration() -> isLogEnabled(), initialLogEnabled, F("Log enabling should stay as is"));
}

bool testAutoZeroCalibrationValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  getConfiguration() -> setConfigurationValue(WtpsOffset, 5);
  getConfiguration() -> setConfigurationValue(PpsOffset, 5);
  bool result = assertEquals(getConfiguration() -> getWtpsOffset(), 5, F("Initial Wtps offset should be correct"));
  result = result && assertEquals(getConfiguration() -> getPpsOffset(), 5, F("Initial Pps offset should be correct"));
  okButtonClick();
  downButtonClick();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  int actualWtpsOffset = getConfiguration() -> getWtpsOffset();
  int actualPpsOffset = getConfiguration() -> getPpsOffset();
  result = result && assertTrue(actualWtpsOffset > 90 && actualWtpsOffset < 110, F("Calculated Wtps offset should be around 100"));
  result = result && assertTrue(actualPpsOffset > 90 && actualPpsOffset < 110, F("Calculated Pps offset should be around 100"));
  return result;
}

bool testAutoZeroCalibrationValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  getConfiguration() -> setConfigurationValue(WtpsOffset, 5);
  getConfiguration() -> setConfigurationValue(PpsOffset, 5);
  bool result = assertEquals(getConfiguration() -> getWtpsOffset(), 5, F("Initial Wtps offset should be correct"));
  result = result && assertEquals(getConfiguration() -> getPpsOffset(), 5, F("Initial Pps offset should be correct"));
  okButtonClick();
  okButtonClick();
  okButtonClick();
  escButtonClick();
  int actualWtpsOffset = getConfiguration() -> getWtpsOffset();
  int actualPpsOffset = getConfiguration() -> getPpsOffset();
  result = result && assertEquals(actualWtpsOffset, 5, F("Wtps offset should stay as is"));
  result = result && assertEquals(actualPpsOffset, 5, F("Pps offset should stay as is"));
  return result;
}

bool testWtpsZeroManualCalibrationValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialWtpsZero = getConfiguration() -> getWtpsOffset();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> getWtpsOffset(), initialWtpsZero + 5, F("Wtps zero should be increased on 5 points"));
}

bool testWtpsZeroManualCalibrationValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialWtpsZero = getConfiguration() -> getWtpsOffset();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  escButtonClick();
  return assertEquals(getConfiguration() -> getWtpsOffset(), initialWtpsZero, F("Wtps zero should stay as is"));
}

bool testPpsZeroManualCalibrationValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialPpsZero = getConfiguration() -> getPpsOffset();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> getPpsOffset(), initialPpsZero + 5, F("Pps zero should be increased on 5 points"));
}

bool testPpsZeroManualCalibrationValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialPpsZero = getConfiguration() -> getPpsOffset();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  escButtonClick();
  return assertEquals(getConfiguration() -> getPpsOffset(), initialPpsZero, F("Pps zero should stay as is"));
}


bool testNonZeroAutoCalibrationPredefinedPressureValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialConfigurationPressure = getConfiguration() -> getConfigurationPressure();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> getConfigurationPressure(), initialConfigurationPressure + 50, F("Predefined pressure should be increased on 50 points"));
}

bool testNonZeroAutoCalibrationPredefinedPressureValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialConfigurationPressure = getConfiguration() -> getConfigurationPressure();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  escButtonClick();
  return assertEquals(getConfiguration() -> getConfigurationPressure(), initialConfigurationPressure, F("Predefined pressure should stay as is"));
}

bool testManualCalibrationValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  getConfiguration() -> setConfigurationValue(WtpsOffset, 5);
  getConfiguration() -> setConfigurationValue(PpsOffset, 5);
  getConfiguration() -> setConfigurationValue(MaxWtpsPressure, 5000);
  getConfiguration() -> setConfigurationValue(MaxPpsPressure, 5000);
  getConfiguration() -> setConfigurationValue(ConfigurationPressure, 2500);
  bool result = assertEquals(getConfiguration() -> getWtpsOffset(), 5, F("Initial Wtps offset should be correct"));
  result = result && assertEquals(getConfiguration() -> getPpsOffset(), 5, F("Initial Pps offset should be correct"));
  result = result && assertEquals(getConfiguration() -> getMaxWtpsPressure(), 5000, F("Initial Max Pps Pressure should be correct"));
  result = result && assertEquals(getConfiguration() -> getMaxPpsPressure(), 5000, F("Initial Max Pps Pressure should be correct"));
  okButtonClick();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  int actualMaxWtpsPressure = getConfiguration() -> getMaxWtpsPressure();
  int actualMaxPpsPressure = getConfiguration() -> getMaxPpsPressure();
  result = result && assertTrue(actualMaxWtpsPressure > 26750 && actualMaxWtpsPressure < 26850, F("Calculated Max Wtps Pressure should be around 26 790"));
  result = result && assertTrue(actualMaxPpsPressure > 26750 && actualMaxPpsPressure < 26850, F("Calculated Max Pps Pressure should be around 26 790"));
  return result;
}

bool testManualCalibrationValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  getConfiguration() -> setConfigurationValue(WtpsOffset, 5);
  getConfiguration() -> setConfigurationValue(PpsOffset, 5);
  getConfiguration() -> setConfigurationValue(MaxWtpsPressure, 5000);
  getConfiguration() -> setConfigurationValue(MaxPpsPressure, 5000);
  bool result = assertEquals(getConfiguration() -> getWtpsOffset(), 5, F("Initial Wtps offset should be correct"));
  result = result && assertEquals(getConfiguration() -> getPpsOffset(), 5, F("Initial Pps offset should be correct"));
  result = result && assertEquals(getConfiguration() -> getMaxWtpsPressure(), 5000, F("Initial Max Pps Pressure should be correct"));
  result = result && assertEquals(getConfiguration() -> getMaxPpsPressure(), 5000, F("Initial Max Pps Pressure should be correct"));
  int initialMaxWtpsPressure = getConfiguration() -> getMaxWtpsPressure();
  int initialMaxPpsPressure = getConfiguration() -> getMaxPpsPressure();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  escButtonClick();
  result = result && assertEquals(getConfiguration() -> getMaxWtpsPressure(), initialMaxWtpsPressure, F("Max Wtps Pressure should stay as is"));
  result = result && assertEquals(getConfiguration() -> getMaxPpsPressure(), initialMaxPpsPressure, F("Max Pps Pressure should stay as is"));
  return result;
}

bool testWtpsNonZeroManualCalibrationValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialMaxWtpsPressure = getConfiguration() -> getMaxWtpsPressure();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> getMaxWtpsPressure(), initialMaxWtpsPressure + 5, F("MaxWtpsPressure should be increased on 5 points"));
}

bool testWtpsNonZeroManualCalibrationValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialMaxWtpsPressure = getConfiguration() -> getMaxWtpsPressure();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  escButtonClick();
  return assertEquals(getConfiguration() -> getMaxWtpsPressure(), initialMaxWtpsPressure, F("MaxWtpsPressure should stay as is"));
}



bool testPpsNonZeroManualCalibrationValueSaving() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialMaxPpsPressure = getConfiguration() -> getMaxPpsPressure();
  okButtonClick();
  okButtonClick();
  downButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> getMaxPpsPressure(), initialMaxPpsPressure + 5, F("MaxPpsPressure should be increased on 5 points"));
}

bool testPpsNonZeroManualCalibrationValueRollback() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  int initialMaxPpsPressure = getConfiguration() -> getMaxPpsPressure();
  okButtonClick();
  okButtonClick();
  okButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  upButtonClick();
  escButtonClick();
  return assertEquals(getConfiguration() -> getMaxPpsPressure(), initialMaxPpsPressure, F("MaxPpsPressure should stay as is"));
}

bool testRollForNumericValueUp() {
  refreshDisplayController();
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  int initialWtpsMin = getConfiguration() -> getWtpsMin();
  const int fullCycle = 1000;
  const int delta = 7;
  for(int i = 0; i < (fullCycle - delta); i++) {
    upButtonClick();
    Serial.print('.');
    if ((i + 1) % 100 == 0) {
      Serial.println();
    }
  }
  Serial.println();
  okButtonClick();
  return assertEquals(getConfiguration() -> getWtpsMin(), initialWtpsMin - delta*10, F("After rolling up of numeric value finally correct value should be saved"));
}

bool testRollForNumericValueDown() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  okButtonClick();
  int initialWtpsMin = getConfiguration() -> getWtpsMin();
  const int fullCycle = 1000;
  const int delta = 7;
  for(int i = 0; i < (fullCycle - delta); i++) {
    downButtonClick();
    Serial.print('.');
    if ((i + 1) % 100 == 0) {
      Serial.println();
    }
  }
  Serial.println();
  okButtonClick();
  return assertEquals(getConfiguration() -> getWtpsMin(), initialWtpsMin + delta*10, F("After rolling down of numeric value finally correct value should be saved"));
}

bool testRollForBooleanValueUp() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  downButtonClick();
  downButtonClick();
  downButtonClick();
  okButtonClick();
  bool initialLogEnabling = getConfiguration() -> isLogEnabled();
  upButtonClick();
  upButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> isLogEnabled(), initialLogEnabling, F("After rolling up of boolean value finally correct value should be saved"));
}

bool testRollForBooleanValueDown() {
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  okButtonClick();
  bool initialLogEnabling = getConfiguration() -> isLogEnabled();
  downButtonClick();
  downButtonClick();
  okButtonClick();
  return assertEquals(getConfiguration() -> isLogEnabled(), initialLogEnabling, F("After rolling down of boolean value finally correct value should be saved"));
}

bool testDisplayContrastApplying() {
  refreshDisplayController();
  getStatistic() -> resetErrorCode();
  getDisplayController() -> setDisplayRegularMenu(true);
  okButtonClick();
  downButtonClick();
  downButtonClick();
  okButtonClick();
  for (int i = 0; i < 50; i++) {
    upButtonClick();
  }
  okButtonClick();
  return assertManualAccept(F("Is display contrast was changed?"));
}

bool testScreenHighlightOff() {
  getDisplayController() -> turnOffHighlight();
  return assertManualAccept(F("Is display higlight off?"));
}

bool testScreenHighlightOn() {
  getDisplayController() -> turnOnHighlight();
  return assertManualAccept(F("Is display higlight on?"));
}

bool testButtonClickWillTurnOnHighlight() {
  getDisplayController() -> turnOffHighlight();
  bool result = assertManualAccept(F("Is display higlight OFF?"));
  okButtonClick();
  result = result && assertManualAccept(F("okButtonClick: Is display higlight ON?"));
  
  getDisplayController() -> turnOffHighlight();
  result = result && assertManualAccept(F("Is display higlight OFF?"));
  upButtonClick();
  result = result && assertManualAccept(F("upButtonClick: Is display higlight ON?"));

  getDisplayController() -> turnOffHighlight();
  result = result && assertManualAccept(F("Is display higlight OFF?"));
  downButtonClick();
  result = result && assertManualAccept(F("downButtonClick: Is display higlight ON?"));

  getDisplayController() -> turnOffHighlight();
  result = result && assertManualAccept(F("Is display higlight OFF?"));
  escButtonClick();
  result = result && assertManualAccept(F("escButtonClick: Is display higlight ON?"));

  getDisplayController() -> turnOffHighlight();
  result = result && assertManualAccept(F("Is display higlight OFF?"));
  resetFiltersButtonClick();
  result = result && assertManualAccept(F("resetButtonClick: Is display higlight ON?"));
  
  return result;
}

bool testAutoOffScreenHiglight() {
  getDisplayController() -> turnOffHighlight();
  bool result = assertManualAccept(F("Is display higlight OFF?"));
  okButtonClick();
  unsigned long startEventTime = millis();
  do {
    getDisplayController() -> check(2600, 2610);
  } while ((millis() - startEventTime) < (CLOSE_MENU_DELAY + 100));
  return result && assertManualAccept(F("Was display higlight ON and then off?"));
}

bool testResetFiltersButton() {
  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
  getStatistic() -> setPpsCycleMax(5000);
  resetFiltersButtonClick();
  bool result = assertEquals(getStatistic() -> getErrorCode(), NoError, F("Wrong error code after reset"));
  result = result && assertEquals(getStatistic() -> getPpsCycleMax(), 0, F("Wrong PPS cycle max after reset"));
  return result;
}

bool testManuallyResetFiltersButton() {
  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
  getStatistic() -> setPpsCycleMax(5000);
  getDisplayController() -> setDisplayRegularMenu(true);
  getDisplayController() -> check(2600, 2610);
  bool result = assertManualAccept(F("Is RED diode lighted?"));
  result = result && assertManualAccept(F("Is displayed PPS cycle max 5000?"));
  resetFiltersButtonClick();
  getDisplayController() -> setDisplayRegularMenu(true);
  getDisplayController() -> check(2600, 2610);
  result = result && assertManualAccept(F("Is GREEN diode lighted?"));
  result = result && assertManualAccept(F("Is displayed PPS cycle max 0?"));
  return result;
}

bool isPumpOn() {
  delay(RELE_DELAY);
  return digitalRead(PUMP_TEST_PIN) == LOW;
}

bool isPumpOff() {
  delay(RELE_DELAY);
  return digitalRead(PUMP_TEST_PIN) == HIGH;
}

bool testAfterPowerOnInitiallyPumpIsOff() {
  PumpController* _pumpController = new PumpController(deltaLog);
  for (int i = 0; i < 10; i++) {
    _pumpController -> check(getConfiguration() -> getWtpsMin() + 1, getConfiguration() -> getWtpsMin() + 1);
  }
  delete _pumpController;
  return isPumpOff();
}

bool testPumpWillOnIfPressureBecomeLowerThenConfigured() {
  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  bool result = assertEquals(getStatistic() -> getErrorCode(), NoError, F("Initially NoError code should be set"));
  result = result && assertTrue(isPumpOff(), F("Initially pump should be turned off"));
  _pumpController -> check(getConfiguration() -> getWtpsMin() + 1, getConfiguration() -> getWtpsMin() + 1);
  result = result && assertTrue(isPumpOff(), F("Pump should be turned off if pressure is bigger then configured"));
  _pumpController -> check(getConfiguration() -> getWtpsMin() - 1, getConfiguration() -> getWtpsMin() + 1);
  result = result && assertEquals(getStatistic() -> getErrorCode(), NoError, F("Finally NoError code should be set"));
  result = result && assertTrue(isPumpOn(), F("Pump should be turned on if pressure lower of configured"));
  delete _pumpController;
  return result;
}

bool testPumpWillBeOnIfWtpsBetweenMinAndMax() {
  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  _pumpController -> check(getConfiguration() -> getWtpsMin() - 1, getConfiguration() -> getWtpsMin() + 1);
  bool result = assertTrue(isPumpOn(), F("Initially pump should be turned on"));
  _pumpController -> check(getConfiguration() -> getWtpsMin() + 1, getConfiguration() -> getWtpsMin() + 1);
  result = result && assertTrue(isPumpOn(), F("Pump should be still on if wtps pressure between min and max"));
  delete _pumpController;
  return result;
}

bool testPumpWillOffIfPressureBecomeBiggerThenConfigured() {
  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  _pumpController -> check(getConfiguration() -> getWtpsMin() - 1, getConfiguration() -> getWtpsMax() + 1);
  bool result = assertEquals(getStatistic() -> getErrorCode(), NoError, F("Initially NoError code should be set"));
  result = result && assertTrue(isPumpOn(), F("Initially pump should be turned on"));
  _pumpController -> check(getConfiguration() -> getWtpsMax() - 1, getConfiguration() -> getWtpsMax() + 1);
  result = result && assertTrue(isPumpOn(), F("Pump should be turned on if pressure is lower of configured max"));
  _pumpController -> check(getConfiguration() -> getWtpsMax() + 1, getConfiguration() -> getWtpsMax() + 1);
  result = result && assertEquals(getStatistic() -> getErrorCode(), NoError, F("Finally NoError code should be set"));
  result = result && assertTrue(isPumpOff(), F("Pump should be turned off if pressure bigger of configured"));
  delete _pumpController;
  return result;
}

bool testPumpWillBeStillOnForPpsYellow() {
  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  _pumpController -> check(getConfiguration() -> getWtpsMin() - 1, getConfiguration() -> getWtpsMax() + 1);
  bool result = assertEquals(getStatistic() -> getErrorCode(), NoError, F("Initially NoError code should be set"));
  result = result && assertTrue(isPumpOn(), F("Initially pump should be turned on"));
  _pumpController -> check(getConfiguration() -> getWtpsMax() - 1, getConfiguration() -> getPpsYellow() - 1);
  result = result && assertEquals(getStatistic() -> getErrorCode(), NoError, F("NoError code should be set if PPS pressure lower of configured yellow level"));
  result = result && assertTrue(isPumpOn(), F("Pump should be turned on if PPS pressure is lower of configured yellow level"));
  _pumpController -> check(getConfiguration() -> getWtpsMax() - 1, getConfiguration() -> getPpsYellow() + 1);
  result = result && assertEquals(getStatistic() -> getErrorCode(), PpsWarning, F("PpsWarning code should be set if PPS pressure bigger of configured yellow level"));
  result = result && assertTrue(isPumpOn(), F("Pump should be turned on if PPS pressure is biger of configured yellow level, but lower red level"));
  delete _pumpController;
  return result;
}

bool testPumpWillBeBlockedIfRedLevelCrossed() {
  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  _pumpController -> check(getConfiguration() -> getWtpsMin() - 1, getConfiguration() -> getWtpsMax() + 1);
  bool result = assertEquals(getStatistic() -> getErrorCode(), NoError, F("Initially NoError code should be set"));
  result = result && assertTrue(isPumpOn(), F("Initially pump should be turned on"));
  _pumpController -> check(getConfiguration() -> getWtpsMax() - 1, getConfiguration() -> getPpsRed() - 1);
  result = result && assertEquals(getStatistic() -> getErrorCode(), PpsWarning, F("PpsWarning code should be set if PPS pressure lower of configured red level"));
  result = result && assertTrue(isPumpOn(), F("Pump should be turned on if PPS pressure is lower of configured red level"));
  _pumpController -> check(getConfiguration() -> getWtpsMax() - 1, getConfiguration() -> getPpsRed() + 1);
  result = result && assertEquals(getStatistic() -> getErrorCode(), ErrorTooBigPpsValue, F("ErrorTooBigPpsValue code should be set if PPS pressure bigger of configured red level"));
  result = result && assertTrue(isPumpOff(), F("Pump should be turned off if PPS pressure is biger of configured red level"));
  delete _pumpController;
  return result;
}

bool testPumpWillBeBlockedWithAnyError() {
  PumpController* _pumpController = new PumpController(deltaLog);
  // NoError
  getStatistic() -> resetErrorCode();
  bool result = assertTrue(isPumpOff(), F("NoError: Initially pump should be turned off"));
  _pumpController -> check(getConfiguration() -> getWtpsMin() - 1, getConfiguration() -> getWtpsMax() + 1);
  result = result && assertTrue(isPumpOn(), F("NoError should allow to turn on pump"));

  // PpsWarning
  getStatistic() -> resetErrorCode();
  _pumpController -> check(getConfiguration() -> getWtpsMax() + 1, getConfiguration() -> getWtpsMax() + 1);
  result = result && assertTrue(isPumpOff(), F("PpsWarning: Initially pump should be turned off"));
  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(PpsWarning);
  _pumpController -> check(getConfiguration() -> getWtpsMin() - 1, getConfiguration() -> getWtpsMax() + 1);
  result = result && assertTrue(isPumpOn(), F("PpsWarning should allow to turn on pump"));

  // ErrorTooBigPpsValue
  getStatistic() -> resetErrorCode();
  _pumpController -> check(getConfiguration() -> getWtpsMax() + 1, getConfiguration() -> getWtpsMax() + 1);
  result = result && assertTrue(isPumpOff(), F("ErrorTooBigPpsValue: Initially pump should be turned off"));
  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
  _pumpController -> check(getConfiguration() -> getWtpsMin() - 1, getConfiguration() -> getWtpsMax() + 1);
  result = result && assertTrue(isPumpOff(), F("ErrorTooBigPpsValue should not allow to turn on pump"));

  // SensorsRestrictionViolation
  getStatistic() -> resetErrorCode();
  _pumpController -> check(getConfiguration() -> getWtpsMax() + 1, getConfiguration() -> getWtpsMax() + 1);
  result = result && assertTrue(isPumpOff(), F("SensorsRestrictionViolation: Initially pump should be turned off"));
  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(SensorsRestrictionViolation);
  _pumpController -> check(getConfiguration() -> getWtpsMin() - 1, getConfiguration() -> getWtpsMax() + 1);
  result = result && assertTrue(isPumpOff(), F("SensorsRestrictionViolation should not allow to turn on pump"));

  delete _pumpController;
  return result;
}

bool testWtpsBiggerPpsWillTurnOffPumpAfterConfiguredDuration() {
  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  int ppsValue = getConfiguration() -> getWtpsMax() + 2;
  int wtpsValue = getConfiguration() -> getWtpsMin() - 1;
  _pumpController -> check(wtpsValue, ppsValue);
  bool result = assertTrue(isPumpOn(), F("Initially pump should be turned on"));
  ppsValue = wtpsValue - SENSOR_ALLOWABLE_ERROR - 1;
  unsigned long startTime = millis();
  while ((startTime + SENSOR_ALLOWABLE_ERROR_DURATION + 100) > millis()) {
    _pumpController -> check(wtpsValue, ppsValue);
  }
  result = result && assertTrue(isPumpOff(), F("If PPS sensor value lover WTPS sensoe value pump should be turned off"));
  delete _pumpController;
  return result;
}

bool testWtpsBiggerPpsWillNotTurnOffPumpBeforeConfiguredDuration() {
  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  int ppsValue = getConfiguration() -> getWtpsMax() + 2;
  int wtpsValue = getConfiguration() -> getWtpsMin() - 1;
  _pumpController -> check(wtpsValue, ppsValue);
  bool result = assertTrue(isPumpOn(), F("Initially pump should be turned on"));
  ppsValue = wtpsValue - SENSOR_ALLOWABLE_ERROR - 1;
  unsigned long startTime = millis();
  while ((startTime + SENSOR_ALLOWABLE_ERROR_DURATION - 100) > millis()) {
    _pumpController -> check(wtpsValue, ppsValue);
    result = result && assertTrue(isPumpOn(), F("If sensor has wrong values less then duration, pump should be turned on"));
  }
  result = result && assertTrue(isPumpOn(), F("If sensor has wrong values less then duration, pump should be turned on"));
  delete _pumpController;
  return result;
}

bool testWtpsBiggerPpsWillSetSensorsRestrictionViolationError() {
  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  int ppsValue = getConfiguration() -> getWtpsMax() + 2;
  int wtpsValue = getConfiguration() -> getWtpsMin() - 1;
  _pumpController -> check(wtpsValue, ppsValue);
  bool result = assertTrue(isPumpOn(), F("Initially pump should be turned on"));
  ppsValue = wtpsValue - SENSOR_ALLOWABLE_ERROR - 1;
  unsigned long startTime = millis();
  while ((startTime + SENSOR_ALLOWABLE_ERROR_DURATION + 100) > millis()) {
    _pumpController -> check(wtpsValue, ppsValue);
  }
  result = result && assertEquals(getStatistic() -> getErrorCode(), SensorsRestrictionViolation, F("If PPS sensor value is lower WTPS sensor value SensorsRestrictionViolation error should be set"));
  delete _pumpController;
  return result;
}

bool testSensorDiffWillBeSavedIfItBiggerThenPrevious() {
  DeltaLogIterator* iterator = deltaLog -> getIterator();
  DeltaEntry lastDeltaEntry = {0, 0};
  int originalCount = 0;
  while (iterator -> hasNext()) {
    originalCount++;
    lastDeltaEntry = iterator -> getNext();
  }
  int newDeltaPressure = lastDeltaEntry.deltaPressure + 1;

  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  int wtpsValue = getConfiguration() -> getWtpsMin() - 1;
  int ppsValue = wtpsValue + newDeltaPressure;
  _pumpController -> check(wtpsValue, ppsValue);
  
  iterator = deltaLog -> getIterator();
  int finalCount = 0;
  while (iterator -> hasNext()) {
    finalCount++;
    lastDeltaEntry = iterator -> getNext();
  }
  bool result = assertEquals(finalCount, originalCount + 1, F("New one delta log record should be added"));
  result = result && assertEquals(lastDeltaEntry.deltaPressure, newDeltaPressure, F("Latest delta pressure should be previous one increased by 1"));
  return result;
}

bool testSensorDiffWillNotBeSavedIfItEqualsOrLessPrevious() {
  DeltaLogIterator* iterator = deltaLog -> getIterator();
  DeltaEntry lastDeltaEntry = {0, 0};
  int originalCount = 0;
  while (iterator -> hasNext()) {
    originalCount++;
    lastDeltaEntry = iterator -> getNext();
  }
  int newDeltaPressure = lastDeltaEntry.deltaPressure;
  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  int wtpsValue = getConfiguration() -> getWtpsMin() - 1;
  int ppsValue = wtpsValue + newDeltaPressure;
  _pumpController -> check(wtpsValue, ppsValue);
  iterator = deltaLog -> getIterator();
  int finalCount = 0;
  while (iterator -> hasNext()) {
    finalCount++;
    lastDeltaEntry = iterator -> getNext();
  }
  bool result = assertEquals(finalCount, originalCount, F("No new delta log record should be added if delta is the same"));

  newDeltaPressure = lastDeltaEntry.deltaPressure - 1;
  ppsValue = wtpsValue + newDeltaPressure;
  _pumpController -> check(wtpsValue, ppsValue);
  iterator = deltaLog -> getIterator();
  finalCount = 0;
  while (iterator -> hasNext()) {
    finalCount++;
    lastDeltaEntry = iterator -> getNext();
  }
  result = result && assertEquals(finalCount, originalCount, F("No new delta log record should be added if delta is lower"));

  return result;
}

bool testPpsCycleMaxWillBeUpdatedIfBigger() {
  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  getStatistic() -> setPpsCycleMax(getConfiguration() -> getWtpsMin());
  int wtpsValue = getConfiguration() -> getWtpsMin() - 1;
  int ppsValue = getConfiguration() -> getWtpsMin() + 1;
  _pumpController -> check(wtpsValue, ppsValue);
  return assertEquals(getStatistic() -> getPpsCycleMax(), ppsValue, F("PPS cycle max should be updated with bigger pps value"));
}

bool testPpsCycleMaxWillBeTheSameIfEqualsOrLess() {
  PumpController* _pumpController = new PumpController(deltaLog);
  getStatistic() -> resetErrorCode();
  int originalPpsCycleMax = getConfiguration() -> getWtpsMin();
  getStatistic() -> setPpsCycleMax(originalPpsCycleMax);
  int wtpsValue = getConfiguration() -> getWtpsMin() - 1;
  int ppsValue = originalPpsCycleMax - 1;
  _pumpController -> check(wtpsValue, ppsValue);
  bool result = assertEquals(getStatistic() -> getPpsCycleMax(), originalPpsCycleMax, F("Original PPS cycle max should stay if new one is lower"));
  ppsValue = originalPpsCycleMax;
  _pumpController -> check(wtpsValue, ppsValue);
  result = result && assertEquals(getStatistic() -> getPpsCycleMax(), originalPpsCycleMax, F("Original PPS cycle max should stay if new one is the same"));
  delete _pumpController;
  return result;
}

bool testAnalogPressureConverterWillReturnZeroForNegativeInput() {
  float floatPressure = convertAnalogPressureToFloat(-10);
  return assertEquals(floatPressure, 0.0, F("Converted pressure should be zero if input is negative"));
}

bool testAnalogPressureConverterWillReturnValidPressure() {
  float floatPressure = convertAnalogPressureToFloat(2100);
  return assertEquals(floatPressure, 2.1, F("Converted pressure should be zero if input is negative"));
}

bool testWtpsSensorValueNormalizerWillBeInitializedWithDefaultValues() {
  int initialSensorValue = 500;
  getConfiguration() -> setConfigurationValue(WtpsOffset, 0);
  getConfiguration() -> setConfigurationValue(MaxWtpsPressure, MAX_SENSOR_VALUE*2);
  SensorValueNormalizer* sensorValueNormalizer = new SensorValueNormalizer(13, 2, initialSensorValue, 0);
  bool result = assertEquals(sensorValueNormalizer -> normalizeWtpsSensorValue(initialSensorValue), initialSensorValue*2, F("Wrong WTPS initial value"));
  delete sensorValueNormalizer;
  return result;
}

bool testPpsSensorValueNormalizerWillBeInitializedWithDefaultValues() {
  int initialSensorValue = 500;
  getConfiguration() -> setConfigurationValue(PpsOffset, 0);
  getConfiguration() -> setConfigurationValue(MaxPpsPressure, MAX_SENSOR_VALUE*2);
  SensorValueNormalizer* sensorValueNormalizer = new SensorValueNormalizer(13, 2, 0, initialSensorValue);
  bool result = assertEquals(sensorValueNormalizer -> normalizePpsSensorValue(initialSensorValue), initialSensorValue*2, F("Wrong WTPS initial value"));
  delete sensorValueNormalizer;
  return result;
}

bool testWtpsSensorValueNormalizerWillProduceCorrectResults() {
  bool result = true;
  SensorValueNormalizer* sensorValueNormalizer = new SensorValueNormalizer(13, 2, 0, 0);
  int sensorValues[21] = {100, 200, 300, 400, 401, 402, 403, 404, 405, 406, 407, 408, 500, 401, 302, 203, 504, 505, 506, 507};
  int expectedPressure[21] = {0, 0, 22, 66, 132, 222, 310, 400, 490, 580, 670, 738, 784, 806, 806, 806, 808, 830, 852, 874};
  getConfiguration() -> setConfigurationValue(WtpsOffset, 0);
  getConfiguration() -> setConfigurationValue(MaxWtpsPressure, MAX_SENSOR_VALUE*2);
  for (int i = 0; i < 20; i++) {
    result = result && assertEquals(sensorValueNormalizer -> normalizeWtpsSensorValue(sensorValues[i]), expectedPressure[i], F("Wrong WTPS calculated value"));
  }
  delete sensorValueNormalizer;
  return result;
}

bool testPpsSensorValueNormalizerWillProduceCorrectResults() {
  bool result = true;
  SensorValueNormalizer* sensorValueNormalizer = new SensorValueNormalizer(13, 2, 0, 0);
  int sensorValues[20] = {100, 200, 300, 400, 401, 402, 403, 404, 405, 406, 407, 408, 500, 401, 302, 203, 504, 505, 506, 507};
  int expectedPressure[20] = {0, 0, 33, 99, 198, 333, 465, 600, 735, 870, 1005, 1107, 1176, 1209, 1209, 1209, 1212, 1245, 1278, 1311};
  getConfiguration() -> setConfigurationValue(PpsOffset, 0);
  getConfiguration() -> setConfigurationValue(MaxPpsPressure, MAX_SENSOR_VALUE*3);
  for (int i = 0; i < 20; i++) {
    result = result && assertEquals(sensorValueNormalizer -> normalizePpsSensorValue(sensorValues[i]), expectedPressure[i], F("Wrong PPS calculated value"));
  }
  delete sensorValueNormalizer;
  return result;
}

class MemoryTest {
  public:
    MemoryTest(int size) {
      b = new byte[size - 6];
    };
    ~MemoryTest() {
      delete[] b;
    }
  private:
    byte* b;
};

bool prepareTestDataForRadio(DeltaLog* deltaLog) {
  for(byte i = 0; i < 10; i++) {
    if (i != 0) {
      delay(1000);
    }
    deltaLog -> writeDeltaIfBigger(10*(i + 1));
  }
  DeltaLogIterator* iterator = deltaLog -> getIterator();
  byte k = 0;
  bool result = true;
  while (iterator -> hasNext()) {
    DeltaEntry deltaEntry = iterator -> getNext();
    result = result && assertEquals(deltaEntry.deltaPressure, 10*k, F("Delta pressure should be multiple of 10"));
    if (k > 1) {
      result = result && assertTrue((deltaEntry.deltaTime > 980) && (deltaEntry.deltaTime < 1020), F("Delta time should be around 1 second"));
    }
    Serial.print(deltaEntry.deltaTime); Serial.print(": ");Serial.println(deltaEntry.deltaPressure);
    k++;
  }
  result = result && assertEquals(k, 11, F("Count of Delta log entries should be 10"));
  return result;
}

bool testStatisticResetErrorWillBeAbleToSetNoErrorCodeRegardlessCurrentErrorCode() {
  bool result = true;
  
  getStatistic() -> resetErrorCode();
  result = result && assertEquals(getStatistic() -> getErrorCode(), NoError, F("Initial state should be NoError"));
  
  getStatistic() -> setErrorCode(SensorsRestrictionViolation);
  getStatistic() -> resetErrorCode();
  result = result && assertEquals(getStatistic() -> getErrorCode(), NoError, F("SensorsRestrictionViolation should be reset to NoError"));

  getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
  getStatistic() -> resetErrorCode();
  result = result && assertEquals(getStatistic() -> getErrorCode(), NoError, F("ErrorTooBigPpsValue should be reset to NoError"));

  getStatistic() -> setErrorCode(PpsWarning);
  getStatistic() -> resetErrorCode();
  result = result && assertEquals(getStatistic() -> getErrorCode(), NoError, F("PpsWarning should be reset to NoError"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> resetErrorCode();
  result = result && assertEquals(getStatistic() -> getErrorCode(), NoError, F("NoError should be reset to NoError"));
  
  return result;
}

bool testErrorsWithHighPriorityCanOverrideLowerPriorityErrors() {
  bool result = true;
  
  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(NoError);
  result = result && assertEquals(getStatistic() -> getErrorCode(), NoError, F("NoError should be changed to NoError"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(PpsWarning);
  result = result && assertEquals(getStatistic() -> getErrorCode(), PpsWarning, F("NoError should be changed to PpsWarning"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
  result = result && assertEquals(getStatistic() -> getErrorCode(), ErrorTooBigPpsValue, F("NoError should be changed to ErrorTooBigPpsValue"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(SensorsRestrictionViolation);
  result = result && assertEquals(getStatistic() -> getErrorCode(), SensorsRestrictionViolation, F("NoError should be changed to SensorsRestrictionViolation"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(PpsWarning);
  getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
  result = result && assertEquals(getStatistic() -> getErrorCode(), ErrorTooBigPpsValue, F("PpsWarning should be changed to ErrorTooBigPpsValue"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(PpsWarning);
  getStatistic() -> setErrorCode(SensorsRestrictionViolation);
  result = result && assertEquals(getStatistic() -> getErrorCode(), SensorsRestrictionViolation, F("PpsWarning should be changed to SensorsRestrictionViolation"));

  return result;
}

bool testErrorCodeCannotBeChangedToTheCodeWithTheSamePriorityOrDowngraded() {
  bool result = true;
  
  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(PpsWarning);
  getStatistic() -> setErrorCode(NoError);
  result = result && assertEquals(getStatistic() -> getErrorCode(), PpsWarning, F("ErrorCode should be PpsWarning"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
  getStatistic() -> setErrorCode(PpsWarning);
  result = result && assertEquals(getStatistic() -> getErrorCode(), ErrorTooBigPpsValue, F("ErrorCode should be ErrorTooBigPpsValue"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
  getStatistic() -> setErrorCode(NoError);
  result = result && assertEquals(getStatistic() -> getErrorCode(), ErrorTooBigPpsValue, F("ErrorCode should be ErrorTooBigPpsValue"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
  getStatistic() -> setErrorCode(SensorsRestrictionViolation);
  result = result && assertEquals(getStatistic() -> getErrorCode(), ErrorTooBigPpsValue, F("ErrorCode should be ErrorTooBigPpsValue"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(SensorsRestrictionViolation);
  getStatistic() -> setErrorCode(PpsWarning);
  result = result && assertEquals(getStatistic() -> getErrorCode(), SensorsRestrictionViolation, F("ErrorCode should be SensorsRestrictionViolation"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(SensorsRestrictionViolation);
  getStatistic() -> setErrorCode(NoError);
  result = result && assertEquals(getStatistic() -> getErrorCode(), SensorsRestrictionViolation, F("ErrorCode should be SensorsRestrictionViolation"));

  getStatistic() -> resetErrorCode();
  getStatistic() -> setErrorCode(SensorsRestrictionViolation);
  getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
  result = result && assertEquals(getStatistic() -> getErrorCode(), SensorsRestrictionViolation, F("ErrorCode should be SensorsRestrictionViolation"));

  return result;
}

bool testWatchdogProcessorWillIncreaseWatchdogCounterForWdtReset() {
  uint8_t mcusrValue = 0b00001000;
  WatchdogProcessor* watchdogProcessor = new WatchdogProcessor();
  unsigned long previousWatchdogCounter = getStatistic() -> getWatchdogCounter();
  watchdogProcessor -> checkReset(mcusrValue);
  return assertEquals(getStatistic() -> getWatchdogCounter(), previousWatchdogCounter + 1, F("Watchdog counter should be increased for reset because of watchdog timer"));
}

bool testWatchdogProcessorWillNotIncreaseWatchdogCounterForNonWdtReset() {
  WatchdogProcessor* watchdogProcessor = new WatchdogProcessor();
  unsigned long previousWatchdogCounter = getStatistic() -> getWatchdogCounter();
  // BrownoutReset
  watchdogProcessor -> checkReset(0b00000100);
  // ExternalReset
  watchdogProcessor -> checkReset(0b00000010);
  // PowerOnReset
  watchdogProcessor -> checkReset(0b00000001);
  return assertEquals(getStatistic() -> getWatchdogCounter(), previousWatchdogCounter, F("Watchdog counter should not be increased for reset because of non wtd timer"));
}

// This test will lead to device reset because of watchdog timeout. It should be executed at the very end. Otherwise following tests will not be executed.
bool testResetWillNotLeadToInfiniteLoop() {
  Serial.println(F("After 4 seconds device should be rebooted"));
  wdt_enable(WDTO_4S);
  int timer = 0;
  while (timer < 5) {
    if (!(millis()%1000)) {
      timer++;
      Serial.println(timer);
      delay(1);
    }
  }
  return false;
}
