#ifndef AUTest_h
#define AUTest_h

#include "Arduino.h"

void processTestResult(bool testResult, const __FlashStringHelper* testName);
void printResults();
bool assertEquals(int actualValue, int expectedValue, const __FlashStringHelper* errorMessage);
bool assertEquals(unsigned long actualValue, unsigned long expectedValue, const __FlashStringHelper* errorMessage);
bool assertEquals(float actualValue, float expectedValue, const __FlashStringHelper* errorMessage);
bool assertEquals(bool actualValue, bool expectedValue, const __FlashStringHelper* errorMessage);
bool assertTrue(bool condition, const __FlashStringHelper* errorMessage);
bool assertManualAccept(const __FlashStringHelper* promptMessage);

#endif
