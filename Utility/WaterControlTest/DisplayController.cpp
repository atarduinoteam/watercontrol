#include "DisplayController.h"
#include <PCD8544.h>
#include "AnalogButtons.h"
#include "ButtonsController.h"
#include "InitBoard.h"

// OK status
const char* STATUS_OK = "OK            ";
// Warning status - PPS value exceed yellow PPS value;
const char STATUS_WARNING[] = "PPS WARNING   ";
// PPS too big - Filters are clogged
const char STATUS_PPS_TOO_BIG[] = "CLOGGED       ";
// Corruped sensor(s) - PPS has value significantly less then WTPS value
const char STATUS_CORRUPTED_SENSORS[] = "BROKEN SENSOR ";

DisplayController::DisplayController(PCD8544* lcd) {
  _lcd = lcd;
  _lcd -> begin(84, 48);
  _lcd -> setContrast(getConfiguration() -> getDisplayContrast());
  _configurationMenu = new ConfigurationMenu(_lcd);
  turnOffHighlight();
}

DisplayController::~DisplayController() {
  delete _configurationMenu;
}

void DisplayController::check(int wtpsCurrentValue, int ppsCurrentValue) {
  if (_displayRegularMenu) {
    _displayRegularScreen(wtpsCurrentValue, ppsCurrentValue);
  } else {
    _configurationMenu -> redrawScreen();
    if ((millis() - getLastButtonClickTime()) > CLOSE_MENU_DELAY) {
      setDisplayRegularMenu(true);
      turnOffHighlight();
    }
  }
  
  if (isHighlightOn() && (millis() - getLastButtonClickTime()) > CLOSE_MENU_DELAY) {
    turnOffHighlight();
  }
}

void DisplayController::setDisplayRegularMenu(bool displayRegularMenu) {
  if (displayRegularMenu != _displayRegularMenu) {
    _displayRegularMenu = displayRegularMenu;
    _lcd -> clear();
    _lcd -> setContrast(getConfiguration() -> getDisplayContrast());
  }
}

bool DisplayController::isDisplayRegularMenu() {
  return _displayRegularMenu;
}

ConfigurationMenu* DisplayController::getConfigurationMenu() {
  return _configurationMenu;
}

void DisplayController::turnOnHighlight() {
  digitalWrite(SCREEN_HIGHLIGHT_PIN, LOW);
  _highlightOn = true;
}

void DisplayController::turnOffHighlight() {
  digitalWrite(SCREEN_HIGHLIGHT_PIN, HIGH);
  _highlightOn = false;
}

boolean DisplayController::isHighlightOn() {
  return _highlightOn;
}

void DisplayController::_displayRegularScreen(int wtpsCurrentValue, int ppsCurrentValue) {
  _lcd -> setCursor(0,0);
  _lcd -> print(convertAnalogPressureToFloat(getConfiguration() -> getWtpsMin()), 2);
  _lcd -> print(' ');
  _lcd -> print(convertAnalogPressureToFloat(wtpsCurrentValue), 2);
  _lcd -> print(' ');
  _lcd -> print(convertAnalogPressureToFloat(getConfiguration() -> getWtpsMax()), 2);
  _lcd -> setCursor(0,1);
  _lcd -> print(convertAnalogPressureToFloat(ppsCurrentValue), 2);
  _lcd -> print(' ');
  _lcd -> print(convertAnalogPressureToFloat(getStatistic() -> getPpsCycleMax()), 2);
  _lcd -> print(' ');
  _lcd -> print(convertAnalogPressureToFloat(getConfiguration() -> getPpsRed()), 2);
  _lcd -> setCursor(0, 2);
  _lcd -> print(getStatistic() -> getPumpCounter());
  _displayCurrentStatus();
  _displayProtectionInfo();
}

void DisplayController::_displayCurrentStatus() {
  _lcd -> setCursor(0, 3);
  switch(getStatistic() -> getErrorCode()) {
    case NoError: {
      _turnOnGreen();
      _lcd -> print(STATUS_OK);
      break;
    }
    case PpsWarning: {
      _turnOnYellow();
      _lcd -> print(STATUS_WARNING);
      break;
    }
    case ErrorTooBigPpsValue: {
      _turnOnRed();
      _lcd -> print(STATUS_PPS_TOO_BIG);
      break;
    }
    case SensorsRestrictionViolation: {
      _turnOnRed();
      _lcd -> print(STATUS_CORRUPTED_SENSORS);
      break;
    }
  }
}

void DisplayController::_displayProtectionInfo() {
  _lcd -> setCursor(0, 4);
  char buffer[15];
  sprintf(buffer, "%-7lu %6lu", getStatistic() -> getRebootCounter(), getStatistic() -> getWatchdogCounter());
  _lcd -> print(buffer);
}

void DisplayController::_turnOnGreen() {
  digitalWrite(DIODE_ADDR_LOW, LOW);
  digitalWrite(DIODE_ADDR_HIGH, LOW);
}

void DisplayController::_turnOnYellow() {
  digitalWrite(DIODE_ADDR_LOW, HIGH);
  digitalWrite(DIODE_ADDR_HIGH, LOW);
}

void DisplayController::_turnOnRed() {
  digitalWrite(DIODE_ADDR_LOW, LOW);
  digitalWrite(DIODE_ADDR_HIGH, HIGH);
}
