#include "ResetController.h"

ResetController::ResetController(void (*onWdtReset)(void), void (*onBrownoutReset)(void), void (*onExternalReset)(void), void (*onPowerOnReset)(void)) {
    _onWdtReset = onWdtReset;
    _onBrownoutReset = onBrownoutReset;
    _onExternalReset = onExternalReset;
    _onPowerOnReset = onPowerOnReset;
}

ResetController::~ResetController() {
    ;
}

void ResetController::checkReset(uint8_t mcusrValue) {
    if((mcusrValue & (1<<WDRF)) && _onWdtReset) {
        (*_onWdtReset)();
    }
    if((mcusrValue & (1<<BORF)) && _onBrownoutReset) {
        (*_onBrownoutReset)();
    }
    if((mcusrValue & (1<<EXTRF)) && _onExternalReset) {
        (*_onExternalReset)();
    }
    if((mcusrValue & (1<<PORF)) && _onPowerOnReset) {
        (*_onPowerOnReset)();
    }
}
