#include "Statistic.h"
#include <EEPROM.h>

Statistic::Statistic(int offset, bool initNewStruct) : BaseEepromStruct(offset){
  if (initNewStruct) {
    initStruct();
  } else {
    readStruct();
  }
  initStructureOffset();
}

Statistic::~Statistic() {
}

void Statistic::initStruct() {
  _statisticStructure.rebootCounter = 0;
  _statisticStructure.pumpCounter = 0;
  _statisticStructure.ppsCycleMax = 0;
  _statisticStructure.index = 0;
  _statisticStructure.errorCode = NoError;
  _statisticStructure.watchdogCounter = 0;
  EEPROM.put(getOffset(), _statisticStructure);
}

void Statistic::readStruct() {
  EEPROM.get(getOffset(), _statisticStructure);
}

void Statistic::initStructureOffset() {
  _structureOffset = new int[6];
  _structureOffset[RebootCounter] = 0;
  _structureOffset[PumpCounter] = _structureOffset[RebootCounter] + sizeof(unsigned long);
  _structureOffset[PpsCycleMax] = _structureOffset[PumpCounter] + sizeof(unsigned long);
  _structureOffset[Index] = _structureOffset[PpsCycleMax] + sizeof(int);
  _structureOffset[ErrorCode] = _structureOffset[Index] + sizeof(int);
  _structureOffset[WatchdogCounter] = _structureOffset[ErrorCode] + sizeof(EErrorCode);
}

int Statistic::getStructSize() {
  return sizeof(StatisticStructure);
}

unsigned long Statistic::getRebootCounter() {
  return _statisticStructure.rebootCounter;
}

int Statistic::getPpsCycleMax() {
  return _statisticStructure.ppsCycleMax;
}

EErrorCode Statistic::getErrorCode() {
  return _statisticStructure.errorCode;
}

int Statistic::getIndex() {
  return _statisticStructure.index;
}

unsigned long Statistic::getPumpCounter() {
  return _statisticStructure.pumpCounter;
}

unsigned long Statistic::getWatchdogCounter() {
  return _statisticStructure.watchdogCounter;
}


void Statistic::increaseRebootCounter() {
  _statisticStructure.rebootCounter++;
  EEPROM.put(getOffset() + _structureOffset[RebootCounter], _statisticStructure.rebootCounter);
}

void Statistic::setErrorCode(EErrorCode errorCode) {
  if (_statisticStructure.errorCode != errorCode && _isErrorApplicable(errorCode)) {
    _updateErrorCode(errorCode);
  }
}

void Statistic::resetErrorCode() {
  _updateErrorCode(NoError);
}

void Statistic::setPpsCycleMax(int ppsCycleMax) {
  _statisticStructure.ppsCycleMax = ppsCycleMax;
  EEPROM.put(getOffset() + _structureOffset[PpsCycleMax], ppsCycleMax);
}

void Statistic::setIndex(int index) {
  _statisticStructure.index = index;
  EEPROM.put(getOffset() + _structureOffset[Index], index);
}

void Statistic::increasePumpCounter() {
  // Save each 10th counter value
  _statisticStructure.pumpCounter++;
  if(_statisticStructure.pumpCounter % 10 == 0) {
    EEPROM.put(getOffset() + _structureOffset[PumpCounter], _statisticStructure.pumpCounter);
  }
}

void Statistic::increaseWatchdogCounter() {
  _statisticStructure.watchdogCounter++;
  EEPROM.put(getOffset() + _structureOffset[WatchdogCounter], _statisticStructure.watchdogCounter);
}

bool Statistic::_isErrorApplicable(EErrorCode errorCode) {
  switch (errorCode) {
    case NoError: {
      return false;
    }
    case PpsWarning: {
      return _statisticStructure.errorCode == NoError;
    }
    case ErrorTooBigPpsValue:
    case SensorsRestrictionViolation: {
      return (_statisticStructure.errorCode == NoError || _statisticStructure.errorCode == PpsWarning);
    }
  }
  return false;
}

void Statistic::_updateErrorCode(EErrorCode errorCode) {
  _statisticStructure.errorCode = errorCode;
  EEPROM.put(getOffset() + _structureOffset[ErrorCode], errorCode);
}
