#include "ConfigurationMenu.h"
#include "InitBoard.h"

// ====================== Utility methods ====================== //

int calculateMaxPressure(int physicalSensorValue, int offset, int expectedPressure) {
  return int((MAX_SENSOR_VALUE - offset)*1.0/(physicalSensorValue - offset)*expectedPressure);
}

// ====================== Configuration menu ====================== //

ConfigurationMenu::ConfigurationMenu(PCD8544* lcd) {
  _lcd = lcd;
  _root = new ContainerMenuItem((char*) "", 5, lcd, NULL);
    ContainerMenuItem* wtpsMenuItem = new ContainerMenuItem((char*) "WTPS", 2, lcd, _root);
      LeafMenuItem* minWtpsMenuItem = new NumericLeafMenuItem((char*) "WTPS MIN", wtpsMenuItem, lcd, WtpsMin, 0, 10000);
      LeafMenuItem* maxWtpsMenuItem = new NumericLeafMenuItem((char*) "WTPS MAX", wtpsMenuItem, lcd, WtpsMax, 0, 10000);
    ContainerMenuItem* ppsMenuItem = new ContainerMenuItem((char*) "PPS", 2, lcd, _root);
      LeafMenuItem* yellowPpsMenuItem = new NumericLeafMenuItem((char*) "PPS YELLOW", ppsMenuItem, lcd, PpsYellow, 0, 10000);
      LeafMenuItem* redPpsMenuItem = new NumericLeafMenuItem((char*) "PPS RED", ppsMenuItem, lcd, PpsRed, 0, 10000);
    LeafMenuItem* screenContrastMenuItem = new NumericLeafMenuItem((char*) "CONTRAST", _root, lcd, DisplayContrast, 0, 127, 1, false);
    BooleanLeafMenuItem* loggingMenuItem = new BooleanLeafMenuItem((char*) "LOGS", _root, lcd, LogEnabled);
    ContainerMenuItem* calibrationMenuItem = new ContainerMenuItem((char*) "CALIBRATION", 6, lcd, _root);
      LeafMenuItem* autoZeroLevelSensorLeafMenuItem = new AutoZeroLevelSensorLeafMenuItem((char*) "SET ZERO", calibrationMenuItem, lcd);
      LeafMenuItem* manualZeroLevelSensorLeafMenuWtps = new ManualZeroLevelSensorLeafMenuItem((char*) "WTPS 0 man" , calibrationMenuItem, lcd, 0, 500, WTPS_PIN);
      LeafMenuItem* manualZeroLevelSensorLeafMenuPps = new ManualZeroLevelSensorLeafMenuItem((char*) "PPS 0 man" , calibrationMenuItem, lcd, 0, 500, PPS_PIN);
      
      LeafMenuItem* autoNonZeroLevelSensorLeafMenuItem = new AutoNonZeroLevelSensorLeafMenuItem((char*) "ADJ NON-ZERO", calibrationMenuItem, lcd, 0, 10000, 10);
      LeafMenuItem* manualNonZeroLevelSensorLeafMenuWtps = new ManualNonZeroLevelSensorLeafMenuItem((char*) "WTPS X man", calibrationMenuItem, lcd, 0, 32760, WTPS_PIN);
      LeafMenuItem* manualNonZeroLevelSensorLeafMenuPps = new ManualNonZeroLevelSensorLeafMenuItem((char*) "PPS X man", calibrationMenuItem, lcd, 0, 32760, PPS_PIN);

  _root -> addMenuItem(wtpsMenuItem);
  _root -> addMenuItem(ppsMenuItem);
  _root -> addMenuItem(screenContrastMenuItem);
  _root -> addMenuItem(loggingMenuItem);
  _root -> addMenuItem(calibrationMenuItem);
  
  wtpsMenuItem -> addMenuItem(minWtpsMenuItem);
  wtpsMenuItem -> addMenuItem(maxWtpsMenuItem);
  
  ppsMenuItem -> addMenuItem(yellowPpsMenuItem);
  ppsMenuItem -> addMenuItem(redPpsMenuItem);

  calibrationMenuItem -> addMenuItem(autoZeroLevelSensorLeafMenuItem);
  calibrationMenuItem -> addMenuItem(manualZeroLevelSensorLeafMenuWtps);
  calibrationMenuItem -> addMenuItem(manualZeroLevelSensorLeafMenuPps);
  calibrationMenuItem -> addMenuItem(autoNonZeroLevelSensorLeafMenuItem);
  calibrationMenuItem -> addMenuItem(manualNonZeroLevelSensorLeafMenuWtps);
  calibrationMenuItem -> addMenuItem(manualNonZeroLevelSensorLeafMenuPps);
  
  _init();
}

ConfigurationMenu::~ConfigurationMenu() {
  delete _root;
}

void ConfigurationMenu::onOkAction() {
  _currentMenuItem = _currentMenuItem -> onOkAction();
  _currentMenuItem -> init();
  _currentMenuItem -> drawScreen();
}

void ConfigurationMenu::onUpAction() {
  _currentMenuItem -> onUpAction();
  _currentMenuItem -> drawScreen();
}

void ConfigurationMenu::onDownAction() {
  _currentMenuItem -> onDownAction();
  _currentMenuItem -> drawScreen();
}

bool ConfigurationMenu::onEscAction() {
  if (_currentMenuItem -> getParent()) {
    _currentMenuItem -> onEscAction();
    _currentMenuItem = _currentMenuItem -> getParent();
    _currentMenuItem -> drawScreen();
    return false;
  } else {
    return true;
  }
}

void ConfigurationMenu::_init() {
  _currentMenuItem = _root;
}

void ConfigurationMenu::turnOn() {
  _init();
  _currentMenuItem -> drawScreen();
}

void ConfigurationMenu::redrawScreen() {
  _currentMenuItem -> redrawScreen();
}

// ====================== Base menu item ====================== //

BaseMenuItem::BaseMenuItem(char __name[], BaseMenuItem* parent, PCD8544* lcd) {
  byte length = strlen(__name) + 1;
  _name = new char[length];
  for (byte i = 0; i < length; i++) {
    _name[i] = __name[i];
  };
  _parent = parent;
  _lcd = lcd;
}

void BaseMenuItem::onEscAction() {
}

BaseMenuItem::~BaseMenuItem() {
  delete[] _name;
}

BaseMenuItem* BaseMenuItem::getParent() {
  return _parent;
}

char* BaseMenuItem::getName() {
  return _name;
}

PCD8544* BaseMenuItem::getLcd() {
  return _lcd;
}

void BaseMenuItem::redrawScreen() {
}

// ====================== Container menu item ====================== //

ContainerMenuItem::ContainerMenuItem(char _name[], byte size, PCD8544* lcd, BaseMenuItem* parent) : BaseMenuItem(_name, parent, lcd) {
  _size = size;
  _containerTop = 0;
  _childItems = new BaseMenuItem*[size];
  _currentSelectedMenuItem = 0;
  init();
}

ContainerMenuItem::~ContainerMenuItem() {
  for (byte i = 0; i < _size; i++) {
    delete (_childItems[i]);
  }
  delete[] _childItems;
}

byte ContainerMenuItem::getSize() {
  return _size;
}

BaseMenuItem* ContainerMenuItem::onOkAction() {
  return _getCurrentMenuItem();
}

void ContainerMenuItem::onDownAction() {
  if (_currentSelectedMenuItem < (getSize() - 1)) {
    _currentSelectedMenuItem++;
  } else {
    _currentSelectedMenuItem = 0;
  }
}

void ContainerMenuItem::onUpAction() {
  if (_currentSelectedMenuItem > 0) {
    _currentSelectedMenuItem --;
  } else {
    _currentSelectedMenuItem = getSize() - 1;
  }
}

BaseMenuItem* ContainerMenuItem::_getCurrentMenuItem() {
  return _childItems[_currentSelectedMenuItem];
}

void ContainerMenuItem::drawScreen() {
  _updateFrame();
  getLcd() -> clear();
  for (byte i = _startPosition; i <= _endPosition; i++) {
    getLcd() -> setCursor(0, i - _startPosition);
    if (i == _currentSelectedMenuItem) {
      getLcd() -> setInverseOutput(true);
      getLcd() -> print(_childItems[i] -> getName());
      getLcd() -> setInverseOutput(false);
    } else {
      getLcd() -> print(_childItems[i] -> getName());
    }
  }
}

void ContainerMenuItem::init() {
  _startPosition = 0;
  _endPosition = min(_size - 1, SCREEN_LINES_COUNT - 1);
  _updateFrame();
}

void ContainerMenuItem::_updateFrame() {
  if (_currentSelectedMenuItem < _startPosition) {
    byte delta = _startPosition - _currentSelectedMenuItem;
    _startPosition -= delta;
    _endPosition -= delta;
  }
  if (_currentSelectedMenuItem > _endPosition) {
    byte delta = _currentSelectedMenuItem - _endPosition;
    _startPosition += delta;
    _endPosition += delta;
  }
}

void ContainerMenuItem::addMenuItem(BaseMenuItem* menuItem) {
  _childItems[_containerTop] = menuItem;
  _containerTop++;
}

// ====================== Leaf menu item ====================== //

LeafMenuItem::LeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd) : BaseMenuItem(_name, parent, lcd) {
}

LeafMenuItem::~LeafMenuItem() {
}

// ====================== Numeric leaf menu item ====================== //

NumericLeafMenuItem::NumericLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd, ConfigurationProperty configurationProperty, int minValue, int maxValue, int delta, bool convertPressure) : LeafMenuItem(_name, parent, lcd) {
  _minValue = minValue;
  _maxValue = maxValue;
  _delta = delta;
  _convertPressure = convertPressure;
  setConfigurationProperty(configurationProperty);
}

NumericLeafMenuItem::~NumericLeafMenuItem() {
}

BaseMenuItem* NumericLeafMenuItem::onOkAction() {
  getConfiguration() -> setConfigurationValue(_configurationProperty, _currentValue);
  return getParent();
}

void NumericLeafMenuItem::onUpAction() {
  if (_currentValue <= (_maxValue - _delta)) {
    _currentValue += _delta;
  } else {
    _currentValue = _currentValue + _delta + _minValue - _maxValue;
  }
  _applyNumericValue();
}

void NumericLeafMenuItem::onDownAction() {
  if (_currentValue >= (_minValue + _delta)) {
    _currentValue -= _delta;
  } else {
    _currentValue = _maxValue + _currentValue - _delta - _minValue;
  }
  _applyNumericValue();
}

void NumericLeafMenuItem::drawScreen() {
  getLcd() -> clear();
  getLcd() -> print(getName());
  getLcd() -> setCursor(0,1);
  if (_convertPressure) {
    getLcd() -> print(convertAnalogPressureToFloat(_currentValue));
  } else {
    getLcd() -> print(_currentValue);
  }
}

void NumericLeafMenuItem::init() {
  _currentValue = getConfiguration() -> getIntConfigurationValue(_configurationProperty);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
void NumericLeafMenuItem::onEscAction() {
  switch (_configurationProperty) {
    case DisplayContrast: {
      getLcd() -> setContrast(getConfiguration() -> getDisplayContrast());
      break;
    }
  }
}
#pragma GCC diagnostic pop

int NumericLeafMenuItem::getCurrentValue() {
  return _currentValue;
}

void NumericLeafMenuItem::setCurrentValue(int currentValue) {
  _currentValue = currentValue;
}

void NumericLeafMenuItem::setConfigurationProperty(ConfigurationProperty configurationProperty) {
  _configurationProperty = configurationProperty;
  init();
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
void NumericLeafMenuItem::_applyNumericValue() {
  switch (_configurationProperty) {
    case DisplayContrast: {
      getLcd() -> setContrast(_currentValue);
      break;
    }
  }
}
#pragma GCC diagnostic pop

// ====================== Boolean leaf menu item ====================== //

BooleanLeafMenuItem::BooleanLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd, ConfigurationProperty configurationProperty) : LeafMenuItem(_name, parent, lcd) {
  _configurationProperty = configurationProperty;
  init();
}

BaseMenuItem* BooleanLeafMenuItem::onOkAction() {
  getConfiguration() -> setConfigurationValue(_configurationProperty, _currentValue);
  return getParent();
}

void BooleanLeafMenuItem::onUpAction() {
  _currentValue = !_currentValue;
}

void BooleanLeafMenuItem::onDownAction() {
  _currentValue = !_currentValue;
}

void BooleanLeafMenuItem::drawScreen() {
  getLcd() -> clear();
  getLcd() -> print(getName());
  getLcd() -> setCursor(0, 1);
  if (_currentValue) {
    getLcd() -> print("ON");
  } else {
    getLcd() -> print("OFF");
  }
}

void BooleanLeafMenuItem::init() {
  _currentValue = getConfiguration() -> getBoolConfigurationValue(_configurationProperty);
}

// ====================== Base action leaf menu item ====================== //
BaseActionLeafMenuItem::BaseActionLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd) : LeafMenuItem(_name, parent, lcd) {
  _saved = 0;
  init();
}

void BaseActionLeafMenuItem::onUpAction() {
  // Do nothing;
}

void BaseActionLeafMenuItem::onDownAction() {
  // Do nothing;
}

BaseMenuItem* BaseActionLeafMenuItem::onOkAction() {
  _saved = 2;
  return this;
}

void BaseActionLeafMenuItem::init() {
  if (_saved > 0) {
    _saved--;
  }
}

// ====================== Zero level sensor leaf menu item ====================== //

AutoZeroLevelSensorLeafMenuItem::AutoZeroLevelSensorLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd) : BaseActionLeafMenuItem(_name, parent, lcd) {
  // Do nothing;
}

BaseMenuItem* AutoZeroLevelSensorLeafMenuItem::onOkAction() {
  getConfiguration() -> setConfigurationValue(WtpsOffset, getSensorValueNormalizer() -> getWtpsMedian(analogRead(WTPS_PIN)));
  getConfiguration() -> setConfigurationValue(PpsOffset, getSensorValueNormalizer() -> getPpsMedian(analogRead(PPS_PIN)));
  return BaseActionLeafMenuItem::onOkAction();
}

void AutoZeroLevelSensorLeafMenuItem::drawScreen() {
  int wtpsOffset = getConfiguration() -> getIntConfigurationValue(WtpsOffset);
  int ppsOffset = getConfiguration() -> getIntConfigurationValue(PpsOffset);
  getLcd() -> clear();
  getLcd() -> print(getName());
  getLcd() -> setCursor(0, 1);
  getLcd() -> print("WTPS ");
  getLcd() -> print(wtpsOffset);
  getLcd() -> setCursor(0, 2);
  getLcd() -> print("PPS  ");
  getLcd() -> print(ppsOffset);
  if (_saved) {
    getLcd() -> setCursor(0, 3);
    getLcd() -> print("SAVED");
  }
}

// ====================== Manual zero level sensor leaf menu item ====================== //

ManualZeroLevelSensorLeafMenuItem::ManualZeroLevelSensorLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd, int minValue, int maxValue, int sensorPin, int delta) : 
                                      NumericLeafMenuItem(_name, parent, lcd, WtpsOffset, minValue, maxValue, delta, false) {
  _sensorPin = sensorPin;
  if (_sensorPin == WTPS_PIN) {
    setConfigurationProperty(WtpsOffset);
  } else if (_sensorPin == PPS_PIN) {
    setConfigurationProperty(PpsOffset);
  }
}

void ManualZeroLevelSensorLeafMenuItem::drawScreen() {
  NumericLeafMenuItem::drawScreen();
  redrawScreen();
}

void ManualZeroLevelSensorLeafMenuItem::redrawScreen() {
  getLcd() -> setCursor(0, 2);
  char buffer[5];
  sprintf(buffer, "%-4d", analogRead(_sensorPin));
  getLcd() -> print(buffer);
}

// ====================== Auto non zero level sensor leaf menu item ====================== //

AutoNonZeroLevelSensorLeafMenuItem::AutoNonZeroLevelSensorLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd, int minValue, int maxValue, int delta) : 
                                      NumericLeafMenuItem(_name, parent, lcd, ConfigurationPressure, minValue, maxValue, delta, true) {
  _saved = 0;
}

BaseMenuItem* AutoNonZeroLevelSensorLeafMenuItem::onOkAction() {
  NumericLeafMenuItem::onOkAction();
  int wtpsMedian = getSensorValueNormalizer() -> getWtpsMedian(analogRead(WTPS_PIN));
  int ppsMedian = getSensorValueNormalizer() -> getPpsMedian(analogRead(PPS_PIN));
  getConfiguration() -> setConfigurationValue(MaxWtpsPressure, calculateMaxPressure(wtpsMedian, getConfiguration() -> getWtpsOffset(), getCurrentValue()));
  getConfiguration() -> setConfigurationValue(MaxPpsPressure, calculateMaxPressure(ppsMedian, getConfiguration() -> getPpsOffset(), getCurrentValue()));
  _saved = 2;
  return this;
}

void AutoNonZeroLevelSensorLeafMenuItem::init() {
  NumericLeafMenuItem::init();
  if (_saved > 0) {
    _saved--;
  }
}

void AutoNonZeroLevelSensorLeafMenuItem::drawScreen() {
  NumericLeafMenuItem::drawScreen();
  int maxWtpsPressure = getConfiguration() -> getIntConfigurationValue(MaxWtpsPressure);
  int maxPpsPressure = getConfiguration() -> getIntConfigurationValue(MaxPpsPressure);
  getLcd() -> setCursor(0, 2);
  getLcd() -> print("WTPS ");
  getLcd() -> print(maxWtpsPressure);
  getLcd() -> setCursor(0, 3);
  getLcd() -> print("PPS  ");
  getLcd() -> print(maxPpsPressure);
  if (_saved) {
    getLcd() -> setCursor(0, 4);
    getLcd() -> print("SAVED");
  }
}

// ====================== Manual non zero level sensor leaf menu item ====================== //

ManualNonZeroLevelSensorLeafMenuItem::ManualNonZeroLevelSensorLeafMenuItem(char _name[], BaseMenuItem* parent, PCD8544* lcd, int minValue, int maxValue, int sensorPin, int delta) : 
                                      NumericLeafMenuItem(_name, parent, lcd, MaxWtpsPressure, minValue, maxValue, delta, false) {
  _sensorPin = sensorPin;
  if (_sensorPin == WTPS_PIN) {
    setConfigurationProperty(MaxWtpsPressure);
  } else if (_sensorPin == PPS_PIN) {
    setConfigurationProperty(MaxPpsPressure);
  }
}

void ManualNonZeroLevelSensorLeafMenuItem::drawScreen() {
  NumericLeafMenuItem::drawScreen();
  getLcd() -> setCursor(0, 2);
  getLcd() -> print(F("CONF "));
  getLcd() -> print(convertAnalogPressureToFloat(getConfiguration() -> getConfigurationPressure()), 2);
  redrawScreen();
}

void ManualNonZeroLevelSensorLeafMenuItem::redrawScreen() {
  getLcd() -> setCursor(0, 3);
  if (_sensorPin == WTPS_PIN) {
    int wtpsPressure = getSensorValueNormalizer() -> normalizeWtpsSensorValue(analogRead(WTPS_PIN), getCurrentValue());
    getLcd() -> setInverseOutput(true);
    getLcd() -> print("WTPS ");
    getLcd() -> print(convertAnalogPressureToFloat(wtpsPressure), 2);
    getLcd() -> setInverseOutput(false);
  } else {
    int wtpsPressure = getSensorValueNormalizer() -> normalizeWtpsSensorValue(analogRead(WTPS_PIN));
    getLcd() -> print("WTPS ");
    getLcd() -> print(convertAnalogPressureToFloat(wtpsPressure), 2);
  }
  getLcd() -> setCursor(0, 4);
  if (_sensorPin == PPS_PIN) {
    int ppsPressure = getSensorValueNormalizer() -> normalizePpsSensorValue(analogRead(PPS_PIN), getCurrentValue());
    getLcd() -> setInverseOutput(true);
    getLcd() -> print("PPS  ");
    getLcd() -> print(convertAnalogPressureToFloat(ppsPressure), 2);
    getLcd() -> setInverseOutput(false);
  } else {
    int ppsPressure = getSensorValueNormalizer() -> normalizePpsSensorValue(analogRead(PPS_PIN));
    getLcd() -> print("PPS  ");
    getLcd() -> print(convertAnalogPressureToFloat(ppsPressure), 2);
  }
}
