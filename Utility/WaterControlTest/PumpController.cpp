#include "PumpController.h"
#include <Arduino.h>
#include "BoardConfiguration.h"
#include "InitBoard.h"

PumpController::PumpController(DeltaLog* deltaLog) {
  _deltaLog = deltaLog;
  _waterPumpIsOn = true;
  _turnOffWaterPump();
  _sensorValidator = new SensorValidator(SENSOR_ALLOWABLE_ERROR, SENSOR_ALLOWABLE_ERROR_DURATION);
}

PumpController::~PumpController() {
  delete _sensorValidator;
}

void PumpController::check(int wtpsValue, int ppsValue) {
  _processWtps(wtpsValue);
  _processPps(ppsValue);
  _processPpsBiggerWtpsRestriction(wtpsValue, ppsValue);
  _logDeltaPressure(ppsValue - wtpsValue);
  _updatePpsCycleMax(ppsValue);
}

bool PumpController::isPumpOn() {
  return _waterPumpIsOn;
}

void PumpController::_processWtps(int wtpsValue) {
  if (getStatistic() -> getErrorCode() == NoError || getStatistic() -> getErrorCode() == PpsWarning) {
    if (wtpsValue < getConfiguration() -> getWtpsMin()) {
      _turnOnWaterPump();
    } else if(wtpsValue > getConfiguration() -> getWtpsMax()) {
      _turnOffWaterPump();
    }
  }
}

void PumpController::_processPps(int ppsValue) {
  if (ppsValue > getConfiguration() -> getPpsRed()) {
      _turnOffWaterPump();
      getStatistic() -> setErrorCode(ErrorTooBigPpsValue);
  } else if (ppsValue > getConfiguration() -> getPpsYellow()) {
      getStatistic() -> setErrorCode(PpsWarning);
  }
}

void PumpController::_processPpsBiggerWtpsRestriction(int wtpsValue, int ppsValue) {
  if (!_sensorValidator -> isValid(wtpsValue, ppsValue)) {
    _turnOffWaterPump();
    getStatistic() -> setErrorCode(SensorsRestrictionViolation);
  }
}

void PumpController::_logDeltaPressure(int deltaPressure) {
  if (getConfiguration() -> isLogEnabled()) {
    _deltaLog -> writeDeltaIfBigger(deltaPressure);
  }
}

void PumpController::_updatePpsCycleMax(int ppsValue) {
  if (ppsValue > getStatistic() -> getPpsCycleMax()) {
    getStatistic() -> setPpsCycleMax(ppsValue);
  }
}

void PumpController::_turnOnWaterPump() {
  if (!_waterPumpIsOn) {
    digitalWrite(WATER_PUMP_PIN, HIGH);
    _waterPumpIsOn = true;
    getStatistic() -> increasePumpCounter();
  }
}

void PumpController::_turnOffWaterPump() {
  if (_waterPumpIsOn) {
    digitalWrite(WATER_PUMP_PIN, LOW);
    _waterPumpIsOn = false;
  }
}
