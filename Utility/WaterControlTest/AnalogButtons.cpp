#include "AnalogButtons.h"

Button::Button(uint16_t value, void (*clickFunction)(void), void (*holdFunction)(void), uint32_t holdDuration, uint16_t holdInterval) {
	this -> value = value;
	this -> duration = holdDuration;
	this -> interval = holdInterval;
	this -> clickFunction = clickFunction;
	this -> holdFunction = holdFunction;
}

void Button::pressed() {
  if (clickFunction) {
    (*clickFunction)();
  }
}

void Button::held() {
  if (holdFunction) {
    (*holdFunction)();
  } else {
    pressed();
  }
}

uint16_t Button::getValue() {
  return value;
}

boolean Button::isHeldDown() {
  return heldDown;
}

void Button::setHeldDown(boolean heldDown) {
  this -> heldDown = heldDown;
}

uint32_t Button::getDuration() {
  return duration;
}

uint16_t Button::getInterval() {
  return interval;
}

AnalogButtons::AnalogButtons(uint8_t pin, uint8_t maxCount, uint8_t mode, uint16_t debounce, uint8_t margin) {
	this -> pin = pin;
	this -> debounce = debounce;
	this -> counter = 0;
	this -> margin = margin;
	this -> buttonsCount = 0;
  this -> maxCount = maxCount;
  this -> time = 0;
  this -> lastButtonPressed = 0x00;
  this -> debounceButton = 0x00;
  buttons = new Button*[maxCount];
	pinMode(pin, mode);
}

AnalogButtons::~AnalogButtons() {
  for (uint8_t i = 0; i < buttonsCount; i++) {
    delete (buttons[i]);
  }
  delete[] buttons;
}

void AnalogButtons::add(Button* button) {
	if (buttonsCount < maxCount) {
    	buttons[buttonsCount++] = button;
  }
}

void AnalogButtons::check() {
	// In case this function gets called very frequently avoid sampling the analog pin too often
	if (millis() - time > ANALOGBUTTONS_SAMPLING_INTERVAL) {
		time = millis();
		uint16_t reading = analogRead(pin);
		for (uint8_t i = 0; i < buttonsCount; i++) {
			if (reading >= buttons[i] -> getValue() - margin && reading <= buttons[i] -> getValue() + margin) {
				if (lastButtonPressed != buttons[i]) {
					buttons[i] -> setHeldDown(false);
					if (debounceButton != buttons[i]) {
            counter = 0;
            debounceButton = buttons[i];
          }
					if (++counter >= debounce) {
						// button properly debounced
						lastButtonPressed = buttons[i];
						previousMillis = millis();
					}
				} else {
					if (!buttons[i] -> isHeldDown() && ((millis() - previousMillis) > buttons[i] -> getDuration())) {
						// button has been hold down long enough
						buttons[i] -> setHeldDown(true);
						buttons[i] -> held();
						previousMillis = millis();
					} else if (buttons[i] -> isHeldDown() && ((millis() - previousMillis) > buttons[i] -> getInterval())) {
						// button was already held, it's time to fire again
						buttons[i] -> held();
						previousMillis = millis();
					}
				}
				// The first matching button is the only one that gets triggered
				return;
			}
		}
		// If execution reaches this point then no button has been pressed during this check
		if (lastButtonPressed != 0x00 && !lastButtonPressed -> isHeldDown()) {
			lastButtonPressed -> pressed();
		}
		debounceButton = lastButtonPressed = 0x00;
	}
}
