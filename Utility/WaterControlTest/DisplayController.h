#ifndef display_controller_h
#define display_controller_h

#include "Configuration.h"
#include "Statistic.h"
#include "ConfigurationMenu.h"

class DisplayController {
  public:
    DisplayController(PCD8544* lcd);
    ~DisplayController();
    void check(int wtpsCurrentValue, int ppsCurrentValue);
    void setDisplayRegularMenu(bool displayRegularMenu);
    bool isDisplayRegularMenu();
    ConfigurationMenu* getConfigurationMenu();
    void turnOnHighlight();
    void turnOffHighlight();
    boolean isHighlightOn();
  private:
    PCD8544* _lcd;
    ConfigurationMenu* _configurationMenu;
    bool _displayRegularMenu = true;
    bool _highlightOn;
    void _displayRegularScreen(int wtpsCurrentValue, int ppsCurrentValue);
    void _displayCurrentStatus();
    void _displayProtectionInfo();
    void _turnOnGreen();
    void _turnOnYellow();
    void _turnOnRed();
};

#endif
