#include "AUTest.h"
#include <Arduino.h>

int failedTestsCount = 0;
int passedTestsCount = 0;

void processTestResult(bool testResult, const __FlashStringHelper* testName) {
  if (testResult) {
    Serial.print(F("       "));
    passedTestsCount++;
  } else {
    Serial.print(F("ERROR: "));
    failedTestsCount++;
  }
  Serial.print(testName); Serial.print(F("..........."));
  if(testResult) {
    Serial.println(F("SUCCESS"));
  } else {
    Serial.println(F("FAILED"));
  }
}

void printResults() {
  Serial.println();
  Serial.println();
  Serial.print(F("Success: ")); Serial.println(passedTestsCount);
  Serial.print(F("Failed:  ")); Serial.println(failedTestsCount);
  Serial.println();
  Serial.println();
}

bool assertEquals(int actualValue, int expectedValue, const __FlashStringHelper* errorMessage) {
  if (actualValue != expectedValue) {
    Serial.print(F(">>> "));Serial.print(errorMessage);Serial.print(F(" Actual value is: "));Serial.print(actualValue);Serial.print(F(" ,but expected value is: "));Serial.println(expectedValue);
  }
  return (actualValue == expectedValue);
}

bool assertEquals(unsigned long actualValue, unsigned long expectedValue, const __FlashStringHelper* errorMessage) {
  if (actualValue != expectedValue) {
    Serial.print(F(">>> "));Serial.print(errorMessage);Serial.print(F(" Actual value is: ")); Serial.print(actualValue); Serial.print(F(" ,but expected value is: ")); Serial.println(expectedValue);
  }
  return (actualValue == expectedValue);
}

bool assertEquals(float actualValue, float expectedValue, const __FlashStringHelper* errorMessage) {
  if (actualValue != expectedValue) {
    Serial.print(F(">>> "));Serial.print(errorMessage);Serial.print(F(" Actual value is: ")); Serial.print(actualValue); Serial.print(F(" ,but expected value is: ")); Serial.println(expectedValue);
  }
  return (actualValue == expectedValue);
}

bool assertEquals(bool actualValue, bool expectedValue, const __FlashStringHelper* errorMessage) {
  if (actualValue != expectedValue) {
    Serial.print(F(">>> "));Serial.print(errorMessage);Serial.print(F(" Actual value is: ")); Serial.print(actualValue); Serial.print(F(" ,but expected value is: ")); Serial.println(expectedValue);
  }
  return (actualValue == expectedValue);
}

bool assertTrue(bool condition, const __FlashStringHelper* errorMessage) {
  if (!condition) {
    Serial.print(F(">>> "));Serial.print(errorMessage);Serial.println(F(" Condition expected to be true"));
  }
  return (condition);
}

bool assertManualAccept(const __FlashStringHelper* promptMessage) {
  Serial.print("              ");Serial.print(promptMessage); Serial.print(F(" (y/n) > "));
  char response;
  do {
    while (!Serial.available()) {};
    response = Serial.read();
  } while (response != 'y' && response != 'n');
  Serial.println(response);
  return response == 'y';
}
