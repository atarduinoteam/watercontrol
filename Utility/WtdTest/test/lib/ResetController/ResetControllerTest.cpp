#include <ResetController.h>
#include <unity.h>
#include <stdio.h>

ResetController* resetController = 0;

bool onWdtResetCall;
bool onBrownoutResetCall;
bool onExternalResetCall;
bool onPowerOnResetCall;

void onWdtResetFunction() {
    onWdtResetCall = true;
}

void onBrownoutResetFunction() {
    onBrownoutResetCall = true;
}

void onExternalResetFunction() {
    onExternalResetCall = true;
}

void onPowerOnResetFunction() {
    onPowerOnResetCall = true;
}

void clearResources() {
    onWdtResetCall = false;
    onBrownoutResetCall = false;
    onExternalResetCall = false;
    onPowerOnResetCall = false;
    if (resetController) {
        delete resetController;
    }
}

void initRestController() {
    clearResources();
    resetController = new ResetController(&onWdtResetFunction, &onBrownoutResetFunction, &onExternalResetFunction, &onPowerOnResetFunction);
}

void testWtdResetFunctionWillBeExecuted() {
    initRestController();
    uint8_t mcusrValue = 0b00001000;
    resetController -> checkReset(mcusrValue);
    TEST_ASSERT_TRUE(onWdtResetCall);
    TEST_ASSERT_FALSE(onBrownoutResetCall);
    TEST_ASSERT_FALSE(onExternalResetCall);
    TEST_ASSERT_FALSE(onPowerOnResetCall);
}

void testBrownoutResetFunctionWillBeExecuted() {
    initRestController();
    uint8_t mcusrValue = 0b00000100;
    resetController -> checkReset(mcusrValue);
    TEST_ASSERT_FALSE(onWdtResetCall);
    TEST_ASSERT_TRUE(onBrownoutResetCall);
    TEST_ASSERT_FALSE(onExternalResetCall);
    TEST_ASSERT_FALSE(onPowerOnResetCall);
}

void testExternalResetFunctionWillBeExecuted() {
    initRestController();
    uint8_t mcusrValue = 0b00000010;
    resetController -> checkReset(mcusrValue);
    TEST_ASSERT_FALSE(onWdtResetCall);
    TEST_ASSERT_FALSE(onBrownoutResetCall);
    TEST_ASSERT_TRUE(onExternalResetCall);
    TEST_ASSERT_FALSE(onPowerOnResetCall);
}

void testPowerOnResetFunctionWillBeExecuted() {
    initRestController();
    uint8_t mcusrValue = 0b00000001;
    resetController -> checkReset(mcusrValue);
    TEST_ASSERT_FALSE(onWdtResetCall);
    TEST_ASSERT_FALSE(onBrownoutResetCall);
    TEST_ASSERT_FALSE(onExternalResetCall);
    TEST_ASSERT_TRUE(onPowerOnResetCall);
}

void testWdtResetFunctionCanBeNull() {
    clearResources();
    resetController = new ResetController();
    uint8_t mcusrValue = 0b00001000;
    resetController -> checkReset(mcusrValue);
    TEST_ASSERT_FALSE(onWdtResetCall);
    TEST_ASSERT_FALSE(onBrownoutResetCall);
    TEST_ASSERT_FALSE(onExternalResetCall);
    TEST_ASSERT_FALSE(onPowerOnResetCall);
}

void testBrownoutResetFunctionCanBeNull() {
    clearResources();
    resetController = new ResetController();
    uint8_t mcusrValue = 0b00000100;
    resetController -> checkReset(mcusrValue);
    TEST_ASSERT_FALSE(onWdtResetCall);
    TEST_ASSERT_FALSE(onBrownoutResetCall);
    TEST_ASSERT_FALSE(onExternalResetCall);
    TEST_ASSERT_FALSE(onPowerOnResetCall);
}

void testExternalResetFunctionCanBeNull() {
    clearResources();
    resetController = new ResetController();
    uint8_t mcusrValue = 0b00000010;
    resetController -> checkReset(mcusrValue);
    TEST_ASSERT_FALSE(onWdtResetCall);
    TEST_ASSERT_FALSE(onBrownoutResetCall);
    TEST_ASSERT_FALSE(onExternalResetCall);
    TEST_ASSERT_FALSE(onPowerOnResetCall);
}

void testPowerOnResetFunctionCanBeNull() {
    clearResources();
    resetController = new ResetController();
    uint8_t mcusrValue = 0b00000001;
    resetController -> checkReset(mcusrValue);
    TEST_ASSERT_FALSE(onWdtResetCall);
    TEST_ASSERT_FALSE(onBrownoutResetCall);
    TEST_ASSERT_FALSE(onExternalResetCall);
    TEST_ASSERT_FALSE(onPowerOnResetCall);
}

void testResetControllerMemoryLeak() {
    for (int i = 1; i < 2101; i++) {
        resetController = new ResetController(&onWdtResetFunction, &onBrownoutResetFunction, &onExternalResetFunction, &onPowerOnResetFunction);
        delete resetController;
        UnityPrint(".");
        if (i > 0 && i % 100 == 0) {
            UNITY_PRINT_EOL();
        }
    }
    UNITY_PRINT_EOL();
    TEST_ASSERT_TRUE(true);
}

int main(int argc, char **argv) {
    UNITY_BEGIN();
    
    RUN_TEST(testWtdResetFunctionWillBeExecuted);
    RUN_TEST(testBrownoutResetFunctionWillBeExecuted);
    RUN_TEST(testExternalResetFunctionWillBeExecuted);
    RUN_TEST(testPowerOnResetFunctionWillBeExecuted);
    
    RUN_TEST(testWdtResetFunctionCanBeNull);
    RUN_TEST(testBrownoutResetFunctionCanBeNull);
    RUN_TEST(testExternalResetFunctionCanBeNull);
    RUN_TEST(testPowerOnResetFunctionCanBeNull);

    RUN_TEST(testResetControllerMemoryLeak);

    UNITY_END();
    return 0;
}
