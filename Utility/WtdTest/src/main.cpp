#include <avr/wdt.h>
#include <Arduino.h>
#include "ResetController.h"

void onWdtReset() {
  Serial.println(F("A watchdog reset occurred"));
}

void onBrownoutReset() {
  Serial.println(F("A brownout reset occurred"));
}

void onExternalReset() {
  Serial.println(F("An external reset occurred"));
}

void onPowerOnReset() {
  Serial.println(F("A power on reset occurred"));
}

void setup() {
  
  uint8_t mcusr_copy;
  mcusr_copy = MCUSR;
  MCUSR = 0;
  wdt_disable();

  Serial.begin(9600);
  Serial.println("Setup...");
  
  ResetController* resetController = new ResetController(&onWdtReset, &onBrownoutReset, &onExternalReset, &onPowerOnReset);
  resetController -> checkReset(mcusr_copy);
  
  Serial.println("Wait 5 sec..");
  delay(5000); // Задержка, чтобы было время перепрошить устройство в случае bootloop
  wdt_enable(WDTO_4S); // Для тестов не рекомендуется устанавливать значение менее 8 сек.
  Serial.println("Watchdog enabled.");
}

int timer = 0;

void loop() {
  // Каждую секунду мигаем светодиодом и значение счетчика пишем в Serial
  if(!(millis()%1000)){
    timer++;
    Serial.println(timer);
    digitalWrite(13, digitalRead(13)==1?0:1); 
    delay(1);
  }
//  wdt_reset();
}