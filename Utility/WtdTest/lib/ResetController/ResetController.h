#ifndef ResetController_h
#define ResetController_h

#include <Arduino.h>

class ResetController {
    public:
        ResetController(void (*onWdtReset)(void) = 0, void (*onBrownoutReset)(void) = 0, void (*onExternalReset)(void) = 0, void (*onPowerOnReset)(void) = 0);
        ~ResetController();
        void checkReset(uint8_t mcusrValue);
    private:
        void (*_onWdtReset)(void);
        void (*_onBrownoutReset)(void);
        void (*_onExternalReset)(void);
        void (*_onPowerOnReset)(void);
};

#endif