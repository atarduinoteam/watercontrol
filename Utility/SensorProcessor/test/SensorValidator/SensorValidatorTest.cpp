#include <SensorValidator.h>
#include <unity.h>
#include <stdio.h>
#include <Arduino.h>

SensorValidator* sensorValidator;

void testMemory() {
    for(int i = 0; i < 2050; i++) {
        sensorValidator = new SensorValidator(100, 1000);
        sensorValidator -> isValid(100, 100);
        delete sensorValidator;
    }
}

void testSensorValidationWillReturnTrueIfWtpsLowerPps() {
    sensorValidator = new SensorValidator(100, 1000);
    unsigned long startTime = millis();
    while ((millis() - startTime) < 3000) {
        TEST_ASSERT_TRUE(sensorValidator -> isValid(500, 600));
    }
    delete sensorValidator;
}

void testSensorValidationWillReturnTrueIfSingleViolationHappens() {
    sensorValidator = new SensorValidator(100, 1000);
    unsigned long startTime = millis();
    bool oddIteration = true;
    while ((millis() - startTime) < 3000) {
        TEST_ASSERT_TRUE(sensorValidator -> isValid(oddIteration? 500 : 800, 600));
        oddIteration = !oddIteration;
    }
    delete sensorValidator;
}

void testSensorValidationWillReturnTrueIfViolationTimeLessThanConfigured() {
    sensorValidator = new SensorValidator(100, 1000);
    unsigned long startTime = millis();
    unsigned long resetStartTime = startTime;
    int wtpsValue = 800;
    while (millis() - startTime < 3000) {
        if ((millis() - resetStartTime) > 900) {
            resetStartTime = millis();
            wtpsValue = 400;
        } else {
            wtpsValue = 800;
        }
        TEST_ASSERT_TRUE(sensorValidator -> isValid(wtpsValue, 600));
    }
    delete sensorValidator;
}

void testSensorValidationWillReturnFalseIfViolationIsContinuousMoreTheConfiguredTime() {
    sensorValidator = new SensorValidator(100, 1000);
    unsigned long startTime = millis();
    bool result = true;
    while ((millis() - startTime) < 1100) {
        result = result && sensorValidator -> isValid(800, 600);
    }
    TEST_ASSERT_FALSE(result);
    delete sensorValidator;
}

void testPpsExceedingDoesNotViolationIfLessConfiguredDelta() {
    sensorValidator = new SensorValidator(100, 1000);
    unsigned long startTime = millis();
    while ((millis() - startTime) < 3000) {
        TEST_ASSERT_TRUE(sensorValidator -> isValid(650, 600));
    }
    delete sensorValidator;
}

void testPpsExceedingDoesNotViolationIfEqualToConfiguredDelta() {
    sensorValidator = new SensorValidator(100, 1000);
    unsigned long startTime = millis();
    while ((millis() - startTime) < 3000) {
        TEST_ASSERT_TRUE(sensorValidator -> isValid(700, 600));
    }
    delete sensorValidator;
}

void testWtpsExceedingIsViolationIfMoreThanConfiguredDelta() {
    sensorValidator = new SensorValidator(100, 1000);
    unsigned long startTime = millis();
    bool result = true;
    while ((millis() - startTime) < 1100) {
        result = result && sensorValidator -> isValid(701, 600);
    }
    TEST_ASSERT_FALSE(result);
    delete sensorValidator;
}

int main(int argc, char **argv) {
    init();
    UNITY_BEGIN();
    RUN_TEST(testMemory);
    RUN_TEST(testSensorValidationWillReturnTrueIfWtpsLowerPps);
    RUN_TEST(testSensorValidationWillReturnTrueIfSingleViolationHappens);
    RUN_TEST(testSensorValidationWillReturnTrueIfViolationTimeLessThanConfigured);
    RUN_TEST(testSensorValidationWillReturnFalseIfViolationIsContinuousMoreTheConfiguredTime);
    RUN_TEST(testPpsExceedingDoesNotViolationIfLessConfiguredDelta);
    RUN_TEST(testPpsExceedingDoesNotViolationIfEqualToConfiguredDelta);
    RUN_TEST(testWtpsExceedingIsViolationIfMoreThanConfiguredDelta);
    UNITY_END();
    return 0;
}
