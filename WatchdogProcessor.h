#ifndef WatchdogProcessor_h
#define WatchdogProcessor_h

#include "ResetController.h"

class WatchdogProcessor {
  public:
    WatchdogProcessor();
    ~WatchdogProcessor();
    void checkReset(uint8_t mcusrCopy);
  private:
    //void _onWdtReset();
    ResetController* _resetController;
};

#endif
