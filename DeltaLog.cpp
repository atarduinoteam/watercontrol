#include "Configuration.h"
#include "DeltaLog.h"
#include "BoardConfiguration.h"
#include <EEPROM.h>
#include "InitBoard.h"

const int MAX_OFFSET = EEPROM_SIZE - sizeof(DeltaEntry);

// ================================ Global functions ================================ //

int _calculateOffset(int index) {
  return index * sizeof(DeltaEntry) + getStatistic() -> getStructOffset();
}

int _getNextIndex(int index) {
  index++;
  if (_calculateOffset(index) > MAX_OFFSET) {
    index = 0;
  }
  return index;
}

// ================================ DeltaLog ================================ //

DeltaLog::DeltaLog() {
  int offset = _calculateOffset(getStatistic() -> getIndex());
  DeltaEntry lastDeltaEntry;
  EEPROM.get(offset, lastDeltaEntry);
  _lastDeltaPressure = lastDeltaEntry.deltaPressure;
  _lastSavingTime = millis();
  _iterator = new DeltaLogIterator();
}

DeltaLog::~DeltaLog() {
  delete _iterator;
}

bool DeltaLog::writeDeltaIfBigger(int delta) {
  if (delta > _lastDeltaPressure) {
    DeltaEntry newDelta;
    newDelta.deltaPressure = delta;
    newDelta.deltaTime = millis() - _lastSavingTime;
    _lastSavingTime = millis();
    _writeDelta(newDelta);
    _lastDeltaPressure = delta;
    return true;
  }
  return false;
}

DeltaLogIterator* DeltaLog::getIterator() {
  _iterator -> init();
  return _iterator;
}

void DeltaLog::_writeDelta(DeltaEntry delta) {
  int nextIndex = _getNextIndex(getStatistic() -> getIndex());
  int offset = _calculateOffset(nextIndex);
  EEPROM.put(offset, delta);
  getStatistic() -> setIndex(nextIndex);
}

// ================================ Iterator for DeltaLog ================================ //

DeltaLogIterator::DeltaLogIterator() {
}

void DeltaLogIterator::init() {
  _stepCount = 0;
  if (_isFirstCycle()) {
    _currentPosition = 0;
    _size = getStatistic() -> getIndex() + 1;
  } else {
    _currentPosition = _getNextIndex(getStatistic() -> getIndex());
    _size = ((EEPROM_SIZE - getStatistic() -> getStructOffset()) / sizeof(DeltaEntry));
  }
}

bool DeltaLogIterator::hasNext() {
  return _stepCount < _size;
}

DeltaEntry DeltaLogIterator::getNext() {
  int offset = _calculateOffset(_currentPosition);
  DeltaEntry deltaEntry;
  EEPROM.get(offset, deltaEntry);
  _currentPosition = _getNextIndex(_currentPosition);
  _stepCount++;
  return deltaEntry;
}

int DeltaLogIterator::getSize() {
  return _size;
}

bool DeltaLogIterator::_isFirstCycle() {
  int nextIndex = _getNextIndex(getStatistic() -> getIndex());
  int offset = _calculateOffset(nextIndex);
  DeltaEntry deltaEntry;
  EEPROM.get(offset, deltaEntry);
  return deltaEntry.deltaPressure == 0 && deltaEntry.deltaTime == 0;
}
