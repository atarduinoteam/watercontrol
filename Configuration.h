#ifndef Configuration_h
#define Configuration_h

#include <Arduino.h>
#include "BaseEepromStruct.h"

typedef enum {
  // Int values
  WtpsMin,
  WtpsMax,
  PpsYellow,
  PpsRed,
  // Bool value
  LogEnabled,
  // Int values
  DisplayContrast,
  WtpsOffset,
  PpsOffset,
  MaxWtpsPressure,
  MaxPpsPressure,
  ConfigurationPressure
} ConfigurationProperty;

typedef struct {                          // Configuration structure
  int wtpsMin;                            // WTPS min value, atm
  int wtpsMax;                            // WTPS max value, atm
  int ppsYellow;                          // PPS value to turn on yellow signal, atm
  int ppsRed;                             // PPS value to turn on red signal and turn off whole system, atm
  bool logEnabled;                        // Is statistics collection turned on
  int displayContrast;                    // Contrast of display
  int wtpsOffset;                         // Result of WTPS zero offset autocalibration
  int ppsOffset;                          // Result of PPS zero offset autocalibration
  int maxWtpsPressure;                    // Scale configuration of WTPS pressure
  int maxPpsPressure;                     // Scale configuration of PPS pressure
  int configurationPressure;              // Defualt pressure for automatic sensor non-zero calibration, atm
} __attribute__((packed)) ConfigurationStructure;

class Configuration : public BaseEepromStruct {
  public:
    Configuration(int offset, bool initNewStruct);
    ~Configuration();
    int getWtpsMin();
    int getWtpsMax();
    int getPpsRed();
    int getPpsYellow();
    bool isLogEnabled();
    int getDisplayContrast();
    int getWtpsOffset();
    int getPpsOffset();
    int getMaxWtpsPressure();
    int getMaxPpsPressure();
    int getConfigurationPressure();
    int getIntConfigurationValue(ConfigurationProperty property);
    bool getBoolConfigurationValue(ConfigurationProperty property);
    void setConfigurationValue(ConfigurationProperty property, int value);
    void setConfigurationValue(ConfigurationProperty property, bool value);
  protected:
    void initStruct() override;
    void readStruct() override;
    void initStructureOffset() override;
    int getStructSize() override;
  private:
    ConfigurationStructure _configurationStructure;
};

#endif
