#ifndef PumpController_h
#define PumpController_h

#include "DeltaLog.h"
#include "SensorValidator.h"

class PumpController {
  public:
    PumpController(DeltaLog* deltaLog);
    ~PumpController();
    void check(int wtpsValue, int ppsValue);
    bool isPumpOn();
  private:
    void _processWtps(int wtpsValue);
    void _processPps(int ppsValue);
    void _turnOnWaterPump();
    void _turnOffWaterPump();
    void _processPpsBiggerWtpsRestriction(int wtpsValue, int ppsValue);
    void _logDeltaPressure(int deltaPressure);
    void _updatePpsCycleMax(int ppsValue);
    DeltaLog* _deltaLog;
    SensorValidator* _sensorValidator;
    bool _waterPumpIsOn;
};

#endif
