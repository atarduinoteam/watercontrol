#ifndef BoardConfiguration_h
#define BoardConfiguration_h

#include "GenericDefinitions.h"
#include <Arduino.h>
#include "FloatingMedian.h"

const int WTPS_PIN=A6;
const int PPS_PIN=A7;
const int MENU_BUTTONS_PIN = A2;
/**
 * Common status diode address:
 * |h|l|status|
 * |0|0|green |
 * |0|1|yellow|
 * |1|0|red   |
 */
const byte DIODE_ADDR_LOW = 17;
const byte DIODE_ADDR_HIGH = 18;

const byte SCREEN_HIGHLIGHT_PIN = 2;

const byte WATER_PUMP_PIN = 8;

const byte NRF_CE_PIN = 9;
const byte NRF_CS_PIN = 10;

const byte EEPROM_DATA_VERSION = 13;

const int MAX_SENSOR_VALUE = 1023;
const int EEPROM_SIZE = 1024;
const int SENSOR_ALLOWABLE_ERROR = 100;
const int SENSOR_ALLOWABLE_ERROR_DURATION = 2000;

  #ifdef TEST_SKETCH
    
    #ifndef MANUAL_E2E_TEST
      #ifdef CONFIGURATION_MENU_ITEMS_TESTS
        const unsigned long CLOSE_MENU_DELAY = 100000;
      #else
        const unsigned long CLOSE_MENU_DELAY = 900;
      #endif
    #else
      const unsigned long CLOSE_MENU_DELAY = 10000;
    #endif
    
    #ifdef CONFIGURATION_MENU_ROLL_TESTS
      const byte SCREEN_LINES_COUNT = 3;
    #else
      const byte SCREEN_LINES_COUNT = 6;
    #endif

    const int FLOAT_MEDIAN_ARRAY_SIZE = 1;
    const int FLOAT_MEDIAN_PEAK_ARRAY_SIZE = 0;
    
  #else
    const unsigned long CLOSE_MENU_DELAY = 10000;
    const byte SCREEN_LINES_COUNT = 6;
    
    const int FLOAT_MEDIAN_ARRAY_SIZE = 13;
    const int FLOAT_MEDIAN_PEAK_ARRAY_SIZE = 2;
  #endif

#endif
