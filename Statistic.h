#ifndef Statistic_h
#define Statistic_h

#include <Arduino.h>
#include "BaseEepromStruct.h"

typedef enum {
  RebootCounter,
  PumpCounter,
  PpsCycleMax,
  Index,
  ErrorCode,
  WatchdogCounter
} StatisticProperty;

typedef enum {
  NoError,
  PpsWarning,
  ErrorTooBigPpsValue,
  SensorsRestrictionViolation     // PPS has value significantly less then WTPS value
} EErrorCode;

typedef struct {                  // Statistics structure
  unsigned long rebootCounter;    // Reboot counter
  unsigned long pumpCounter;      // Water pump was turned on number of times, (X10)
  int ppsCycleMax;                // Max pressure on PPS during last cycle
  int index;                      // Position index in Delta array to write new value
  EErrorCode errorCode;           // Code of error
  unsigned long watchdogCounter;  // Watchdog restart counter
} __attribute__((packed)) StatisticStructure;

class Statistic : public BaseEepromStruct {
  public:
    Statistic(int offset, bool initNewStruct);
    ~Statistic();
    unsigned long getRebootCounter();
    unsigned long getPumpCounter();
    int getPpsCycleMax();
    int getIndex();
    EErrorCode getErrorCode();
    unsigned long getWatchdogCounter();
    void increaseRebootCounter();
    void increasePumpCounter();
    void setPpsCycleMax(int ppsCycleMax);
    void setIndex(int index);
    void setErrorCode(EErrorCode errorCode);
    void resetErrorCode();
    void increaseWatchdogCounter();
  protected:
    void initStruct() override;
    void readStruct() override;
    void initStructureOffset() override;
    int getStructSize() override;
  private:
    bool _isErrorApplicable(EErrorCode errorCode);
    void _updateErrorCode(EErrorCode errorCode);
    StatisticStructure _statisticStructure;
};

#endif
