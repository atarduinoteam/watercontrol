#include "InitBoard.h"
#include <EEPROM.h>

DisplayController* _displayController;
Configuration* _configuration;
Statistic* _statistic;
SensorValueNormalizer* _sensorValueNormalizer;

DisplayController* getDisplayController() {
  return _displayController;
}

Configuration* getConfiguration() {
  return _configuration;
}

Statistic* getStatistic() {
  return _statistic;
}

SensorValueNormalizer* getSensorValueNormalizer() {
  return _sensorValueNormalizer;
}

void initDisplayController(PCD8544* lcd) {
  _displayController = new DisplayController(lcd);
}

void initEeprom(byte version) {
  byte lastVersion;
  EEPROM.get(0, lastVersion);
  bool initNewStruct = (version != lastVersion);
  _configuration = new Configuration(sizeof(byte), initNewStruct);
  _statistic = new Statistic(_configuration -> getStructOffset(), initNewStruct);
  _statistic -> increaseRebootCounter();
  _sensorValueNormalizer = new SensorValueNormalizer(FLOAT_MEDIAN_ARRAY_SIZE, FLOAT_MEDIAN_PEAK_ARRAY_SIZE, analogRead(WTPS_PIN), analogRead(PPS_PIN));
  if (initNewStruct) {
    EEPROM.put(0, version);
    for (int i = _statistic -> getStructOffset(); i < EEPROM_SIZE; i++) {
      EEPROM[i] = 0;
    }
  }
}
void releaseCommonResources() {
  delete _configuration;
  delete _statistic;
  delete _sensorValueNormalizer;
}

float convertAnalogPressureToFloat(int pressure) {
  return pressure < 0 ? 0 : pressure/1000.0;
}
