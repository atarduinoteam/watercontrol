#include "EEPROM.h"
#include "Configuration.h"

Configuration::Configuration(int offset, bool initNewStruct) : BaseEepromStruct(offset) {
  if (initNewStruct) {
    initStruct();
  } else {
    readStruct();
  }
  initStructureOffset();
}

Configuration::~Configuration() {
}

void Configuration::initStruct() {
  _configurationStructure.wtpsMin = 2500;
  _configurationStructure.wtpsMax = 3500;
  _configurationStructure.ppsYellow = 4000;
  _configurationStructure.ppsRed = 4500;
  _configurationStructure.logEnabled = true;
  _configurationStructure.displayContrast = 60;
  _configurationStructure.wtpsOffset = 100;
  _configurationStructure.ppsOffset = 100;
  _configurationStructure.maxWtpsPressure = 11843;
  _configurationStructure.maxPpsPressure = 11843;
  _configurationStructure.configurationPressure = 3300;
  EEPROM.put(getOffset(), _configurationStructure);
}

void Configuration::readStruct() {
  EEPROM.get(getOffset(), _configurationStructure);
}

void Configuration::initStructureOffset() {
  _structureOffset = new int[11];
  _structureOffset[WtpsMin] = 0;
  _structureOffset[WtpsMax] = _structureOffset[WtpsMin] + sizeof(int);
  _structureOffset[PpsYellow] = _structureOffset[WtpsMax] + sizeof(int);
  _structureOffset[PpsRed] = _structureOffset[PpsYellow] + sizeof(int);
  _structureOffset[LogEnabled] = _structureOffset[PpsRed] + sizeof(int);
  _structureOffset[DisplayContrast] = _structureOffset[LogEnabled] + sizeof(bool);
  _structureOffset[WtpsOffset] = _structureOffset[DisplayContrast] + sizeof(int);
  _structureOffset[PpsOffset] = _structureOffset[WtpsOffset] + sizeof(int);
  _structureOffset[MaxWtpsPressure] = _structureOffset[PpsOffset] + sizeof(int);
  _structureOffset[MaxPpsPressure] = _structureOffset[MaxWtpsPressure] + sizeof(int);
  _structureOffset[ConfigurationPressure] = _structureOffset[MaxPpsPressure] + sizeof(int);
}

int Configuration::getStructSize() {
  return sizeof(ConfigurationStructure);
}

int Configuration::getWtpsMin() {
  return _configurationStructure.wtpsMin;
}

int Configuration::getWtpsMax() {
  return _configurationStructure.wtpsMax;
}

int Configuration::getPpsRed() {
  return _configurationStructure.ppsRed;
}

int Configuration::getPpsYellow() {
  return _configurationStructure.ppsYellow;
}

bool Configuration::isLogEnabled() {
  return _configurationStructure.logEnabled;
}

int Configuration::getDisplayContrast() {
  return _configurationStructure.displayContrast;
}

int Configuration::getWtpsOffset() {
  return _configurationStructure.wtpsOffset;
}

int Configuration::getPpsOffset() {
  return _configurationStructure.ppsOffset;
}

int Configuration::getMaxWtpsPressure() {
  return _configurationStructure.maxWtpsPressure;
}

int Configuration::getMaxPpsPressure() {
  return _configurationStructure.maxPpsPressure;
}

int Configuration::getConfigurationPressure() {
  return _configurationStructure.configurationPressure;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
#pragma GCC diagnostic ignored "-Wreturn-type"
int Configuration::getIntConfigurationValue(ConfigurationProperty property) {
  switch (property) {
    case WtpsMin: {
      return getWtpsMin();
    }
    case WtpsMax: {
      return getWtpsMax();
    }
    case PpsYellow: {
      return getPpsYellow();
    }
    case PpsRed: {
      return getPpsRed();
    }
    case DisplayContrast: {
      return getDisplayContrast();
    }
    case WtpsOffset: {
      return getWtpsOffset();
    }
    case PpsOffset: {
      return getPpsOffset();
    }
    case MaxWtpsPressure: {
      return getMaxWtpsPressure();
    }
    case MaxPpsPressure: {
      return getMaxPpsPressure();
    }
    case ConfigurationPressure: {
      return getConfigurationPressure();
    }
  }
  Serial.print("Unknown int configuration property.");
}
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
#pragma GCC diagnostic ignored "-Wreturn-type"
bool Configuration::getBoolConfigurationValue(ConfigurationProperty property) {
  switch (property) {
    case LogEnabled: {
      return isLogEnabled();
    }
  }
  Serial.print("Unknown bool configuration property.");
}
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
void Configuration::setConfigurationValue(ConfigurationProperty property, int value) {
  switch (property) {
    case WtpsMin: {
       _configurationStructure.wtpsMin = value;
      break;
    }
    case WtpsMax: {
      _configurationStructure.wtpsMax = value;
      break;
    }
    case PpsYellow: {
      _configurationStructure.ppsYellow = value;
      break;
    }
    case PpsRed: {
      _configurationStructure.ppsRed = value;
      break;
    }
    case DisplayContrast: {
      _configurationStructure.displayContrast = value;
      break;
    }
    case WtpsOffset: {
      _configurationStructure.wtpsOffset = value;
      break;
    }
    case PpsOffset: {
      _configurationStructure.ppsOffset = value;
      break;
    }
    case MaxWtpsPressure: {
      _configurationStructure.maxWtpsPressure = value;
      break;
    }
    case MaxPpsPressure: {
      _configurationStructure.maxPpsPressure = value;
      break;
    }
    case ConfigurationPressure: {
      _configurationStructure.configurationPressure = value;
      break;
    }
  }
  EEPROM.put(getOffset() + _structureOffset[property], value);
}
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
void Configuration::setConfigurationValue(ConfigurationProperty property, bool value) {
   switch (property) {
    case LogEnabled: {
      _configurationStructure.logEnabled = value;
      break;
    }
  }
  EEPROM.put(getOffset() + _structureOffset[property], value);
}
#pragma GCC diagnostic pop
