#ifndef __ANALOGBUTTONS_H
#define __ANALOGBUTTONS_H

/*
 AnalogButtons

 In order to reduce the number of pins used by some projects, sketches can use
 this library to wire multiple buttons to one single analog pin.
 You can register a call-back function which gets called when a button is
 pressed or held down for the defined number of seconds.
 Includes a software key de-bouncing simple algorithm which can be tweaked and
 is based on the max sampling frequency of 50Hz (one sample every 120ms)
 
 Minimum hold duration (time that must elapse before a button is considered
 being held) and hold interval (time that must elapse between each activation
 of the hold function) can both be configured.

 This work is largely inspired by the AnalogButtons library available in the
 Arduino Playground library collection.

 */
#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#ifndef ANALOGBUTTONS_SAMPLING_INTERVAL
#define ANALOGBUTTONS_SAMPLING_INTERVAL 20
#endif 

class Button {
  public:
    Button(uint16_t value, void (*clickFunction)(void) = 0, void (*holdFunction)(void) = 0, uint32_t holdDuration = 1000, uint16_t holdInterval = 100);
    inline void pressed();
    inline void held();
    uint16_t getValue();
    boolean isHeldDown();
    void setHeldDown(boolean heldDown);
    uint32_t getDuration();
    uint16_t getInterval();
  private:
    uint16_t value;
    void (*clickFunction)(void);
    void (*holdFunction)(void);
    uint32_t duration;
    uint16_t interval;
    boolean heldDown;
};

class AnalogButtons {
  public:
    AnalogButtons(uint8_t pin, uint8_t maxCount = 8, uint8_t mode = INPUT, uint16_t debounce = 5, uint8_t margin = 15);
    ~AnalogButtons();
    void add(Button* button);
    void check();
  private:
    uint32_t previousMillis;
    uint16_t debounce;
    uint8_t debounceCounter;
    uint8_t counter;
    uint32_t time;
    uint8_t margin;
    uint8_t pin;
    uint8_t buttonsCount;
    uint8_t maxCount;
    Button** buttons;
    Button* lastButtonPressed;
    Button* debounceButton;
};

#endif
