#include "Configuration.h"
#include "Statistic.h"
#include "DeltaLog.h"
#include "SensorValueNormalizer.h"
#include "ButtonsController.h"
#include "InitBoard.h"
#include "RadioController.h"
#include "PumpController.h"
#include <avr/wdt.h>
#include "WatchdogProcessor.h"

PCD8544 lcd;
RadioController* radioController; 
DeltaLog* deltaLog;
PumpController* pumpController;

void setup() {
  // Watchdog initial configuration
  uint8_t mcusrCopy;
  mcusrCopy = MCUSR;
  MCUSR = 0;
  wdt_disable();
  WatchdogProcessor* watchdogProcessor = new WatchdogProcessor();
  watchdogProcessor -> checkReset(mcusrCopy);
  delete watchdogProcessor;
  
  pinMode(DIODE_ADDR_LOW, OUTPUT);
  pinMode(DIODE_ADDR_HIGH, OUTPUT);
  pinMode(WATER_PUMP_PIN, OUTPUT);
  pinMode(SCREEN_HIGHLIGHT_PIN, OUTPUT);
  pinMode(WTPS_PIN, INPUT);
  pinMode(PPS_PIN, INPUT);
  initEeprom(EEPROM_DATA_VERSION);
  deltaLog = new DeltaLog();
  pumpController = new PumpController(deltaLog);
  initDisplayController(&lcd);
  radioController = new RadioController(getStatistic(), deltaLog);
  initButtons();

  wdt_enable(WDTO_4S);
}

void loop() {
  wdt_reset();
  int wtpsCurrentValue = getSensorValueNormalizer() -> normalizeWtpsSensorValue(analogRead(WTPS_PIN));
  int ppsCurrentValue = getSensorValueNormalizer() -> normalizePpsSensorValue(analogRead(PPS_PIN));
  radioController -> check();
  getAnalogButtons() -> check();
  getDisplayController() -> check(wtpsCurrentValue, ppsCurrentValue);
  pumpController -> check(wtpsCurrentValue, ppsCurrentValue);
}
