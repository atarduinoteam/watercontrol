#include "RadioController.h"
#include "BoardConfiguration.h"
#include <SmartWitchHouse.h>
#include <printf.h>
#include <avr/wdt.h>

//#define ENABLE_CONSOLE_LOG

RadioController::RadioController(Statistic* statistic, DeltaLog* deltaLog) {
  printf_begin();
  _radio = new RF24(NRF_CE_PIN, NRF_CS_PIN);
  _statistic = statistic;
  _deltaLog = deltaLog;
  _radio -> begin();
  _radio -> setAutoAck(true);
  _radio -> setPayloadSize(32);
  _radio -> setChannel(RADIO_CHANNEL);
  _radio -> setPALevel(RF24_PA_MIN);
  _radio -> setDataRate(RF24_250KBPS);
  _radio -> setAddressWidth(5);
  _radio -> openWritingPipe(PIPE[1]);
  _radio -> openReadingPipe(1, PIPE[0]);
  _radio -> setRetries(3,5);
  _radio -> printDetails();
  _radio -> powerUp();
  _radio -> startListening(); 
}

unsigned long startTime = 0;

void RadioController::check() {
  SmartWitchHouseRequest request;
  while (_radio -> available()) {

    wdt_reset();
    
    #ifdef ENABLE_CONSOLE_LOG
      Serial.println("Available");
    #endif

    startTime = millis();
    _radio -> read(&request, sizeof(request));
    if (request == REQUEST_WC_STAT) {
       _radio -> stopListening();
       _passLogs();
       _radio -> startListening();

       #ifdef ENABLE_CONSOLE_LOG
         Serial.print("Finished: "); Serial.println(millis() - startTime);
       #endif

    } else {

      #ifdef ENABLE_CONSOLE_LOG
        Serial.println("Wrong request");
      #endif

    }
  }
}

void RadioController::_passLogs() {
  DeltaLogIterator* iterator = _deltaLog -> getIterator();
  int index = 0;
  DeltaEntry deltaEntry;
  while (iterator -> hasNext()) {
    wdt_reset();
    index++;
    deltaEntry = iterator -> getNext();
    bool res = _sendDeltaEntry(deltaEntry, iterator -> getSize(), index);
    if (!res) {
      break;
    }
  }
}

bool RadioController::_sendDeltaEntry(DeltaEntry deltaEntry, int size, int index) {
  RadioDeltaEntry radioDeltaEntry;
  radioDeltaEntry.deltaEntry = deltaEntry;
  radioDeltaEntry.size = size;
  radioDeltaEntry.index = index;
  _radio -> writeBlocking(&radioDeltaEntry, sizeof(RadioDeltaEntry), 300);
  bool res = _radio -> txStandBy(300);

  #ifdef ENABLE_CONSOLE_LOG
    if (!res) {
      Serial.println("Cannot pass data");
    }
  #endif

  return res;
}
