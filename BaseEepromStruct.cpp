#include "BaseEepromStruct.h"

BaseEepromStruct::BaseEepromStruct(int offset) {
  _offset = offset;
}

BaseEepromStruct::~BaseEepromStruct() {
  delete _structureOffset;
}

int BaseEepromStruct::getStructOffset() {
  return getOffset() + getStructSize();
}

int BaseEepromStruct::getOffset() {
  return _offset;
}

