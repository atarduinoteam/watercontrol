#include "WatchdogProcessor.h"
#include "InitBoard.h"

void _onWdtReset() {
  getStatistic() -> increaseWatchdogCounter();
}

WatchdogProcessor::WatchdogProcessor() {
  _resetController = new ResetController(&_onWdtReset);
}

WatchdogProcessor::~WatchdogProcessor() {
  delete _resetController;
}

void WatchdogProcessor::checkReset(uint8_t mcusrCopy) {
  _resetController -> checkReset(mcusrCopy);
}
