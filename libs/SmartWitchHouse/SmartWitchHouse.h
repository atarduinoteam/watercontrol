#ifndef SmartWitchHouse_h
#define SmartWitchHouse_h

const byte RADIO_CHANNEL = 0x66;

// SERVR - Server pipe
// 1CLNT - Water control client

const byte PIPE[2][6] = {"SERVR", "1CLNT"};

typedef enum {
  REQUEST_WC_STAT = 1
} SmartWitchHouseRequest;

// ================ WaterControl response entry ================ //
typedef struct {
  int deltaPressure;
  unsigned long deltaTime;
} __attribute__((packed)) DeltaEntry;

typedef struct {
	DeltaEntry deltaEntry;
	byte size;
	byte index;
} __attribute__((packed)) RadioDeltaEntry;

#endif
